* directory: enforcers - contains the code to synthesize enforcers in Verilog from properties
		
		- directories: parser and compiler contain the code to synthesize enforcers written in Verilog from properties

		- directories: goal1_attack12, goal2_attack12, goal3_attack3: contain the Verilog code of the enforcers and their associated properties.

* directory: PLCs - contains the ladder logic code of the uncompromised PLCs and the compromised PLCs implemented via OpenPLC

* directory: swat_simulink_model - contains the Simulink model of the plant of a water transmission network
