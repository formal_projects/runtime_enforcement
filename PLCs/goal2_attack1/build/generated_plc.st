PROGRAM PLC1
  VAR_INPUT
    level : DINT;
    request : DINT;
  END_VAR
  VAR_OUTPUT
    pumps : BOOL;
    valve : BOOL;
  END_VAR
  VAR
    low : BOOL;
    MIDDLE : BOOL;
    high : BOOL;
    open_req : BOOL;
    close_req : BOOL;
    low_1 : DINT := 40;
    high_1 : DINT := 80;
    start_inject : BOOL := 1;
    inject : BOOL := 1;
    reset_inject : BOOL := 1;
    run : BOOL := 1;
    TON0 : TON;
    TON1 : TON;
    LE3_OUT : BOOL;
    GE7_OUT : BOOL;
    EQ23_OUT : BOOL;
    EQ24_OUT : BOOL;
  END_VAR

  LE3_OUT := LE(level, low_1);
  low := LE3_OUT;
  GE7_OUT := GE(level, high_1);
  high := GE7_OUT;
  EQ23_OUT := EQ(request, 1);
  open_req := EQ23_OUT;
  EQ24_OUT := EQ(request, 0);
  close_req := EQ24_OUT;
  pumps := NOT(high) AND (low OR pumps);
  TON0(IN := NOT(reset_inject) AND run, PT := T#5s);
  start_inject := TON0.Q;
  TON1(IN := start_inject, PT := T#5s);
  reset_inject := TON1.Q;
  inject := NOT(inject) AND start_inject;
  valve := NOT(close_req) AND (open_req AND NOT(low) OR valve);
  valve := NOT(start_inject) AND valve OR start_inject AND inject;
END_PROGRAM


CONFIGURATION Config0

  RESOURCE Res0 ON PLC
    TASK task0(INTERVAL := T#20ms,PRIORITY := 0);
    PROGRAM instance0 WITH task0 : PLC1;
  END_RESOURCE
END_CONFIGURATION
