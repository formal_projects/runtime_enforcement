Requirements: - antlr v.4.8 i.e., antlr-4.8-complete.jar and python 3.*

Usage: 

1. build antlr files: java -jar antlr-4.8-complete.jar -Dlanguage=Python3 Policy.g4 -visitor

2. input.txt contains the plc specification from which an edit automaton can be synthesized.

3. parse input.txt:  python ./parser/parser.py "custom_directory/input.txt"

4. compile the edit automaton code into an .v file: pyhton ./compiler/compiler.py 


* Note that the compilation process requires the directory "policy_automaton".