import math
import sys
import json


class Compiler():
    def __init__(self, input_file):
        self.PLCTypeToVerilogType = {"Bool": ""}
        # Or
        self.PLCTypeToVerilogType = {"Bool":"", "Integer":"[31:0]"}
        with open(input_file, "r") as f:
            dict_var = json.load(f)
            automaton = dict_var["automaton"]
            metadata = dict_var["metadata"]
            self.__automaton = automaton
            self.__metadata = metadata
            print("Automaton \n" + str(automaton))
            print("Metadata \n" + str(metadata))

    def __getVerilogTemplate(self):
        file_text = ""
        with open("verilog.template", "r") as f:
            file_text = f.read(-1)
        return file_text

    def __buildTransitions(self):
        result = ""

        initial_pattern = """
        """
        case_pattern = (
            """
            {current_state}:begin
                {transitions}
            end""")
        transition_pattern = """
                {condition_stm} (inner_{var_name}_in {operator} {value}) begin
                    state_next = {next_state};
                    {edit_action}
                end"""
        final_transition_pattern = """
                else begin
                    state_next = {next_state};
                    aux_inner_{var_name}_out = {value};
                end"""
        for state in self.__automaton["states"]:
            print("State " + str(state))
            transitions = ""
            num_transitions = len(self.__automaton["states"][state]["transitions"])
            count = 0
            final_var = None
            for transition in self.__automaton["states"][state]["transitions"]:
                # print("Transition " + str(transition))
                next_state = transition["next_state"]
                variable = transition["variable"]
                operator = transition["operator"]
                value = transition["value"]
                if self.__metadata["variables"][variable]["i/oType"] == "end_cycle":
                    # AN END CYCLE TRANSITION has no other transition
                    transitions += (
                        """
                    next_scan_cycle = {next_state};
                    state_next = IDLE;"""
                    #\n\t\t\t\t\tstart = 'b0;"""
                    ).format(next_state=next_state)
                elif value == "-":
                    if self.__metadata["variables"][variable]["i/oType"] == "input":
                        transitions += "state_next = {next_state};".format(next_state=next_state)
                    else:
                        edit_action = "aux_inner_{var_name}_out = inner_{var_name}_in;".format(
                            var_name=variable,
                            value=value)
                        transitions += "state_next = {next_state}; {edit_action}".format(
                            next_state=next_state,
                            edit_action=edit_action)
                elif self.__metadata["variables"][variable]["i/oType"] == "input":
                    transitions += transition_pattern.format(
                        condition_stm="else if" if count > 0 else "if",
                        var_name=variable,
                        operator=operator,
                        value = value,
                        next_state=next_state,
                        edit_action="")
                else:
                    final_var = variable
                    final_value = value
                    final_next_state = next_state
                    transitions += transition_pattern.format(
                        condition_stm="else if" if count > 0 else "if",
                        var_name=variable,
                        operator=operator,
                        value=value,
                        next_state=next_state,
                        edit_action="aux_inner_{var_name}_out = {value};".format(
                            var_name=variable,
                            value=value))
                count +=1
                """edit_action = transition["edit_action"]
                print("Transition " + str(transition))
                transitions += transition_pattern.format(
                    condition_stm="else if" if count > 0 else "if",
                    label=label,
                    next_state=next_state,
                    edit_action=edit_action+";" if edit_action is not "" else "")
                """
            if final_var is not None:
                transitions += final_transition_pattern.format(
                    var_name=final_var,
                    next_state=final_next_state,
                    value=final_value
                )
            result += case_pattern.format(current_state=state,
                                          transitions=transitions)

        return result

    def __buildGlobalSatesDeclaration(self):
        result = ""
        pattern = "localparam {name} = {value}; \n"
        count = 1
        for s in self.__metadata["states"]:
            result += pattern.format(name=s, value=count)
            count += 1
        return result

    def __buildStateDeclarations(self):
        result_dict = {}
        global_states_declaration = self.__buildGlobalSatesDeclaration()

        num_states = len(self.__metadata["states"])
        size_states = math.ceil(math.log2(num_states))
        size_states_str = "[" + str(size_states) + ":0]"

        result_dict.update({
            "_global_states_declaration": global_states_declaration,
            "_size_states": size_states_str})
            #"_state_initialization": "`initial_state"})

        return result_dict

    def __getPolicyName(self):
        return self.__metadata["policy_name"]

    def __buidlVariablesDeclarationAndUsage(self):
        result_dict = {}

        outer_variables_declaration_pattern = (
            """
input wire {type} outer_{var_name}_in,
output wire {type} outer_{var_name}_out,"""
        )
        outer_private_variables_declaration_pattern = (
            """
reg {type} outer_pre_{var_name}_in;
reg {type} outer_store_{var_name}_in;"""
        )
        outer_pre_assignment_pattern = (
            """\t\touter_pre_{var_name}_in = outer_store_{var_name}_in;\n"""
        )
        outer_store_assignment_pattern = (
            """\t\touter_store_{var_name}_in = outer_{var_name}_in;\n"""
        )
        inner_variables_declaration_inputs_pattern = (
            """
input  wire {type} inner_{var_name}_in,"""
        )
        inner_variables_declaration_outputs_pattern = (
            """
input  wire {type} inner_{var_name}_in,
output wire {type} inner_{var_name}_out,"""
        )
        combinatorial_wiring_inputs_pattern = (
            """
.inner_{var_name}_in(outer_pre_{var_name}_in),"""
        )
        combinatorial_wiring_outputs_pattern = (
            """
.inner_{var_name}_in(outer_{var_name}_in),
.inner_{var_name}_out(outer_{var_name}_out),"""
        )
        inner_reg_variables_declaration_pattern = (
            """reg {type} aux_inner_{var_name}_out;
reg {type} to_out_inner_{var_name}_out = {init};
"""
        )

        inner_variables_assignment_pattern_output_type = (
            """
assign  inner_{var_name}_out = to_out_inner_{var_name}_out;"""
        )
        inner_variables_assignment_pattern_to_out_type = (
            """
        to_out_inner_{var_name}_out = aux_inner_{var_name}_out;"""
        )
        outer_variables_assignment_pattern_output_type = (
            """
assign  outer_{var_name}_out = outer_{var_name}_in;"""
        )
        outer_variables_result = ""
        outer_private_variables_result = ""
        outer_pre_assignment_result = ""
        outer_store_assignment_result = ""
        inner_variables_declaration_inputs_result = ""
        inner_variables_declaration_outputs_result = ""
        combinatorial_wiring_inputs_result = ""
        combinatorial_wiring_outputs_result = ""
        outer_variables_assignment_result = ""
        inner_reg_variables_declaration_result = ""
        inner_variables_assignment_result = ""
        inner_variables_assignment_to_out_result = ""
        for v in self.__metadata["variables"]:
            if self.__metadata["variables"][v]["i/oType"] == "end_cycle":
                continue
            s = outer_variables_declaration_pattern.format(
                type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                var_name=v
            )
            outer_variables_result += s
            if self.__metadata["variables"][v]["i/oType"] == "output":

                s = inner_variables_declaration_outputs_pattern.format(
                    type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                    var_name=v
                )
                inner_variables_declaration_outputs_result += s

                s = combinatorial_wiring_outputs_pattern.format(
                    var_name=v
                )
                combinatorial_wiring_outputs_result += s

                s = inner_reg_variables_declaration_pattern.format(
                    type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                    var_name=v,
                    init = self.__metadata["variables"][v]["init"]
                )
                inner_reg_variables_declaration_result += s

                s = inner_variables_assignment_pattern_output_type.format(
                    var_name=v
                )
                inner_variables_assignment_result += s

                s = inner_variables_assignment_pattern_to_out_type.format(
                    var_name=v
                )
                inner_variables_assignment_to_out_result += s

                # IF INPUT
            else:
                s = inner_variables_declaration_inputs_pattern.format(
                    type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                    var_name=v
                )
                inner_variables_declaration_inputs_result += s

                s = outer_private_variables_declaration_pattern.format(
                    type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                    var_name=v
                )
                outer_private_variables_result += s

                s = outer_pre_assignment_pattern.format(
                    var_name=v
                )
                outer_pre_assignment_result +=s

                s = outer_store_assignment_pattern.format(
                    var_name=v
                )
                outer_store_assignment_result +=s

                s = combinatorial_wiring_inputs_pattern.format(
                    var_name=v
                )
                combinatorial_wiring_inputs_result += s

                s = outer_variables_assignment_pattern_output_type.format(
                    var_name=v
                )
                outer_variables_assignment_result += s

        result_dict.update(
            {
                "_outer_variables_declaration": outer_variables_result,
                "_outer_private_variables_declaration": outer_private_variables_result,
                "_outer_pre_assignment": outer_pre_assignment_result,
                "_outer_store_assignment": outer_store_assignment_result,
                "_inner_variables_inputs_declaration": inner_variables_declaration_inputs_result,
                "_inner_variables_outputs_declaration": inner_variables_declaration_outputs_result,
                "_combinatorial_wiring_inputs": combinatorial_wiring_inputs_result,
                "_combinatorial_wiring_outputs": combinatorial_wiring_outputs_result,
                "_inner_reg_variables_declaration": inner_reg_variables_declaration_result,
                "_inner_variables_assignment": inner_variables_assignment_result,
                "_inner_variables_assignment_to_out": inner_variables_assignment_to_out_result,
                "_outer_variables_assignment": outer_variables_assignment_result,
            }
        )
        return result_dict

    def __buildTester(self):
        result_dict = {}

        # if (!$feof(file_in)) begin
        # r = $fscanf(file_in, "%b,%b\n", o_var_in, expected);

        test_variables_declaration_pattern = (
            """
reg {type} test_{var_name}_in;
wire {type} test_{var_name}_out;"""
        )
        test_wiring_pattern = (
            """
.outer_{var_name}_in(test_{var_name}_in),
.outer_{var_name}_out(test_{var_name}_out),"""
        )
        scan_variables_pattern = (
            """ "{format_scan}\\n", {params_scan}"""
        )
        write_variables_pattern = (
            """ "{format_write}\\n", {params_write}"""
        )
        test_variables_result = ""
        test_wiring_result = ""
        format_scan_result = ""
        params_write_result = ""
        format_write_result = ""
        params_scan_result = ""
        for v in self.__metadata["variables"]:
            if self.__metadata["variables"][v]["i/oType"] == "end_cycle":
                continue
            s = test_variables_declaration_pattern.format(
                type=self.PLCTypeToVerilogType[self.__metadata["variables"][v]["type"]],
                var_name=v
            )
            test_variables_result += s

            s = test_wiring_pattern.format(
                var_name=v
            )
            test_wiring_result += s

            # TODO add typecheck
            format_scan_result += "%b,"
            # TODO add expced variables part
            params_scan_result += "test_{var_name}_in,".format(var_name=v)
            # TODO add typecheck
            format_write_result += "%b,"
            # TODO add expced variables part
            params_write_result += "test_{var_name}_out,".format(var_name=v)

        scan_variables_result = scan_variables_pattern.format(
            format_scan=format_scan_result[:-1],
            params_scan=params_scan_result[:-1])

        write_variables_result = write_variables_pattern.format(
            format_write=format_scan_result[:-1],
            params_write=params_scan_result[:-1])
        result_dict.update(
            {
                "_test_variables_declaration": test_variables_result,
                "_test_wiring": test_wiring_result,
                "_scan_variables": scan_variables_result,
                "_write_variables": write_variables_result

            })
        return result_dict

    def compile(self):
        args = {}
        transitions = self.__buildTransitions()
        policy_name = self.__getPolicyName()
        args.update({"_policy_name": policy_name,
                     "_transitions": transitions}
                    )

        state_declaration_dict = self.__buildStateDeclarations()
        args.update(state_declaration_dict)
        variables_dict = self.__buidlVariablesDeclarationAndUsage()
        args.update(variables_dict)

        tester_dict = self.__buildTester()
        args.update(tester_dict)
        verilog_template = self.__getVerilogTemplate()

        output_string = verilog_template.format(**args)
        """
        output_string = verilog_template.format(_policy_name=policy_name,
                                                _global_states_declaration=global_states_declaration,
                                                _transitions=transitions)
        """
        with open("../" + policy_name + ".v", "w") as f:
            f.write(output_string)


def main(argv):
    policy_dir = "../policy_automaton/"
    input_file = policy_dir + argv[1]
    compiler = Compiler(input_file)
    compiler.compile()


if __name__ == "__main__":
    main(sys.argv)
