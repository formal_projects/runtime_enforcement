grammar Policy;

globalProperty  : '(' globalProperty ')' | one=local | left=globalProperty OP_INTERSECT right=globalProperty;
local		    : '(' local ')' | OP_UNION '[' atomic local (',' atomic local)+ ']'| left=local OP_INTERSECT right=local | left=local OP_CONCAT right=local | EPS | pre=atomic sub=local;
atomic          :  variable=EXPR OPERATOR value=EXPR |'(' variable=EXPR OPERATOR value=EXPR ')' | END  ;


OP_CONCAT 	: ';' ;
OP_UNION	: '|' ;
OP_INTERSECT	: '∩' ;

fragment Digit      : '0'..'9' ;
fragment AlphaUnd   : '_' | 'a'..'z';
fragment Alpha      : 'a'..'z';

EPS 		: '$' ;
OPERATOR    : '<' | '>' | '<=' | '>=' | '==' ;

EXPR        : ( AlphaUnd | Digit)+ | '-';
END         : 'END';
WHITESPACE 	: (' ' | '.' | '|' | ',' | '(' | ')' )-> skip ;
