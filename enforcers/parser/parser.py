from antlr4 import *
from PolicyLexer import PolicyLexer
from PolicyParser import PolicyParser
import sys
import json


class AtomicNode():
    def __init__(self, variable=None, operator=None, value=None):
        print("Atomic node: " + str(variable) + " " + str(operator) + " " + str(value))
        self.variable = str(variable)
        self.operator = str(operator)
        self.value = str(value)


class UnionNode():
    def __init__(self):
        self.elements = list()

    def addElement(self, first, second):
        self.elements.append((first, second))

    def getElements(self):
        return self.elements


class PrefixNode():
    def __init__(self, pre=None, sub=None):
        self.pre = pre
        self.sub = sub


class ConcatNode():
    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right


class IntersectNode():
    def __init__(self, left=None, right=None):
        self.left = left
        self.right = right


class PolicyVisitor(ParseTreeVisitor):

    def visitAtomic(self, ctx: PolicyParser.AtomicContext):
        if ctx.END():
            return AtomicNode(variable=ctx.END())
        else:
            return AtomicNode(ctx.variable.text, ctx.OPERATOR(), ctx.value.text)


    def visitGlobalPropetty(self, ctx: PolicyParser.GlobalPropertyContext):
        print("Global Node: " + str(ctx.getText()))

        node = None

        if ctx.OP_INTERSECT():
            print("Intersect global")
            left = self.visit(ctx.left)
            right = self.visit(ctx.right)
            node = IntersectNode(left, right)
        else:
            str_aux = ctx.getText()
            if(str_aux[0] == '('):
                node = self.visit(ctx.getChild(1))
            else:
                node = self.visit(ctx.one)
        return node

    def visitLocal(self, ctx: PolicyParser.LocalContext):
        print("Local Node: " + str(ctx.getText()))

        node = None

        if ctx.OP_UNION():
            print("Union")

            node = UnionNode()
            i = 0
            while i < ctx.getChildCount():

                if str(ctx.getChild(i).getText()) in [',', ']', '[', '|']:
                    pass
                else:
                    first = self.visit(ctx.getChild(i))
                    second = self.visit(ctx.getChild(i + 1))
                    node.addElement(first, second)

                    i += 1
                i += 1
        elif ctx.OP_INTERSECT():
            print("Intersect local")
            left = self.visit(ctx.left)
            right = self.visit(ctx.right)
            node = IntersectNode(left, right)
        elif ctx.OP_CONCAT():
            print("Concat")

            left = self.visit(ctx.left)
            right = self.visit(ctx.right)
            node = ConcatNode(left, right)
        elif ctx.EPS():
            print("EPS")
            return None
        elif isinstance(ctx.getChild(0), PolicyParser.AtomicContext):
            print("Prefix")
            pre = self.visit(ctx.pre)
            sub = self.visit(ctx.sub)
            node = PrefixNode(pre, sub)

        else:
            print("Parenthesis")
            print("Node: " + str(ctx.getText()))
            node = self.visit(ctx.getChild(1))

        return node



class AutomataBuilder():
    def __init__(self, automaton_template, metadata):
        self.__automaton = automaton_template
        self.__metadata = metadata
        self.__state_count = 0

    # TODO maybe useless
    def __in_automaton(self, state):
        return state in self.__automaton["states"]

    def __get_new_state(self):
        self.__state_count += 1
        state = "s_" + str(self.__state_count)
        return state

    def __addTransition(self, variable=None, operator=None, value=None,
                        current_state=None, next_state=None):

        if not self.__in_automaton(current_state):
            self.__automaton["states"][current_state] = dict(transitions=list())

        transition = dict(next_state=next_state,
                          variable=variable,
                          operator=operator,
                          value=value)

        self.__automaton["states"][current_state]["transitions"].append(transition)


    def __build_aux(self, node, current_state, next_state=None, eps_state=None):
        if type(node) == AtomicNode:
            print("Atomic Node")
            print("" + str(node.variable) + "" + str(node.operator) +""+str(node.value))
            self.__addTransition(variable=node.variable,
                                 operator=node.operator,
                                 value=node.value,
                                 current_state=current_state,
                                 next_state=next_state)


        elif type(node) == PrefixNode:
            print("Prefix Node")
            pre = node.pre
            sub = node.sub
            if sub == None:
                self.__build_aux(node=pre,
                                 current_state=current_state,
                                 next_state=eps_state)
            else:
                next_state = self.__get_new_state()
                self.__build_aux(node=pre,
                                 current_state=current_state,
                                 next_state=next_state,
                                 eps_state=eps_state)
                current_state = next_state
                next_state = self.__get_new_state()
                self.__build_aux(node=sub,
                                 current_state=current_state,
                                 next_state=next_state,
                                 eps_state=eps_state)

        elif type(node) == UnionNode:
            print("Union Node")
            for e in node.getElements():
                head = e[0]
                tail = e[1]
                if tail == None:
                    self.__build_aux(node=head,
                                     current_state=current_state,
                                     next_state=eps_state)
                else:
                    next_state = self.__get_new_state()
                    self.__build_aux(node=head,
                                     current_state=current_state,
                                     next_state=next_state,
                                     eps_state=eps_state)
                    next_current_state = next_state
                    next_next_state = self.__get_new_state()
                    self.__build_aux(node=tail,
                                     current_state=next_current_state,
                                     next_state=next_next_state,
                                     eps_state=eps_state)


        elif type(node) == ConcatNode:
            print("Concat Node")
            left = node.left
            right = node.right
            next_state = self.__get_new_state()
            new_eps_state = self.__get_new_state()
            self.__build_aux(node=left,
                             current_state=current_state,
                             next_state=next_state,
                             eps_state=new_eps_state)

            new_next_state = self.__get_new_state()
            self.__build_aux(node=right,
                             current_state=new_eps_state,
                             next_state=new_next_state,
                             eps_state=eps_state)

        elif(type(node) == IntersectNode):
            left = node.left
            right = node.right
            self.__build_aux(left,
                             current_state,
                             next_state,
                             eps_state)

        return



    def build(self, node):
        next_state = self.__get_new_state()
        eps_state = self.__get_new_state()
        self.__build_aux(node=node,
                         current_state="initial_state",
                         next_state="initial_state",
                         eps_state="initial_state")

        self.__metadata["states"].extend(self.__automaton["states"].keys())
        result = {"metadata": self.__metadata, "automaton": self.__automaton}
        result = json.dumps(result, indent=2)
        with open("../policy_automaton/"+'automaton.json', 'w') as outfile:
            outfile.write(result)
        return result


automaton_template = {
    "states":
        {"initial_state":
            {"transitions":
                [
                    # {"label": "none", "next_state": "none", "variable":"none"} # TODO should be variables
                ]
            }
        }
}

metadata_template = {
    "policy_name" : "Name",
    "states": [],
    "variables": {
        "a" : {"type" : "Bool", "i/oType" : "input" },
        "c" : {"type" : "Bool", "i/oType" : "input" },
        "b" : {"type" : "Bool", "i/oType" : "output"},
        "d" : {"type" : "Bool", "i/oType" : "output"}
    }
}


def main(argv):
    input_json = None
    input_file = argv[1]
    with open(input_file,"r") as f:
        input_json = json.load(f)
    policy = input_json["policy"]
    metadata = input_json["metadata"]
    lexer = PolicyLexer(InputStream(policy))
    stream = CommonTokenStream(lexer)
    parser = PolicyParser(stream)
    tree = parser.local()
    ast_root = PolicyVisitor().visit(tree)

    print("Building automaton: ")
    AutomataBuilder(automaton_template, metadata).build(ast_root)


if __name__ == '__main__':
    x = 1000000000
    sys.setrecursionlimit(x)
    main(sys.argv)
# antlr4 -Dlanguage=Python3 Policy.g4 -visitor
# pip install antlr4-python3-runtime
