/*
States declaration of combinatorial module
*/
`timescale 10ms/1ms
//timescale 1 ns/10 ps




module prop_PLC1_combinatorial(
/*
Input/Output ports
*/
//for each variable

input  wire [31:0] inner_level_in,
input  wire [31:0] inner_req_in,

input  wire [31:0] inner_pump1_in,
output wire [31:0] inner_pump1_out,
input  wire [31:0] inner_pump2_in,
output wire [31:0] inner_pump2_out,
input  wire [31:0] inner_valve_in,
output wire [31:0] inner_valve_out,
input signal_start,
input wire clk
);
localparam IDLE = 0;
localparam initial_state = 1; 
localparam s_3 = 2; 
localparam s_5 = 3; 
localparam s_7 = 4; 
localparam s_9 = 5; 
localparam s_11 = 6; 
localparam s_13 = 7; 
localparam s_15 = 8; 
localparam s_17 = 9; 
localparam s_19 = 10; 
localparam s_21 = 11; 
localparam s_23 = 12; 
localparam s_25 = 13; 
localparam s_27 = 14; 
localparam s_29 = 15; 
localparam s_31 = 16; 
localparam s_33 = 17; 
localparam s_35 = 18; 
localparam s_37 = 19; 
localparam s_39 = 20; 
localparam s_41 = 21; 
localparam s_43 = 22; 
localparam s_45 = 23; 
localparam s_47 = 24; 
localparam s_49 = 25; 
localparam s_51 = 26; 
localparam s_53 = 27; 
localparam s_55 = 28; 
localparam s_57 = 29; 
localparam s_59 = 30; 
localparam s_61 = 31; 
localparam s_63 = 32; 
localparam s_65 = 33; 
localparam s_67 = 34; 
localparam s_69 = 35; 
localparam s_71 = 36; 
localparam s_73 = 37; 
localparam s_75 = 38; 
localparam s_77 = 39; 
localparam s_79 = 40; 
localparam s_81 = 41; 
localparam s_83 = 42; 
localparam s_85 = 43; 
localparam s_87 = 44; 
localparam s_89 = 45; 
localparam s_91 = 46; 
localparam s_93 = 47; 
localparam s_95 = 48; 
localparam s_97 = 49; 
localparam s_99 = 50; 
localparam s_101 = 51; 
localparam s_103 = 52; 
localparam s_105 = 53; 
localparam s_107 = 54; 
localparam s_109 = 55; 
localparam s_111 = 56; 
localparam s_113 = 57; 
localparam s_115 = 58; 
localparam s_117 = 59; 
localparam s_119 = 60; 
localparam s_121 = 61; 
localparam s_123 = 62; 
localparam s_125 = 63; 
localparam s_127 = 64; 
localparam s_129 = 65; 
localparam s_131 = 66; 
localparam s_133 = 67; 
localparam s_135 = 68; 
localparam s_137 = 69; 
localparam s_139 = 70; 
localparam s_141 = 71; 
localparam s_143 = 72; 
localparam s_145 = 73; 
localparam s_147 = 74; 
localparam s_149 = 75; 
localparam s_151 = 76; 
localparam s_153 = 77; 
localparam s_155 = 78; 
localparam s_157 = 79; 
localparam s_159 = 80; 
localparam s_161 = 81; 
localparam s_163 = 82; 
localparam s_165 = 83; 
localparam s_167 = 84; 
localparam s_169 = 85; 
localparam s_171 = 86; 
localparam s_173 = 87; 
localparam s_175 = 88; 
localparam s_177 = 89; 
localparam s_179 = 90; 
localparam s_181 = 91; 
localparam s_183 = 92; 
localparam s_185 = 93; 
localparam s_187 = 94; 
localparam s_189 = 95; 
localparam s_191 = 96; 
localparam s_193 = 97; 
localparam s_195 = 98; 
localparam s_197 = 99; 
localparam s_199 = 100; 
localparam s_201 = 101; 
localparam s_203 = 102; 
localparam s_205 = 103; 
localparam s_207 = 104; 
localparam s_209 = 105; 
localparam s_211 = 106; 
localparam s_213 = 107; 
localparam s_215 = 108; 
localparam s_217 = 109; 
localparam s_219 = 110; 
localparam s_221 = 111; 
localparam s_223 = 112; 
localparam s_225 = 113; 
localparam s_227 = 114; 
localparam s_229 = 115; 
localparam s_231 = 116; 
localparam s_233 = 117; 
localparam s_235 = 118; 
localparam s_237 = 119; 
localparam s_239 = 120; 
localparam s_241 = 121; 
localparam s_243 = 122; 
localparam s_245 = 123; 
localparam s_247 = 124; 
localparam s_249 = 125; 
localparam s_251 = 126; 
localparam s_253 = 127; 
localparam s_255 = 128; 
localparam s_257 = 129; 
localparam s_259 = 130; 
localparam s_261 = 131; 
localparam s_263 = 132; 
localparam s_265 = 133; 
localparam s_267 = 134; 
localparam s_269 = 135; 
localparam s_271 = 136; 
localparam s_273 = 137; 
localparam s_275 = 138; 
localparam s_277 = 139; 
localparam s_279 = 140; 
localparam s_281 = 141; 
localparam s_283 = 142; 
localparam s_285 = 143; 
localparam s_287 = 144; 
localparam s_289 = 145; 
localparam s_291 = 146; 
localparam s_293 = 147; 
localparam s_295 = 148; 
localparam s_297 = 149; 
localparam s_299 = 150; 
localparam s_301 = 151; 
localparam s_303 = 152; 
localparam s_305 = 153; 
localparam s_307 = 154; 
localparam s_309 = 155; 
localparam s_311 = 156; 
localparam s_313 = 157; 
localparam s_315 = 158; 
localparam s_317 = 159; 
localparam s_319 = 160; 
localparam s_321 = 161; 
localparam s_323 = 162; 
localparam s_325 = 163; 
localparam s_327 = 164; 
localparam s_329 = 165; 
localparam s_331 = 166; 
localparam s_333 = 167; 
localparam s_335 = 168; 
localparam s_337 = 169; 
localparam s_339 = 170; 
localparam s_341 = 171; 
localparam s_343 = 172; 
localparam s_345 = 173; 
localparam s_347 = 174; 
localparam s_349 = 175; 
localparam s_351 = 176; 
localparam s_353 = 177; 
localparam s_355 = 178; 
localparam s_357 = 179; 
localparam s_359 = 180; 
localparam s_361 = 181; 
localparam s_363 = 182; 
localparam s_365 = 183; 
localparam s_367 = 184; 
localparam s_369 = 185; 
localparam s_371 = 186; 
localparam s_373 = 187; 
localparam s_375 = 188; 
localparam s_377 = 189; 
localparam s_379 = 190; 
localparam s_381 = 191; 
localparam s_383 = 192; 
localparam s_385 = 193; 
localparam s_387 = 194; 
localparam s_389 = 195; 
localparam s_391 = 196; 
localparam s_393 = 197; 
localparam s_395 = 198; 
localparam s_397 = 199; 
localparam s_399 = 200; 
localparam s_401 = 201; 
localparam s_403 = 202; 
localparam s_405 = 203; 
localparam s_407 = 204; 
localparam s_409 = 205; 
localparam s_411 = 206; 
localparam s_413 = 207; 
localparam s_415 = 208; 
localparam s_417 = 209; 
localparam s_419 = 210; 
localparam s_421 = 211; 
localparam s_423 = 212; 
localparam s_425 = 213; 
localparam s_427 = 214; 
localparam s_429 = 215; 
localparam s_431 = 216; 
localparam s_433 = 217; 
localparam s_435 = 218; 
localparam s_437 = 219; 
localparam s_439 = 220; 
localparam s_441 = 221; 
localparam s_443 = 222; 
localparam s_445 = 223; 
localparam s_447 = 224; 
localparam s_449 = 225; 
localparam s_451 = 226; 
localparam s_453 = 227; 
localparam s_455 = 228; 
localparam s_457 = 229; 
localparam s_459 = 230; 
localparam s_461 = 231; 
localparam s_463 = 232; 
localparam s_465 = 233; 
localparam s_467 = 234; 
localparam s_469 = 235; 
localparam s_471 = 236; 
localparam s_473 = 237; 
localparam s_475 = 238; 
localparam s_477 = 239; 
localparam s_479 = 240; 
localparam s_481 = 241; 
localparam s_483 = 242; 
localparam s_485 = 243; 
localparam s_487 = 244; 
localparam s_489 = 245; 
localparam s_491 = 246; 
localparam s_493 = 247; 
localparam s_495 = 248; 
localparam s_497 = 249; 
localparam s_499 = 250; 
localparam s_501 = 251; 
localparam s_503 = 252; 
localparam s_505 = 253; 
localparam s_507 = 254; 
localparam s_509 = 255; 
localparam s_511 = 256; 
localparam s_513 = 257; 
localparam s_515 = 258; 
localparam s_517 = 259; 
localparam s_519 = 260; 
localparam s_521 = 261; 
localparam s_523 = 262; 
localparam s_525 = 263; 
localparam s_527 = 264; 
localparam s_529 = 265; 
localparam s_531 = 266; 
localparam s_533 = 267; 
localparam s_535 = 268; 
localparam s_537 = 269; 
localparam s_539 = 270; 
localparam s_541 = 271; 
localparam s_543 = 272; 
localparam s_545 = 273; 
localparam s_547 = 274; 
localparam s_549 = 275; 
localparam s_551 = 276; 
localparam s_553 = 277; 
localparam s_555 = 278; 
localparam s_557 = 279; 
localparam s_559 = 280; 
localparam s_561 = 281; 
localparam s_563 = 282; 
localparam s_565 = 283; 
localparam s_567 = 284; 
localparam s_569 = 285; 
localparam s_571 = 286; 
localparam s_573 = 287; 
localparam s_575 = 288; 
localparam s_577 = 289; 
localparam s_579 = 290; 
localparam s_581 = 291; 
localparam s_583 = 292; 
localparam s_585 = 293; 
localparam s_587 = 294; 
localparam s_589 = 295; 
localparam s_591 = 296; 
localparam s_593 = 297; 
localparam s_595 = 298; 
localparam s_597 = 299; 
localparam s_599 = 300; 
localparam s_601 = 301; 
localparam s_603 = 302; 
localparam s_605 = 303; 
localparam s_607 = 304; 
localparam s_609 = 305; 
localparam s_611 = 306; 
localparam s_613 = 307; 
localparam s_615 = 308; 
localparam s_617 = 309; 
localparam s_619 = 310; 
localparam s_621 = 311; 
localparam s_623 = 312; 
localparam s_625 = 313; 
localparam s_627 = 314; 
localparam s_629 = 315; 
localparam s_631 = 316; 
localparam s_633 = 317; 
localparam s_635 = 318; 
localparam s_637 = 319; 
localparam s_639 = 320; 
localparam s_641 = 321; 
localparam s_643 = 322; 
localparam s_645 = 323; 
localparam s_647 = 324; 
localparam s_649 = 325; 
localparam s_651 = 326; 
localparam s_653 = 327; 
localparam s_655 = 328; 
localparam s_657 = 329; 
localparam s_659 = 330; 
localparam s_661 = 331; 
localparam s_663 = 332; 
localparam s_665 = 333; 
localparam s_667 = 334; 
localparam s_669 = 335; 
localparam s_671 = 336; 
localparam s_673 = 337; 
localparam s_675 = 338; 
localparam s_677 = 339; 
localparam s_679 = 340; 
localparam s_681 = 341; 
localparam s_683 = 342; 
localparam s_685 = 343; 
localparam s_687 = 344; 
localparam s_689 = 345; 
localparam s_691 = 346; 
localparam s_693 = 347; 
localparam s_695 = 348; 
localparam s_697 = 349; 
localparam s_699 = 350; 
localparam s_701 = 351; 
localparam s_703 = 352; 
localparam s_705 = 353; 
localparam s_707 = 354; 
localparam s_709 = 355; 
localparam s_711 = 356; 
localparam s_713 = 357; 
localparam s_715 = 358; 
localparam s_717 = 359; 
localparam s_719 = 360; 
localparam s_721 = 361; 
localparam s_723 = 362; 
localparam s_725 = 363; 
localparam s_727 = 364; 
localparam s_729 = 365; 
localparam s_731 = 366; 
localparam s_733 = 367; 
localparam s_735 = 368; 
localparam s_737 = 369; 
localparam s_739 = 370; 
localparam s_741 = 371; 
localparam s_743 = 372; 
localparam s_745 = 373; 
localparam s_747 = 374; 
localparam s_749 = 375; 
localparam s_751 = 376; 
localparam s_753 = 377; 
localparam s_755 = 378; 
localparam s_757 = 379; 
localparam s_759 = 380; 
localparam s_761 = 381; 
localparam s_763 = 382; 
localparam s_765 = 383; 
localparam s_767 = 384; 
localparam s_769 = 385; 
localparam s_771 = 386; 
localparam s_773 = 387; 
localparam s_775 = 388; 
localparam s_777 = 389; 
localparam s_779 = 390; 
localparam s_781 = 391; 
localparam s_783 = 392; 
localparam s_785 = 393; 
localparam s_787 = 394; 
localparam s_789 = 395; 
localparam s_791 = 396; 
localparam s_793 = 397; 
localparam s_795 = 398; 
localparam s_797 = 399; 
localparam s_799 = 400; 
localparam s_801 = 401; 
localparam s_803 = 402; 
localparam s_805 = 403; 
localparam s_807 = 404; 
localparam s_809 = 405; 
localparam s_811 = 406; 
localparam s_813 = 407; 
localparam s_815 = 408; 
localparam s_817 = 409; 
localparam s_819 = 410; 
localparam s_821 = 411; 
localparam s_823 = 412; 
localparam s_825 = 413; 
localparam s_827 = 414; 
localparam s_829 = 415; 
localparam s_831 = 416; 
localparam s_833 = 417; 
localparam s_835 = 418; 
localparam s_837 = 419; 
localparam s_839 = 420; 
localparam s_841 = 421; 
localparam s_843 = 422; 
localparam s_845 = 423; 
localparam s_847 = 424; 
localparam s_849 = 425; 
localparam s_851 = 426; 
localparam s_853 = 427; 
localparam s_855 = 428; 
localparam s_857 = 429; 
localparam s_859 = 430; 
localparam s_861 = 431; 
localparam s_863 = 432; 
localparam s_865 = 433; 
localparam s_867 = 434; 
localparam s_869 = 435; 
localparam s_871 = 436; 
localparam s_873 = 437; 
localparam s_875 = 438; 
localparam s_877 = 439; 
localparam s_879 = 440; 
localparam s_881 = 441; 
localparam s_883 = 442; 
localparam s_885 = 443; 
localparam s_887 = 444; 
localparam s_889 = 445; 
localparam s_891 = 446; 
localparam s_893 = 447; 
localparam s_895 = 448; 
localparam s_897 = 449; 
localparam s_899 = 450; 
localparam s_901 = 451; 
localparam s_903 = 452; 
localparam s_905 = 453; 
localparam s_907 = 454; 
localparam s_909 = 455; 
localparam s_911 = 456; 
localparam s_913 = 457; 
localparam s_915 = 458; 
localparam s_917 = 459; 
localparam s_919 = 460; 
localparam s_921 = 461; 
localparam s_923 = 462; 
localparam s_925 = 463; 
localparam s_927 = 464; 
localparam s_929 = 465; 
localparam s_931 = 466; 
localparam s_933 = 467; 
localparam s_935 = 468; 
localparam s_937 = 469; 
localparam s_939 = 470; 
localparam s_941 = 471; 
localparam s_943 = 472; 
localparam s_945 = 473; 
localparam s_947 = 474; 
localparam s_949 = 475; 
localparam s_951 = 476; 
localparam s_953 = 477; 
localparam s_955 = 478; 
localparam s_957 = 479; 
localparam s_959 = 480; 
localparam s_961 = 481; 
localparam s_963 = 482; 
localparam s_965 = 483; 
localparam s_967 = 484; 
localparam s_969 = 485; 
localparam s_971 = 486; 
localparam s_973 = 487; 
localparam s_975 = 488; 
localparam s_977 = 489; 
localparam s_979 = 490; 
localparam s_981 = 491; 
localparam s_983 = 492; 
localparam s_985 = 493; 
localparam s_987 = 494; 
localparam s_989 = 495; 
localparam s_991 = 496; 
localparam s_993 = 497; 
localparam s_995 = 498; 
localparam s_997 = 499; 
localparam s_999 = 500; 
localparam s_1001 = 501; 
localparam s_1003 = 502; 
localparam s_1005 = 503; 
localparam s_1007 = 504; 
localparam s_1009 = 505; 
localparam s_1011 = 506; 
localparam s_1013 = 507; 
localparam s_1015 = 508; 
localparam s_1017 = 509; 
localparam s_1019 = 510; 
localparam s_1021 = 511; 
localparam s_1023 = 512; 
localparam s_1025 = 513; 
localparam s_1027 = 514; 
localparam s_1029 = 515; 
localparam s_1031 = 516; 
localparam s_1033 = 517; 
localparam s_1035 = 518; 
localparam s_1037 = 519; 
localparam s_1039 = 520; 
localparam s_1041 = 521; 
localparam s_1043 = 522; 
localparam s_1045 = 523; 
localparam s_1047 = 524; 
localparam s_1049 = 525; 
localparam s_1051 = 526; 
localparam s_1053 = 527; 
localparam s_1055 = 528; 
localparam s_1057 = 529; 
localparam s_1059 = 530; 
localparam s_1061 = 531; 
localparam s_1063 = 532; 
localparam s_1065 = 533; 
localparam s_1067 = 534; 
localparam s_1069 = 535; 
localparam s_1071 = 536; 
localparam s_1073 = 537; 
localparam s_1075 = 538; 
localparam s_1077 = 539; 
localparam s_1079 = 540; 
localparam s_1081 = 541; 
localparam s_1083 = 542; 
localparam s_1085 = 543; 
localparam s_1087 = 544; 
localparam s_1089 = 545; 
localparam s_1091 = 546; 
localparam s_1093 = 547; 
localparam s_1095 = 548; 
localparam s_1097 = 549; 
localparam s_1099 = 550; 
localparam s_1101 = 551; 
localparam s_1103 = 552; 
localparam s_1105 = 553; 
localparam s_1107 = 554; 
localparam s_1109 = 555; 
localparam s_1111 = 556; 
localparam s_1113 = 557; 
localparam s_1115 = 558; 
localparam s_1117 = 559; 
localparam s_1119 = 560; 
localparam s_1121 = 561; 
localparam s_1123 = 562; 
localparam s_1125 = 563; 
localparam s_1127 = 564; 
localparam s_1129 = 565; 
localparam s_1131 = 566; 
localparam s_1133 = 567; 
localparam s_1135 = 568; 
localparam s_1137 = 569; 
localparam s_1139 = 570; 
localparam s_1141 = 571; 
localparam s_1143 = 572; 
localparam s_1145 = 573; 
localparam s_1147 = 574; 
localparam s_1149 = 575; 
localparam s_1151 = 576; 
localparam s_1153 = 577; 
localparam s_1155 = 578; 
localparam s_1157 = 579; 
localparam s_1159 = 580; 
localparam s_1161 = 581; 
localparam s_1163 = 582; 
localparam s_1165 = 583; 
localparam s_1167 = 584; 
localparam s_1169 = 585; 
localparam s_1171 = 586; 
localparam s_1173 = 587; 
localparam s_1175 = 588; 
localparam s_1177 = 589; 
localparam s_1179 = 590; 
localparam s_1181 = 591; 
localparam s_1183 = 592; 
localparam s_1185 = 593; 
localparam s_1187 = 594; 
localparam s_1189 = 595; 
localparam s_1191 = 596; 
localparam s_1193 = 597; 
localparam s_1195 = 598; 
localparam s_1197 = 599; 
localparam s_1199 = 600; 
localparam s_1201 = 601; 
localparam s_1203 = 602; 
localparam s_1205 = 603; 
localparam s_1207 = 604; 
localparam s_1209 = 605; 
localparam s_1211 = 606; 
localparam s_1213 = 607; 
localparam s_1215 = 608; 
localparam s_1217 = 609; 
localparam s_1219 = 610; 
localparam s_1221 = 611; 
localparam s_1223 = 612; 
localparam s_1225 = 613; 
localparam s_1227 = 614; 
localparam s_1229 = 615; 
localparam s_1231 = 616; 
localparam s_1233 = 617; 
localparam s_1235 = 618; 
localparam s_1237 = 619; 
localparam s_1239 = 620; 
localparam s_1241 = 621; 
localparam s_1243 = 622; 
localparam s_1245 = 623; 
localparam s_1247 = 624; 
localparam s_1249 = 625; 
localparam s_1251 = 626; 
localparam s_1253 = 627; 
localparam s_1255 = 628; 
localparam s_1257 = 629; 
localparam s_1259 = 630; 
localparam s_1261 = 631; 
localparam s_1263 = 632; 
localparam s_1265 = 633; 
localparam s_1267 = 634; 
localparam s_1269 = 635; 
localparam s_1271 = 636; 
localparam s_1273 = 637; 
localparam s_1275 = 638; 
localparam s_1277 = 639; 
localparam s_1279 = 640; 
localparam s_1281 = 641; 
localparam s_1283 = 642; 
localparam s_1285 = 643; 
localparam s_1287 = 644; 
localparam s_1289 = 645; 
localparam s_1291 = 646; 
localparam s_1293 = 647; 
localparam s_1295 = 648; 
localparam s_1297 = 649; 
localparam s_1299 = 650; 
localparam s_1301 = 651; 
localparam s_1303 = 652; 
localparam s_1305 = 653; 
localparam s_1307 = 654; 
localparam s_1309 = 655; 
localparam s_1311 = 656; 
localparam s_1313 = 657; 
localparam s_1315 = 658; 
localparam s_1317 = 659; 
localparam s_1319 = 660; 
localparam s_1321 = 661; 
localparam s_1323 = 662; 
localparam s_1325 = 663; 
localparam s_1327 = 664; 
localparam s_1329 = 665; 
localparam s_1331 = 666; 
localparam s_1333 = 667; 
localparam s_1335 = 668; 
localparam s_1337 = 669; 
localparam s_1339 = 670; 
localparam s_1341 = 671; 
localparam s_1343 = 672; 
localparam s_1345 = 673; 
localparam s_1347 = 674; 
localparam s_1349 = 675; 
localparam s_1351 = 676; 
localparam s_1353 = 677; 
localparam s_1355 = 678; 
localparam s_1357 = 679; 
localparam s_1359 = 680; 
localparam s_1361 = 681; 
localparam s_1363 = 682; 
localparam s_1365 = 683; 
localparam s_1367 = 684; 
localparam s_1369 = 685; 
localparam s_1371 = 686; 
localparam s_1373 = 687; 
localparam s_1375 = 688; 
localparam s_1377 = 689; 
localparam s_1379 = 690; 
localparam s_1381 = 691; 
localparam s_1383 = 692; 
localparam s_1385 = 693; 
localparam s_1387 = 694; 
localparam s_1389 = 695; 
localparam s_1391 = 696; 
localparam s_1393 = 697; 
localparam s_1395 = 698; 
localparam s_1397 = 699; 
localparam s_1399 = 700; 
localparam s_1401 = 701; 
localparam s_1403 = 702; 
localparam s_1405 = 703; 
localparam s_1407 = 704; 
localparam s_1409 = 705; 
localparam s_1411 = 706; 
localparam s_1413 = 707; 
localparam s_1415 = 708; 
localparam s_1417 = 709; 
localparam s_1419 = 710; 
localparam s_1421 = 711; 
localparam s_1423 = 712; 
localparam s_1425 = 713; 
localparam s_1427 = 714; 
localparam s_1429 = 715; 
localparam s_1431 = 716; 
localparam s_1433 = 717; 
localparam s_1435 = 718; 
localparam s_1437 = 719; 
localparam s_1439 = 720; 
localparam s_1441 = 721; 
localparam s_1443 = 722; 
localparam s_1445 = 723; 
localparam s_1447 = 724; 
localparam s_1449 = 725; 
localparam s_1451 = 726; 
localparam s_1453 = 727; 
localparam s_1455 = 728; 
localparam s_1457 = 729; 
localparam s_1459 = 730; 
localparam s_1461 = 731; 
localparam s_1463 = 732; 
localparam s_1465 = 733; 
localparam s_1467 = 734; 
localparam s_1469 = 735; 
localparam s_1471 = 736; 
localparam s_1473 = 737; 
localparam s_1475 = 738; 
localparam s_1477 = 739; 
localparam s_1479 = 740; 
localparam s_1481 = 741; 
localparam s_1483 = 742; 
localparam s_1485 = 743; 
localparam s_1487 = 744; 
localparam s_1489 = 745; 
localparam s_1491 = 746; 
localparam s_1493 = 747; 
localparam s_1495 = 748; 
localparam s_1497 = 749; 
localparam s_1499 = 750; 
localparam s_1501 = 751; 
localparam s_1503 = 752; 
localparam s_1505 = 753; 
localparam s_1507 = 754; 
localparam s_1509 = 755; 
localparam s_1511 = 756; 
localparam s_1513 = 757; 
localparam s_1515 = 758; 
localparam s_1517 = 759; 
localparam s_1519 = 760; 
localparam s_1521 = 761; 
localparam s_1523 = 762; 
localparam s_1525 = 763; 
localparam s_1527 = 764; 
localparam s_1529 = 765; 
localparam s_1531 = 766; 
localparam s_1533 = 767; 
localparam s_1535 = 768; 
localparam s_1537 = 769; 
localparam s_1539 = 770; 
localparam s_1541 = 771; 
localparam s_1543 = 772; 
localparam s_1545 = 773; 
localparam s_1547 = 774; 
localparam s_1549 = 775; 
localparam s_1551 = 776; 
localparam s_1553 = 777; 
localparam s_1555 = 778; 
localparam s_1557 = 779; 
localparam s_1559 = 780; 
localparam s_1561 = 781; 
localparam s_1563 = 782; 
localparam s_1565 = 783; 
localparam s_1567 = 784; 
localparam s_1569 = 785; 
localparam s_1571 = 786; 
localparam s_1573 = 787; 
localparam s_1575 = 788; 
localparam s_1577 = 789; 
localparam s_1579 = 790; 
localparam s_1581 = 791; 
localparam s_1583 = 792; 
localparam s_1585 = 793; 
localparam s_1587 = 794; 
localparam s_1589 = 795; 
localparam s_1591 = 796; 
localparam s_1593 = 797; 
localparam s_1595 = 798; 
localparam s_1597 = 799; 
localparam s_1599 = 800; 
localparam s_1601 = 801; 
localparam s_1603 = 802; 
localparam s_1605 = 803; 
localparam s_1607 = 804; 
localparam s_1609 = 805; 
localparam s_1611 = 806; 
localparam s_1613 = 807; 
localparam s_1615 = 808; 
localparam s_1617 = 809; 
localparam s_1619 = 810; 
localparam s_1621 = 811; 
localparam s_1623 = 812; 
localparam s_1625 = 813; 
localparam s_1627 = 814; 
localparam s_1629 = 815; 
localparam s_1631 = 816; 
localparam s_1633 = 817; 
localparam s_1635 = 818; 
localparam s_1637 = 819; 
localparam s_1639 = 820; 
localparam s_1641 = 821; 
localparam s_1643 = 822; 
localparam s_1645 = 823; 
localparam s_1647 = 824; 
localparam s_1649 = 825; 
localparam s_1651 = 826; 
localparam s_1653 = 827; 
localparam s_1655 = 828; 
localparam s_1657 = 829; 
localparam s_1659 = 830; 
localparam s_1661 = 831; 
localparam s_1663 = 832; 
localparam s_1665 = 833; 
localparam s_1667 = 834; 
localparam s_1669 = 835; 
localparam s_1671 = 836; 
localparam s_1673 = 837; 
localparam s_1675 = 838; 
localparam s_1677 = 839; 
localparam s_1679 = 840; 
localparam s_1681 = 841; 
localparam s_1683 = 842; 
localparam s_1685 = 843; 
localparam s_1687 = 844; 
localparam s_1689 = 845; 
localparam s_1691 = 846; 
localparam s_1693 = 847; 
localparam s_1695 = 848; 
localparam s_1697 = 849; 
localparam s_1699 = 850; 
localparam s_1701 = 851; 
localparam s_1703 = 852; 
localparam s_1705 = 853; 
localparam s_1707 = 854; 
localparam s_1709 = 855; 
localparam s_1711 = 856; 
localparam s_1713 = 857; 
localparam s_1715 = 858; 
localparam s_1717 = 859; 
localparam s_1719 = 860; 
localparam s_1721 = 861; 
localparam s_1723 = 862; 
localparam s_1725 = 863; 
localparam s_1727 = 864; 
localparam s_1729 = 865; 
localparam s_1731 = 866; 
localparam s_1733 = 867; 
localparam s_1735 = 868; 
localparam s_1737 = 869; 
localparam s_1739 = 870; 
localparam s_1741 = 871; 
localparam s_1743 = 872; 
localparam s_1745 = 873; 
localparam s_1747 = 874; 
localparam s_1749 = 875; 
localparam s_1751 = 876; 
localparam s_1753 = 877; 
localparam s_1755 = 878; 
localparam s_1757 = 879; 
localparam s_1759 = 880; 
localparam s_1761 = 881; 
localparam s_1763 = 882; 
localparam s_1765 = 883; 
localparam s_1767 = 884; 
localparam s_1769 = 885; 
localparam s_1771 = 886; 
localparam s_1773 = 887; 
localparam s_1775 = 888; 
localparam s_1777 = 889; 
localparam s_1779 = 890; 
localparam s_1781 = 891; 
localparam s_1783 = 892; 
localparam s_1785 = 893; 
localparam s_1787 = 894; 
localparam s_1789 = 895; 
localparam s_1791 = 896; 
localparam s_1793 = 897; 
localparam s_1795 = 898; 
localparam s_1797 = 899; 
localparam s_1799 = 900; 
localparam s_1801 = 901; 
localparam s_1803 = 902; 
localparam s_1805 = 903; 
localparam s_1807 = 904; 
localparam s_1809 = 905; 
localparam s_1811 = 906; 
localparam s_1813 = 907; 
localparam s_1815 = 908; 
localparam s_1817 = 909; 
localparam s_1819 = 910; 
localparam s_1821 = 911; 
localparam s_1823 = 912; 
localparam s_1825 = 913; 
localparam s_1827 = 914; 
localparam s_1829 = 915; 
localparam s_1831 = 916; 
localparam s_1833 = 917; 
localparam s_1835 = 918; 
localparam s_1837 = 919; 
localparam s_1839 = 920; 
localparam s_1841 = 921; 
localparam s_1843 = 922; 
localparam s_1845 = 923; 
localparam s_1847 = 924; 
localparam s_1849 = 925; 
localparam s_1851 = 926; 
localparam s_1853 = 927; 
localparam s_1855 = 928; 
localparam s_1857 = 929; 
localparam s_1859 = 930; 
localparam s_1861 = 931; 
localparam s_1863 = 932; 
localparam s_1865 = 933; 
localparam s_1867 = 934; 
localparam s_1869 = 935; 
localparam s_1871 = 936; 
localparam s_1873 = 937; 
localparam s_1875 = 938; 
localparam s_1877 = 939; 
localparam s_1879 = 940; 
localparam s_1881 = 941; 
localparam s_1883 = 942; 
localparam s_1885 = 943; 
localparam s_1887 = 944; 
localparam s_1889 = 945; 
localparam s_1891 = 946; 
localparam s_1893 = 947; 
localparam s_1895 = 948; 
localparam s_1897 = 949; 
localparam s_1899 = 950; 
localparam s_1901 = 951; 
localparam s_1903 = 952; 
localparam s_1905 = 953; 
localparam s_1907 = 954; 
localparam s_1909 = 955; 
localparam s_1911 = 956; 
localparam s_1913 = 957; 
localparam s_1915 = 958; 
localparam s_1917 = 959; 
localparam s_1919 = 960; 
localparam s_1921 = 961; 
localparam s_1923 = 962; 
localparam s_1925 = 963; 
localparam s_1927 = 964; 
localparam s_1929 = 965; 
localparam s_1931 = 966; 
localparam s_1933 = 967; 
localparam s_1935 = 968; 
localparam s_1937 = 969; 
localparam s_1939 = 970; 
localparam s_1941 = 971; 
localparam s_1943 = 972; 
localparam s_1945 = 973; 
localparam s_1947 = 974; 
localparam s_1949 = 975; 
localparam s_1951 = 976; 
localparam s_1953 = 977; 
localparam s_1955 = 978; 
localparam s_1957 = 979; 
localparam s_1959 = 980; 
localparam s_1961 = 981; 
localparam s_1963 = 982; 
localparam s_1965 = 983; 
localparam s_1967 = 984; 
localparam s_1969 = 985; 
localparam s_1971 = 986; 
localparam s_1973 = 987; 
localparam s_1975 = 988; 
localparam s_1977 = 989; 
localparam s_1979 = 990; 
localparam s_1981 = 991; 
localparam s_1983 = 992; 
localparam s_1985 = 993; 
localparam s_1987 = 994; 
localparam s_1989 = 995; 
localparam s_1991 = 996; 
localparam s_1993 = 997; 
localparam s_1995 = 998; 
localparam s_1997 = 999; 
localparam s_1999 = 1000; 
localparam s_2001 = 1001; 
localparam s_2003 = 1002; 
localparam s_2005 = 1003; 
localparam s_2007 = 1004; 
localparam s_2009 = 1005; 
localparam s_2011 = 1006; 
localparam s_2013 = 1007; 
localparam s_2015 = 1008; 
localparam s_2017 = 1009; 
localparam s_2019 = 1010; 
localparam s_2021 = 1011; 
localparam s_2023 = 1012; 
localparam s_2025 = 1013; 
localparam s_2027 = 1014; 
localparam s_2029 = 1015; 
localparam s_2031 = 1016; 
localparam s_2033 = 1017; 
localparam s_2035 = 1018; 
localparam s_2037 = 1019; 
localparam s_2039 = 1020; 
localparam s_2041 = 1021; 
localparam s_2043 = 1022; 
localparam s_2045 = 1023; 
localparam s_2047 = 1024; 
localparam s_2049 = 1025; 
localparam s_2051 = 1026; 
localparam s_2053 = 1027; 
localparam s_2055 = 1028; 
localparam s_2057 = 1029; 
localparam s_2059 = 1030; 
localparam s_2061 = 1031; 
localparam s_2063 = 1032; 
localparam s_2065 = 1033; 
localparam s_2067 = 1034; 
localparam s_2069 = 1035; 
localparam s_2071 = 1036; 
localparam s_2073 = 1037; 
localparam s_2075 = 1038; 
localparam s_2077 = 1039; 
localparam s_2079 = 1040; 
localparam s_2081 = 1041; 
localparam s_2083 = 1042; 
localparam s_2085 = 1043; 
localparam s_2087 = 1044; 
localparam s_2089 = 1045; 
localparam s_2091 = 1046; 
localparam s_2093 = 1047; 
localparam s_2095 = 1048; 
localparam s_2097 = 1049; 
localparam s_2099 = 1050; 
localparam s_2101 = 1051; 
localparam s_2103 = 1052; 
localparam s_2105 = 1053; 
localparam s_2107 = 1054; 
localparam s_2109 = 1055; 
localparam s_2111 = 1056; 
localparam s_2113 = 1057; 
localparam s_2115 = 1058; 
localparam s_2117 = 1059; 
localparam s_2119 = 1060; 
localparam s_2121 = 1061; 
localparam s_2123 = 1062; 
localparam s_2125 = 1063; 
localparam s_2127 = 1064; 
localparam s_2129 = 1065; 
localparam s_2131 = 1066; 
localparam s_2133 = 1067; 
localparam s_2135 = 1068; 
localparam s_2137 = 1069; 
localparam s_2139 = 1070; 
localparam s_2141 = 1071; 
localparam s_2143 = 1072; 
localparam s_2145 = 1073; 
localparam s_2147 = 1074; 
localparam s_2149 = 1075; 
localparam s_2151 = 1076; 
localparam s_2153 = 1077; 
localparam s_2155 = 1078; 
localparam s_2157 = 1079; 
localparam s_2159 = 1080; 
localparam s_2161 = 1081; 
localparam s_2163 = 1082; 
localparam s_2165 = 1083; 
localparam s_2167 = 1084; 
localparam s_2169 = 1085; 
localparam s_2171 = 1086; 
localparam s_2173 = 1087; 
localparam s_2175 = 1088; 
localparam s_2177 = 1089; 
localparam s_2179 = 1090; 
localparam s_2181 = 1091; 
localparam s_2183 = 1092; 
localparam s_2185 = 1093; 
localparam s_2187 = 1094; 
localparam s_2189 = 1095; 
localparam s_2191 = 1096; 
localparam s_2193 = 1097; 
localparam s_2195 = 1098; 
localparam s_2197 = 1099; 
localparam s_2199 = 1100; 
localparam s_2201 = 1101; 
localparam s_2203 = 1102; 
localparam s_2205 = 1103; 
localparam s_2207 = 1104; 
localparam s_2209 = 1105; 
localparam s_2211 = 1106; 
localparam s_2213 = 1107; 
localparam s_2215 = 1108; 
localparam s_2217 = 1109; 
localparam s_2219 = 1110; 
localparam s_2221 = 1111; 
localparam s_2223 = 1112; 
localparam s_2225 = 1113; 
localparam s_2227 = 1114; 
localparam s_2229 = 1115; 
localparam s_2231 = 1116; 
localparam s_2233 = 1117; 
localparam s_2235 = 1118; 
localparam s_2237 = 1119; 
localparam s_2239 = 1120; 
localparam s_2241 = 1121; 
localparam s_2243 = 1122; 
localparam s_2245 = 1123; 
localparam s_2247 = 1124; 
localparam s_2249 = 1125; 
localparam s_2251 = 1126; 
localparam s_2253 = 1127; 
localparam s_2255 = 1128; 
localparam s_2257 = 1129; 
localparam s_2259 = 1130; 
localparam s_2261 = 1131; 
localparam s_2263 = 1132; 
localparam s_2265 = 1133; 
localparam s_2267 = 1134; 
localparam s_2269 = 1135; 
localparam s_2271 = 1136; 
localparam s_2273 = 1137; 
localparam s_2275 = 1138; 
localparam s_2277 = 1139; 
localparam s_2279 = 1140; 
localparam s_2281 = 1141; 
localparam s_2283 = 1142; 
localparam s_2285 = 1143; 
localparam s_2287 = 1144; 
localparam s_2289 = 1145; 
localparam s_2291 = 1146; 
localparam s_2293 = 1147; 
localparam s_2295 = 1148; 
localparam s_2297 = 1149; 
localparam s_2299 = 1150; 
localparam s_2301 = 1151; 
localparam s_2303 = 1152; 
localparam s_2305 = 1153; 
localparam s_2307 = 1154; 
localparam s_2309 = 1155; 
localparam s_2311 = 1156; 
localparam s_2313 = 1157; 
localparam s_2315 = 1158; 
localparam s_2317 = 1159; 
localparam s_2319 = 1160; 
localparam s_2321 = 1161; 
localparam s_2323 = 1162; 
localparam s_2325 = 1163; 
localparam s_2327 = 1164; 
localparam s_2329 = 1165; 
localparam s_2331 = 1166; 
localparam s_2333 = 1167; 
localparam s_2335 = 1168; 
localparam s_2337 = 1169; 
localparam s_2339 = 1170; 
localparam s_2341 = 1171; 
localparam s_2343 = 1172; 
localparam s_2345 = 1173; 
localparam s_2347 = 1174; 
localparam s_2349 = 1175; 
localparam s_2351 = 1176; 
localparam s_2353 = 1177; 
localparam s_2355 = 1178; 
localparam s_2357 = 1179; 
localparam s_2359 = 1180; 
localparam s_2361 = 1181; 
localparam s_2363 = 1182; 
localparam s_2365 = 1183; 
localparam s_2367 = 1184; 
localparam s_2369 = 1185; 
localparam s_2371 = 1186; 
localparam s_2373 = 1187; 
localparam s_2375 = 1188; 
localparam s_2377 = 1189; 
localparam s_2379 = 1190; 
localparam s_2381 = 1191; 
localparam s_2383 = 1192; 
localparam s_2385 = 1193; 
localparam s_2387 = 1194; 
localparam s_2389 = 1195; 
localparam s_2391 = 1196; 
localparam s_2393 = 1197; 
localparam s_2395 = 1198; 
localparam s_2397 = 1199; 
localparam s_2399 = 1200; 
localparam s_2401 = 1201; 
localparam s_2403 = 1202; 
localparam s_2405 = 1203; 
localparam s_2407 = 1204; 
localparam s_2409 = 1205; 
localparam s_2411 = 1206; 
localparam s_2413 = 1207; 

reg [11:0] state, state_next, next_scan_cycle;
reg [31:0] aux_inner_pump1_out;
reg [31:0] to_out_inner_pump1_out = 'd1;
reg [31:0] aux_inner_pump2_out;
reg [31:0] to_out_inner_pump2_out = 'd1;
reg [31:0] aux_inner_valve_out;
reg [31:0] to_out_inner_valve_out = 'd1;


initial begin
state = IDLE;
state_next = IDLE;
next_scan_cycle = initial_state;
end
always @(posedge clk) begin
    state <= state_next;
end

always @* begin

    case(state)
        IDLE : begin
            
        to_out_inner_pump1_out = aux_inner_pump1_out;
        to_out_inner_pump2_out = aux_inner_pump2_out;
        to_out_inner_valve_out = aux_inner_valve_out;
            if (signal_start == 'b1) begin
                state_next = next_scan_cycle;
            end
        end
        
            initial_state:begin
                state_next = s_3;
            end
            s_3:begin
                state_next = s_5;
            end
            s_5:begin
                state_next = s_7; aux_inner_pump1_out = inner_pump1_in;
            end
            s_7:begin
                state_next = s_9; aux_inner_pump2_out = inner_pump2_in;
            end
            s_9:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_11;
                    aux_inner_valve_out = 0;
                end
                else if (inner_valve_in == 1) begin
                    state_next = s_1213;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1213;
                    aux_inner_valve_out = 1;
                end
            end
            s_11:begin
                
                    next_scan_cycle = s_13;
                    state_next = IDLE;
            end
            s_13:begin
                state_next = s_15;
            end
            s_15:begin
                state_next = s_17;
            end
            s_17:begin
                state_next = s_19; aux_inner_pump1_out = inner_pump1_in;
            end
            s_19:begin
                state_next = s_21; aux_inner_pump2_out = inner_pump2_in;
            end
            s_21:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_23;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_23;
                    aux_inner_valve_out = 0;
                end
            end
            s_23:begin
                
                    next_scan_cycle = s_25;
                    state_next = IDLE;
            end
            s_25:begin
                state_next = s_27;
            end
            s_27:begin
                state_next = s_29;
            end
            s_29:begin
                state_next = s_31; aux_inner_pump1_out = inner_pump1_in;
            end
            s_31:begin
                state_next = s_33; aux_inner_pump2_out = inner_pump2_in;
            end
            s_33:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_35;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_35;
                    aux_inner_valve_out = 0;
                end
            end
            s_35:begin
                
                    next_scan_cycle = s_37;
                    state_next = IDLE;
            end
            s_37:begin
                state_next = s_39;
            end
            s_39:begin
                state_next = s_41;
            end
            s_41:begin
                state_next = s_43; aux_inner_pump1_out = inner_pump1_in;
            end
            s_43:begin
                state_next = s_45; aux_inner_pump2_out = inner_pump2_in;
            end
            s_45:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_47;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_47;
                    aux_inner_valve_out = 0;
                end
            end
            s_47:begin
                
                    next_scan_cycle = s_49;
                    state_next = IDLE;
            end
            s_49:begin
                state_next = s_51;
            end
            s_51:begin
                state_next = s_53;
            end
            s_53:begin
                state_next = s_55; aux_inner_pump1_out = inner_pump1_in;
            end
            s_55:begin
                state_next = s_57; aux_inner_pump2_out = inner_pump2_in;
            end
            s_57:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_59;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_59;
                    aux_inner_valve_out = 0;
                end
            end
            s_59:begin
                
                    next_scan_cycle = s_61;
                    state_next = IDLE;
            end
            s_61:begin
                state_next = s_63;
            end
            s_63:begin
                state_next = s_65;
            end
            s_65:begin
                state_next = s_67; aux_inner_pump1_out = inner_pump1_in;
            end
            s_67:begin
                state_next = s_69; aux_inner_pump2_out = inner_pump2_in;
            end
            s_69:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_71;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_71;
                    aux_inner_valve_out = 0;
                end
            end
            s_71:begin
                
                    next_scan_cycle = s_73;
                    state_next = IDLE;
            end
            s_73:begin
                state_next = s_75;
            end
            s_75:begin
                state_next = s_77;
            end
            s_77:begin
                state_next = s_79; aux_inner_pump1_out = inner_pump1_in;
            end
            s_79:begin
                state_next = s_81; aux_inner_pump2_out = inner_pump2_in;
            end
            s_81:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_83;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_83;
                    aux_inner_valve_out = 0;
                end
            end
            s_83:begin
                
                    next_scan_cycle = s_85;
                    state_next = IDLE;
            end
            s_85:begin
                state_next = s_87;
            end
            s_87:begin
                state_next = s_89;
            end
            s_89:begin
                state_next = s_91; aux_inner_pump1_out = inner_pump1_in;
            end
            s_91:begin
                state_next = s_93; aux_inner_pump2_out = inner_pump2_in;
            end
            s_93:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_95;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_95;
                    aux_inner_valve_out = 0;
                end
            end
            s_95:begin
                
                    next_scan_cycle = s_97;
                    state_next = IDLE;
            end
            s_97:begin
                state_next = s_99;
            end
            s_99:begin
                state_next = s_101;
            end
            s_101:begin
                state_next = s_103; aux_inner_pump1_out = inner_pump1_in;
            end
            s_103:begin
                state_next = s_105; aux_inner_pump2_out = inner_pump2_in;
            end
            s_105:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_107;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_107;
                    aux_inner_valve_out = 0;
                end
            end
            s_107:begin
                
                    next_scan_cycle = s_109;
                    state_next = IDLE;
            end
            s_109:begin
                state_next = s_111;
            end
            s_111:begin
                state_next = s_113;
            end
            s_113:begin
                state_next = s_115; aux_inner_pump1_out = inner_pump1_in;
            end
            s_115:begin
                state_next = s_117; aux_inner_pump2_out = inner_pump2_in;
            end
            s_117:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_119;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_119;
                    aux_inner_valve_out = 0;
                end
            end
            s_119:begin
                
                    next_scan_cycle = s_121;
                    state_next = IDLE;
            end
            s_121:begin
                state_next = s_123;
            end
            s_123:begin
                state_next = s_125;
            end
            s_125:begin
                state_next = s_127; aux_inner_pump1_out = inner_pump1_in;
            end
            s_127:begin
                state_next = s_129; aux_inner_pump2_out = inner_pump2_in;
            end
            s_129:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_131;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_131;
                    aux_inner_valve_out = 0;
                end
            end
            s_131:begin
                
                    next_scan_cycle = s_133;
                    state_next = IDLE;
            end
            s_133:begin
                state_next = s_135;
            end
            s_135:begin
                state_next = s_137;
            end
            s_137:begin
                state_next = s_139; aux_inner_pump1_out = inner_pump1_in;
            end
            s_139:begin
                state_next = s_141; aux_inner_pump2_out = inner_pump2_in;
            end
            s_141:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_143;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_143;
                    aux_inner_valve_out = 0;
                end
            end
            s_143:begin
                
                    next_scan_cycle = s_145;
                    state_next = IDLE;
            end
            s_145:begin
                state_next = s_147;
            end
            s_147:begin
                state_next = s_149;
            end
            s_149:begin
                state_next = s_151; aux_inner_pump1_out = inner_pump1_in;
            end
            s_151:begin
                state_next = s_153; aux_inner_pump2_out = inner_pump2_in;
            end
            s_153:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_155;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_155;
                    aux_inner_valve_out = 0;
                end
            end
            s_155:begin
                
                    next_scan_cycle = s_157;
                    state_next = IDLE;
            end
            s_157:begin
                state_next = s_159;
            end
            s_159:begin
                state_next = s_161;
            end
            s_161:begin
                state_next = s_163; aux_inner_pump1_out = inner_pump1_in;
            end
            s_163:begin
                state_next = s_165; aux_inner_pump2_out = inner_pump2_in;
            end
            s_165:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_167;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_167;
                    aux_inner_valve_out = 0;
                end
            end
            s_167:begin
                
                    next_scan_cycle = s_169;
                    state_next = IDLE;
            end
            s_169:begin
                state_next = s_171;
            end
            s_171:begin
                state_next = s_173;
            end
            s_173:begin
                state_next = s_175; aux_inner_pump1_out = inner_pump1_in;
            end
            s_175:begin
                state_next = s_177; aux_inner_pump2_out = inner_pump2_in;
            end
            s_177:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_179;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_179;
                    aux_inner_valve_out = 0;
                end
            end
            s_179:begin
                
                    next_scan_cycle = s_181;
                    state_next = IDLE;
            end
            s_181:begin
                state_next = s_183;
            end
            s_183:begin
                state_next = s_185;
            end
            s_185:begin
                state_next = s_187; aux_inner_pump1_out = inner_pump1_in;
            end
            s_187:begin
                state_next = s_189; aux_inner_pump2_out = inner_pump2_in;
            end
            s_189:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_191;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_191;
                    aux_inner_valve_out = 0;
                end
            end
            s_191:begin
                
                    next_scan_cycle = s_193;
                    state_next = IDLE;
            end
            s_193:begin
                state_next = s_195;
            end
            s_195:begin
                state_next = s_197;
            end
            s_197:begin
                state_next = s_199; aux_inner_pump1_out = inner_pump1_in;
            end
            s_199:begin
                state_next = s_201; aux_inner_pump2_out = inner_pump2_in;
            end
            s_201:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_203;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_203;
                    aux_inner_valve_out = 0;
                end
            end
            s_203:begin
                
                    next_scan_cycle = s_205;
                    state_next = IDLE;
            end
            s_205:begin
                state_next = s_207;
            end
            s_207:begin
                state_next = s_209;
            end
            s_209:begin
                state_next = s_211; aux_inner_pump1_out = inner_pump1_in;
            end
            s_211:begin
                state_next = s_213; aux_inner_pump2_out = inner_pump2_in;
            end
            s_213:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_215;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_215;
                    aux_inner_valve_out = 0;
                end
            end
            s_215:begin
                
                    next_scan_cycle = s_217;
                    state_next = IDLE;
            end
            s_217:begin
                state_next = s_219;
            end
            s_219:begin
                state_next = s_221;
            end
            s_221:begin
                state_next = s_223; aux_inner_pump1_out = inner_pump1_in;
            end
            s_223:begin
                state_next = s_225; aux_inner_pump2_out = inner_pump2_in;
            end
            s_225:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_227;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_227;
                    aux_inner_valve_out = 0;
                end
            end
            s_227:begin
                
                    next_scan_cycle = s_229;
                    state_next = IDLE;
            end
            s_229:begin
                state_next = s_231;
            end
            s_231:begin
                state_next = s_233;
            end
            s_233:begin
                state_next = s_235; aux_inner_pump1_out = inner_pump1_in;
            end
            s_235:begin
                state_next = s_237; aux_inner_pump2_out = inner_pump2_in;
            end
            s_237:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_239;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_239;
                    aux_inner_valve_out = 0;
                end
            end
            s_239:begin
                
                    next_scan_cycle = s_241;
                    state_next = IDLE;
            end
            s_241:begin
                state_next = s_243;
            end
            s_243:begin
                state_next = s_245;
            end
            s_245:begin
                state_next = s_247; aux_inner_pump1_out = inner_pump1_in;
            end
            s_247:begin
                state_next = s_249; aux_inner_pump2_out = inner_pump2_in;
            end
            s_249:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_251;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_251;
                    aux_inner_valve_out = 0;
                end
            end
            s_251:begin
                
                    next_scan_cycle = s_253;
                    state_next = IDLE;
            end
            s_253:begin
                state_next = s_255;
            end
            s_255:begin
                state_next = s_257;
            end
            s_257:begin
                state_next = s_259; aux_inner_pump1_out = inner_pump1_in;
            end
            s_259:begin
                state_next = s_261; aux_inner_pump2_out = inner_pump2_in;
            end
            s_261:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_263;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_263;
                    aux_inner_valve_out = 0;
                end
            end
            s_263:begin
                
                    next_scan_cycle = s_265;
                    state_next = IDLE;
            end
            s_265:begin
                state_next = s_267;
            end
            s_267:begin
                state_next = s_269;
            end
            s_269:begin
                state_next = s_271; aux_inner_pump1_out = inner_pump1_in;
            end
            s_271:begin
                state_next = s_273; aux_inner_pump2_out = inner_pump2_in;
            end
            s_273:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_275;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_275;
                    aux_inner_valve_out = 0;
                end
            end
            s_275:begin
                
                    next_scan_cycle = s_277;
                    state_next = IDLE;
            end
            s_277:begin
                state_next = s_279;
            end
            s_279:begin
                state_next = s_281;
            end
            s_281:begin
                state_next = s_283; aux_inner_pump1_out = inner_pump1_in;
            end
            s_283:begin
                state_next = s_285; aux_inner_pump2_out = inner_pump2_in;
            end
            s_285:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_287;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_287;
                    aux_inner_valve_out = 0;
                end
            end
            s_287:begin
                
                    next_scan_cycle = s_289;
                    state_next = IDLE;
            end
            s_289:begin
                state_next = s_291;
            end
            s_291:begin
                state_next = s_293;
            end
            s_293:begin
                state_next = s_295; aux_inner_pump1_out = inner_pump1_in;
            end
            s_295:begin
                state_next = s_297; aux_inner_pump2_out = inner_pump2_in;
            end
            s_297:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_299;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_299;
                    aux_inner_valve_out = 0;
                end
            end
            s_299:begin
                
                    next_scan_cycle = s_301;
                    state_next = IDLE;
            end
            s_301:begin
                state_next = s_303;
            end
            s_303:begin
                state_next = s_305;
            end
            s_305:begin
                state_next = s_307; aux_inner_pump1_out = inner_pump1_in;
            end
            s_307:begin
                state_next = s_309; aux_inner_pump2_out = inner_pump2_in;
            end
            s_309:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_311;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_311;
                    aux_inner_valve_out = 0;
                end
            end
            s_311:begin
                
                    next_scan_cycle = s_313;
                    state_next = IDLE;
            end
            s_313:begin
                state_next = s_315;
            end
            s_315:begin
                state_next = s_317;
            end
            s_317:begin
                state_next = s_319; aux_inner_pump1_out = inner_pump1_in;
            end
            s_319:begin
                state_next = s_321; aux_inner_pump2_out = inner_pump2_in;
            end
            s_321:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_323;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_323;
                    aux_inner_valve_out = 0;
                end
            end
            s_323:begin
                
                    next_scan_cycle = s_325;
                    state_next = IDLE;
            end
            s_325:begin
                state_next = s_327;
            end
            s_327:begin
                state_next = s_329;
            end
            s_329:begin
                state_next = s_331; aux_inner_pump1_out = inner_pump1_in;
            end
            s_331:begin
                state_next = s_333; aux_inner_pump2_out = inner_pump2_in;
            end
            s_333:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_335;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_335;
                    aux_inner_valve_out = 0;
                end
            end
            s_335:begin
                
                    next_scan_cycle = s_337;
                    state_next = IDLE;
            end
            s_337:begin
                state_next = s_339;
            end
            s_339:begin
                state_next = s_341;
            end
            s_341:begin
                state_next = s_343; aux_inner_pump1_out = inner_pump1_in;
            end
            s_343:begin
                state_next = s_345; aux_inner_pump2_out = inner_pump2_in;
            end
            s_345:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_347;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_347;
                    aux_inner_valve_out = 0;
                end
            end
            s_347:begin
                
                    next_scan_cycle = s_349;
                    state_next = IDLE;
            end
            s_349:begin
                state_next = s_351;
            end
            s_351:begin
                state_next = s_353;
            end
            s_353:begin
                state_next = s_355; aux_inner_pump1_out = inner_pump1_in;
            end
            s_355:begin
                state_next = s_357; aux_inner_pump2_out = inner_pump2_in;
            end
            s_357:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_359;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_359;
                    aux_inner_valve_out = 0;
                end
            end
            s_359:begin
                
                    next_scan_cycle = s_361;
                    state_next = IDLE;
            end
            s_361:begin
                state_next = s_363;
            end
            s_363:begin
                state_next = s_365;
            end
            s_365:begin
                state_next = s_367; aux_inner_pump1_out = inner_pump1_in;
            end
            s_367:begin
                state_next = s_369; aux_inner_pump2_out = inner_pump2_in;
            end
            s_369:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_371;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_371;
                    aux_inner_valve_out = 0;
                end
            end
            s_371:begin
                
                    next_scan_cycle = s_373;
                    state_next = IDLE;
            end
            s_373:begin
                state_next = s_375;
            end
            s_375:begin
                state_next = s_377;
            end
            s_377:begin
                state_next = s_379; aux_inner_pump1_out = inner_pump1_in;
            end
            s_379:begin
                state_next = s_381; aux_inner_pump2_out = inner_pump2_in;
            end
            s_381:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_383;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_383;
                    aux_inner_valve_out = 0;
                end
            end
            s_383:begin
                
                    next_scan_cycle = s_385;
                    state_next = IDLE;
            end
            s_385:begin
                state_next = s_387;
            end
            s_387:begin
                state_next = s_389;
            end
            s_389:begin
                state_next = s_391; aux_inner_pump1_out = inner_pump1_in;
            end
            s_391:begin
                state_next = s_393; aux_inner_pump2_out = inner_pump2_in;
            end
            s_393:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_395;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_395;
                    aux_inner_valve_out = 0;
                end
            end
            s_395:begin
                
                    next_scan_cycle = s_397;
                    state_next = IDLE;
            end
            s_397:begin
                state_next = s_399;
            end
            s_399:begin
                state_next = s_401;
            end
            s_401:begin
                state_next = s_403; aux_inner_pump1_out = inner_pump1_in;
            end
            s_403:begin
                state_next = s_405; aux_inner_pump2_out = inner_pump2_in;
            end
            s_405:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_407;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_407;
                    aux_inner_valve_out = 0;
                end
            end
            s_407:begin
                
                    next_scan_cycle = s_409;
                    state_next = IDLE;
            end
            s_409:begin
                state_next = s_411;
            end
            s_411:begin
                state_next = s_413;
            end
            s_413:begin
                state_next = s_415; aux_inner_pump1_out = inner_pump1_in;
            end
            s_415:begin
                state_next = s_417; aux_inner_pump2_out = inner_pump2_in;
            end
            s_417:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_419;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_419;
                    aux_inner_valve_out = 0;
                end
            end
            s_419:begin
                
                    next_scan_cycle = s_421;
                    state_next = IDLE;
            end
            s_421:begin
                state_next = s_423;
            end
            s_423:begin
                state_next = s_425;
            end
            s_425:begin
                state_next = s_427; aux_inner_pump1_out = inner_pump1_in;
            end
            s_427:begin
                state_next = s_429; aux_inner_pump2_out = inner_pump2_in;
            end
            s_429:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_431;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_431;
                    aux_inner_valve_out = 0;
                end
            end
            s_431:begin
                
                    next_scan_cycle = s_433;
                    state_next = IDLE;
            end
            s_433:begin
                state_next = s_435;
            end
            s_435:begin
                state_next = s_437;
            end
            s_437:begin
                state_next = s_439; aux_inner_pump1_out = inner_pump1_in;
            end
            s_439:begin
                state_next = s_441; aux_inner_pump2_out = inner_pump2_in;
            end
            s_441:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_443;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_443;
                    aux_inner_valve_out = 0;
                end
            end
            s_443:begin
                
                    next_scan_cycle = s_445;
                    state_next = IDLE;
            end
            s_445:begin
                state_next = s_447;
            end
            s_447:begin
                state_next = s_449;
            end
            s_449:begin
                state_next = s_451; aux_inner_pump1_out = inner_pump1_in;
            end
            s_451:begin
                state_next = s_453; aux_inner_pump2_out = inner_pump2_in;
            end
            s_453:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_455;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_455;
                    aux_inner_valve_out = 0;
                end
            end
            s_455:begin
                
                    next_scan_cycle = s_457;
                    state_next = IDLE;
            end
            s_457:begin
                state_next = s_459;
            end
            s_459:begin
                state_next = s_461;
            end
            s_461:begin
                state_next = s_463; aux_inner_pump1_out = inner_pump1_in;
            end
            s_463:begin
                state_next = s_465; aux_inner_pump2_out = inner_pump2_in;
            end
            s_465:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_467;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_467;
                    aux_inner_valve_out = 0;
                end
            end
            s_467:begin
                
                    next_scan_cycle = s_469;
                    state_next = IDLE;
            end
            s_469:begin
                state_next = s_471;
            end
            s_471:begin
                state_next = s_473;
            end
            s_473:begin
                state_next = s_475; aux_inner_pump1_out = inner_pump1_in;
            end
            s_475:begin
                state_next = s_477; aux_inner_pump2_out = inner_pump2_in;
            end
            s_477:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_479;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_479;
                    aux_inner_valve_out = 0;
                end
            end
            s_479:begin
                
                    next_scan_cycle = s_481;
                    state_next = IDLE;
            end
            s_481:begin
                state_next = s_483;
            end
            s_483:begin
                state_next = s_485;
            end
            s_485:begin
                state_next = s_487; aux_inner_pump1_out = inner_pump1_in;
            end
            s_487:begin
                state_next = s_489; aux_inner_pump2_out = inner_pump2_in;
            end
            s_489:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_491;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_491;
                    aux_inner_valve_out = 0;
                end
            end
            s_491:begin
                
                    next_scan_cycle = s_493;
                    state_next = IDLE;
            end
            s_493:begin
                state_next = s_495;
            end
            s_495:begin
                state_next = s_497;
            end
            s_497:begin
                state_next = s_499; aux_inner_pump1_out = inner_pump1_in;
            end
            s_499:begin
                state_next = s_501; aux_inner_pump2_out = inner_pump2_in;
            end
            s_501:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_503;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_503;
                    aux_inner_valve_out = 0;
                end
            end
            s_503:begin
                
                    next_scan_cycle = s_505;
                    state_next = IDLE;
            end
            s_505:begin
                state_next = s_507;
            end
            s_507:begin
                state_next = s_509;
            end
            s_509:begin
                state_next = s_511; aux_inner_pump1_out = inner_pump1_in;
            end
            s_511:begin
                state_next = s_513; aux_inner_pump2_out = inner_pump2_in;
            end
            s_513:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_515;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_515;
                    aux_inner_valve_out = 0;
                end
            end
            s_515:begin
                
                    next_scan_cycle = s_517;
                    state_next = IDLE;
            end
            s_517:begin
                state_next = s_519;
            end
            s_519:begin
                state_next = s_521;
            end
            s_521:begin
                state_next = s_523; aux_inner_pump1_out = inner_pump1_in;
            end
            s_523:begin
                state_next = s_525; aux_inner_pump2_out = inner_pump2_in;
            end
            s_525:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_527;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_527;
                    aux_inner_valve_out = 0;
                end
            end
            s_527:begin
                
                    next_scan_cycle = s_529;
                    state_next = IDLE;
            end
            s_529:begin
                state_next = s_531;
            end
            s_531:begin
                state_next = s_533;
            end
            s_533:begin
                state_next = s_535; aux_inner_pump1_out = inner_pump1_in;
            end
            s_535:begin
                state_next = s_537; aux_inner_pump2_out = inner_pump2_in;
            end
            s_537:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_539;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_539;
                    aux_inner_valve_out = 0;
                end
            end
            s_539:begin
                
                    next_scan_cycle = s_541;
                    state_next = IDLE;
            end
            s_541:begin
                state_next = s_543;
            end
            s_543:begin
                state_next = s_545;
            end
            s_545:begin
                state_next = s_547; aux_inner_pump1_out = inner_pump1_in;
            end
            s_547:begin
                state_next = s_549; aux_inner_pump2_out = inner_pump2_in;
            end
            s_549:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_551;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_551;
                    aux_inner_valve_out = 0;
                end
            end
            s_551:begin
                
                    next_scan_cycle = s_553;
                    state_next = IDLE;
            end
            s_553:begin
                state_next = s_555;
            end
            s_555:begin
                state_next = s_557;
            end
            s_557:begin
                state_next = s_559; aux_inner_pump1_out = inner_pump1_in;
            end
            s_559:begin
                state_next = s_561; aux_inner_pump2_out = inner_pump2_in;
            end
            s_561:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_563;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_563;
                    aux_inner_valve_out = 0;
                end
            end
            s_563:begin
                
                    next_scan_cycle = s_565;
                    state_next = IDLE;
            end
            s_565:begin
                state_next = s_567;
            end
            s_567:begin
                state_next = s_569;
            end
            s_569:begin
                state_next = s_571; aux_inner_pump1_out = inner_pump1_in;
            end
            s_571:begin
                state_next = s_573; aux_inner_pump2_out = inner_pump2_in;
            end
            s_573:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_575;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_575;
                    aux_inner_valve_out = 0;
                end
            end
            s_575:begin
                
                    next_scan_cycle = s_577;
                    state_next = IDLE;
            end
            s_577:begin
                state_next = s_579;
            end
            s_579:begin
                state_next = s_581;
            end
            s_581:begin
                state_next = s_583; aux_inner_pump1_out = inner_pump1_in;
            end
            s_583:begin
                state_next = s_585; aux_inner_pump2_out = inner_pump2_in;
            end
            s_585:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_587;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_587;
                    aux_inner_valve_out = 0;
                end
            end
            s_587:begin
                
                    next_scan_cycle = s_589;
                    state_next = IDLE;
            end
            s_589:begin
                state_next = s_591;
            end
            s_591:begin
                state_next = s_593;
            end
            s_593:begin
                state_next = s_595; aux_inner_pump1_out = inner_pump1_in;
            end
            s_595:begin
                state_next = s_597; aux_inner_pump2_out = inner_pump2_in;
            end
            s_597:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_599;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_599;
                    aux_inner_valve_out = 0;
                end
            end
            s_599:begin
                
                    next_scan_cycle = s_601;
                    state_next = IDLE;
            end
            s_601:begin
                state_next = s_603;
            end
            s_603:begin
                state_next = s_605;
            end
            s_605:begin
                state_next = s_607; aux_inner_pump1_out = inner_pump1_in;
            end
            s_607:begin
                state_next = s_609; aux_inner_pump2_out = inner_pump2_in;
            end
            s_609:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_611;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_611;
                    aux_inner_valve_out = 0;
                end
            end
            s_611:begin
                
                    next_scan_cycle = s_613;
                    state_next = IDLE;
            end
            s_613:begin
                state_next = s_615;
            end
            s_615:begin
                state_next = s_617;
            end
            s_617:begin
                state_next = s_619; aux_inner_pump1_out = inner_pump1_in;
            end
            s_619:begin
                state_next = s_621; aux_inner_pump2_out = inner_pump2_in;
            end
            s_621:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_623;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_623;
                    aux_inner_valve_out = 0;
                end
            end
            s_623:begin
                
                    next_scan_cycle = s_625;
                    state_next = IDLE;
            end
            s_625:begin
                state_next = s_627;
            end
            s_627:begin
                state_next = s_629;
            end
            s_629:begin
                state_next = s_631; aux_inner_pump1_out = inner_pump1_in;
            end
            s_631:begin
                state_next = s_633; aux_inner_pump2_out = inner_pump2_in;
            end
            s_633:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_635;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_635;
                    aux_inner_valve_out = 0;
                end
            end
            s_635:begin
                
                    next_scan_cycle = s_637;
                    state_next = IDLE;
            end
            s_637:begin
                state_next = s_639;
            end
            s_639:begin
                state_next = s_641;
            end
            s_641:begin
                state_next = s_643; aux_inner_pump1_out = inner_pump1_in;
            end
            s_643:begin
                state_next = s_645; aux_inner_pump2_out = inner_pump2_in;
            end
            s_645:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_647;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_647;
                    aux_inner_valve_out = 0;
                end
            end
            s_647:begin
                
                    next_scan_cycle = s_649;
                    state_next = IDLE;
            end
            s_649:begin
                state_next = s_651;
            end
            s_651:begin
                state_next = s_653;
            end
            s_653:begin
                state_next = s_655; aux_inner_pump1_out = inner_pump1_in;
            end
            s_655:begin
                state_next = s_657; aux_inner_pump2_out = inner_pump2_in;
            end
            s_657:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_659;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_659;
                    aux_inner_valve_out = 0;
                end
            end
            s_659:begin
                
                    next_scan_cycle = s_661;
                    state_next = IDLE;
            end
            s_661:begin
                state_next = s_663;
            end
            s_663:begin
                state_next = s_665;
            end
            s_665:begin
                state_next = s_667; aux_inner_pump1_out = inner_pump1_in;
            end
            s_667:begin
                state_next = s_669; aux_inner_pump2_out = inner_pump2_in;
            end
            s_669:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_671;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_671;
                    aux_inner_valve_out = 0;
                end
            end
            s_671:begin
                
                    next_scan_cycle = s_673;
                    state_next = IDLE;
            end
            s_673:begin
                state_next = s_675;
            end
            s_675:begin
                state_next = s_677;
            end
            s_677:begin
                state_next = s_679; aux_inner_pump1_out = inner_pump1_in;
            end
            s_679:begin
                state_next = s_681; aux_inner_pump2_out = inner_pump2_in;
            end
            s_681:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_683;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_683;
                    aux_inner_valve_out = 0;
                end
            end
            s_683:begin
                
                    next_scan_cycle = s_685;
                    state_next = IDLE;
            end
            s_685:begin
                state_next = s_687;
            end
            s_687:begin
                state_next = s_689;
            end
            s_689:begin
                state_next = s_691; aux_inner_pump1_out = inner_pump1_in;
            end
            s_691:begin
                state_next = s_693; aux_inner_pump2_out = inner_pump2_in;
            end
            s_693:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_695;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_695;
                    aux_inner_valve_out = 0;
                end
            end
            s_695:begin
                
                    next_scan_cycle = s_697;
                    state_next = IDLE;
            end
            s_697:begin
                state_next = s_699;
            end
            s_699:begin
                state_next = s_701;
            end
            s_701:begin
                state_next = s_703; aux_inner_pump1_out = inner_pump1_in;
            end
            s_703:begin
                state_next = s_705; aux_inner_pump2_out = inner_pump2_in;
            end
            s_705:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_707;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_707;
                    aux_inner_valve_out = 0;
                end
            end
            s_707:begin
                
                    next_scan_cycle = s_709;
                    state_next = IDLE;
            end
            s_709:begin
                state_next = s_711;
            end
            s_711:begin
                state_next = s_713;
            end
            s_713:begin
                state_next = s_715; aux_inner_pump1_out = inner_pump1_in;
            end
            s_715:begin
                state_next = s_717; aux_inner_pump2_out = inner_pump2_in;
            end
            s_717:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_719;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_719;
                    aux_inner_valve_out = 0;
                end
            end
            s_719:begin
                
                    next_scan_cycle = s_721;
                    state_next = IDLE;
            end
            s_721:begin
                state_next = s_723;
            end
            s_723:begin
                state_next = s_725;
            end
            s_725:begin
                state_next = s_727; aux_inner_pump1_out = inner_pump1_in;
            end
            s_727:begin
                state_next = s_729; aux_inner_pump2_out = inner_pump2_in;
            end
            s_729:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_731;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_731;
                    aux_inner_valve_out = 0;
                end
            end
            s_731:begin
                
                    next_scan_cycle = s_733;
                    state_next = IDLE;
            end
            s_733:begin
                state_next = s_735;
            end
            s_735:begin
                state_next = s_737;
            end
            s_737:begin
                state_next = s_739; aux_inner_pump1_out = inner_pump1_in;
            end
            s_739:begin
                state_next = s_741; aux_inner_pump2_out = inner_pump2_in;
            end
            s_741:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_743;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_743;
                    aux_inner_valve_out = 0;
                end
            end
            s_743:begin
                
                    next_scan_cycle = s_745;
                    state_next = IDLE;
            end
            s_745:begin
                state_next = s_747;
            end
            s_747:begin
                state_next = s_749;
            end
            s_749:begin
                state_next = s_751; aux_inner_pump1_out = inner_pump1_in;
            end
            s_751:begin
                state_next = s_753; aux_inner_pump2_out = inner_pump2_in;
            end
            s_753:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_755;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_755;
                    aux_inner_valve_out = 0;
                end
            end
            s_755:begin
                
                    next_scan_cycle = s_757;
                    state_next = IDLE;
            end
            s_757:begin
                state_next = s_759;
            end
            s_759:begin
                state_next = s_761;
            end
            s_761:begin
                state_next = s_763; aux_inner_pump1_out = inner_pump1_in;
            end
            s_763:begin
                state_next = s_765; aux_inner_pump2_out = inner_pump2_in;
            end
            s_765:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_767;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_767;
                    aux_inner_valve_out = 0;
                end
            end
            s_767:begin
                
                    next_scan_cycle = s_769;
                    state_next = IDLE;
            end
            s_769:begin
                state_next = s_771;
            end
            s_771:begin
                state_next = s_773;
            end
            s_773:begin
                state_next = s_775; aux_inner_pump1_out = inner_pump1_in;
            end
            s_775:begin
                state_next = s_777; aux_inner_pump2_out = inner_pump2_in;
            end
            s_777:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_779;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_779;
                    aux_inner_valve_out = 0;
                end
            end
            s_779:begin
                
                    next_scan_cycle = s_781;
                    state_next = IDLE;
            end
            s_781:begin
                state_next = s_783;
            end
            s_783:begin
                state_next = s_785;
            end
            s_785:begin
                state_next = s_787; aux_inner_pump1_out = inner_pump1_in;
            end
            s_787:begin
                state_next = s_789; aux_inner_pump2_out = inner_pump2_in;
            end
            s_789:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_791;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_791;
                    aux_inner_valve_out = 0;
                end
            end
            s_791:begin
                
                    next_scan_cycle = s_793;
                    state_next = IDLE;
            end
            s_793:begin
                state_next = s_795;
            end
            s_795:begin
                state_next = s_797;
            end
            s_797:begin
                state_next = s_799; aux_inner_pump1_out = inner_pump1_in;
            end
            s_799:begin
                state_next = s_801; aux_inner_pump2_out = inner_pump2_in;
            end
            s_801:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_803;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_803;
                    aux_inner_valve_out = 0;
                end
            end
            s_803:begin
                
                    next_scan_cycle = s_805;
                    state_next = IDLE;
            end
            s_805:begin
                state_next = s_807;
            end
            s_807:begin
                state_next = s_809;
            end
            s_809:begin
                state_next = s_811; aux_inner_pump1_out = inner_pump1_in;
            end
            s_811:begin
                state_next = s_813; aux_inner_pump2_out = inner_pump2_in;
            end
            s_813:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_815;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_815;
                    aux_inner_valve_out = 0;
                end
            end
            s_815:begin
                
                    next_scan_cycle = s_817;
                    state_next = IDLE;
            end
            s_817:begin
                state_next = s_819;
            end
            s_819:begin
                state_next = s_821;
            end
            s_821:begin
                state_next = s_823; aux_inner_pump1_out = inner_pump1_in;
            end
            s_823:begin
                state_next = s_825; aux_inner_pump2_out = inner_pump2_in;
            end
            s_825:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_827;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_827;
                    aux_inner_valve_out = 0;
                end
            end
            s_827:begin
                
                    next_scan_cycle = s_829;
                    state_next = IDLE;
            end
            s_829:begin
                state_next = s_831;
            end
            s_831:begin
                state_next = s_833;
            end
            s_833:begin
                state_next = s_835; aux_inner_pump1_out = inner_pump1_in;
            end
            s_835:begin
                state_next = s_837; aux_inner_pump2_out = inner_pump2_in;
            end
            s_837:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_839;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_839;
                    aux_inner_valve_out = 0;
                end
            end
            s_839:begin
                
                    next_scan_cycle = s_841;
                    state_next = IDLE;
            end
            s_841:begin
                state_next = s_843;
            end
            s_843:begin
                state_next = s_845;
            end
            s_845:begin
                state_next = s_847; aux_inner_pump1_out = inner_pump1_in;
            end
            s_847:begin
                state_next = s_849; aux_inner_pump2_out = inner_pump2_in;
            end
            s_849:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_851;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_851;
                    aux_inner_valve_out = 0;
                end
            end
            s_851:begin
                
                    next_scan_cycle = s_853;
                    state_next = IDLE;
            end
            s_853:begin
                state_next = s_855;
            end
            s_855:begin
                state_next = s_857;
            end
            s_857:begin
                state_next = s_859; aux_inner_pump1_out = inner_pump1_in;
            end
            s_859:begin
                state_next = s_861; aux_inner_pump2_out = inner_pump2_in;
            end
            s_861:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_863;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_863;
                    aux_inner_valve_out = 0;
                end
            end
            s_863:begin
                
                    next_scan_cycle = s_865;
                    state_next = IDLE;
            end
            s_865:begin
                state_next = s_867;
            end
            s_867:begin
                state_next = s_869;
            end
            s_869:begin
                state_next = s_871; aux_inner_pump1_out = inner_pump1_in;
            end
            s_871:begin
                state_next = s_873; aux_inner_pump2_out = inner_pump2_in;
            end
            s_873:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_875;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_875;
                    aux_inner_valve_out = 0;
                end
            end
            s_875:begin
                
                    next_scan_cycle = s_877;
                    state_next = IDLE;
            end
            s_877:begin
                state_next = s_879;
            end
            s_879:begin
                state_next = s_881;
            end
            s_881:begin
                state_next = s_883; aux_inner_pump1_out = inner_pump1_in;
            end
            s_883:begin
                state_next = s_885; aux_inner_pump2_out = inner_pump2_in;
            end
            s_885:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_887;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_887;
                    aux_inner_valve_out = 0;
                end
            end
            s_887:begin
                
                    next_scan_cycle = s_889;
                    state_next = IDLE;
            end
            s_889:begin
                state_next = s_891;
            end
            s_891:begin
                state_next = s_893;
            end
            s_893:begin
                state_next = s_895; aux_inner_pump1_out = inner_pump1_in;
            end
            s_895:begin
                state_next = s_897; aux_inner_pump2_out = inner_pump2_in;
            end
            s_897:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_899;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_899;
                    aux_inner_valve_out = 0;
                end
            end
            s_899:begin
                
                    next_scan_cycle = s_901;
                    state_next = IDLE;
            end
            s_901:begin
                state_next = s_903;
            end
            s_903:begin
                state_next = s_905;
            end
            s_905:begin
                state_next = s_907; aux_inner_pump1_out = inner_pump1_in;
            end
            s_907:begin
                state_next = s_909; aux_inner_pump2_out = inner_pump2_in;
            end
            s_909:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_911;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_911;
                    aux_inner_valve_out = 0;
                end
            end
            s_911:begin
                
                    next_scan_cycle = s_913;
                    state_next = IDLE;
            end
            s_913:begin
                state_next = s_915;
            end
            s_915:begin
                state_next = s_917;
            end
            s_917:begin
                state_next = s_919; aux_inner_pump1_out = inner_pump1_in;
            end
            s_919:begin
                state_next = s_921; aux_inner_pump2_out = inner_pump2_in;
            end
            s_921:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_923;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_923;
                    aux_inner_valve_out = 0;
                end
            end
            s_923:begin
                
                    next_scan_cycle = s_925;
                    state_next = IDLE;
            end
            s_925:begin
                state_next = s_927;
            end
            s_927:begin
                state_next = s_929;
            end
            s_929:begin
                state_next = s_931; aux_inner_pump1_out = inner_pump1_in;
            end
            s_931:begin
                state_next = s_933; aux_inner_pump2_out = inner_pump2_in;
            end
            s_933:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_935;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_935;
                    aux_inner_valve_out = 0;
                end
            end
            s_935:begin
                
                    next_scan_cycle = s_937;
                    state_next = IDLE;
            end
            s_937:begin
                state_next = s_939;
            end
            s_939:begin
                state_next = s_941;
            end
            s_941:begin
                state_next = s_943; aux_inner_pump1_out = inner_pump1_in;
            end
            s_943:begin
                state_next = s_945; aux_inner_pump2_out = inner_pump2_in;
            end
            s_945:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_947;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_947;
                    aux_inner_valve_out = 0;
                end
            end
            s_947:begin
                
                    next_scan_cycle = s_949;
                    state_next = IDLE;
            end
            s_949:begin
                state_next = s_951;
            end
            s_951:begin
                state_next = s_953;
            end
            s_953:begin
                state_next = s_955; aux_inner_pump1_out = inner_pump1_in;
            end
            s_955:begin
                state_next = s_957; aux_inner_pump2_out = inner_pump2_in;
            end
            s_957:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_959;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_959;
                    aux_inner_valve_out = 0;
                end
            end
            s_959:begin
                
                    next_scan_cycle = s_961;
                    state_next = IDLE;
            end
            s_961:begin
                state_next = s_963;
            end
            s_963:begin
                state_next = s_965;
            end
            s_965:begin
                state_next = s_967; aux_inner_pump1_out = inner_pump1_in;
            end
            s_967:begin
                state_next = s_969; aux_inner_pump2_out = inner_pump2_in;
            end
            s_969:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_971;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_971;
                    aux_inner_valve_out = 0;
                end
            end
            s_971:begin
                
                    next_scan_cycle = s_973;
                    state_next = IDLE;
            end
            s_973:begin
                state_next = s_975;
            end
            s_975:begin
                state_next = s_977;
            end
            s_977:begin
                state_next = s_979; aux_inner_pump1_out = inner_pump1_in;
            end
            s_979:begin
                state_next = s_981; aux_inner_pump2_out = inner_pump2_in;
            end
            s_981:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_983;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_983;
                    aux_inner_valve_out = 0;
                end
            end
            s_983:begin
                
                    next_scan_cycle = s_985;
                    state_next = IDLE;
            end
            s_985:begin
                state_next = s_987;
            end
            s_987:begin
                state_next = s_989;
            end
            s_989:begin
                state_next = s_991; aux_inner_pump1_out = inner_pump1_in;
            end
            s_991:begin
                state_next = s_993; aux_inner_pump2_out = inner_pump2_in;
            end
            s_993:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_995;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_995;
                    aux_inner_valve_out = 0;
                end
            end
            s_995:begin
                
                    next_scan_cycle = s_997;
                    state_next = IDLE;
            end
            s_997:begin
                state_next = s_999;
            end
            s_999:begin
                state_next = s_1001;
            end
            s_1001:begin
                state_next = s_1003; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1003:begin
                state_next = s_1005; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1005:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1007;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1007;
                    aux_inner_valve_out = 0;
                end
            end
            s_1007:begin
                
                    next_scan_cycle = s_1009;
                    state_next = IDLE;
            end
            s_1009:begin
                state_next = s_1011;
            end
            s_1011:begin
                state_next = s_1013;
            end
            s_1013:begin
                state_next = s_1015; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1015:begin
                state_next = s_1017; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1017:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1019;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1019;
                    aux_inner_valve_out = 0;
                end
            end
            s_1019:begin
                
                    next_scan_cycle = s_1021;
                    state_next = IDLE;
            end
            s_1021:begin
                state_next = s_1023;
            end
            s_1023:begin
                state_next = s_1025;
            end
            s_1025:begin
                state_next = s_1027; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1027:begin
                state_next = s_1029; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1029:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1031;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1031;
                    aux_inner_valve_out = 0;
                end
            end
            s_1031:begin
                
                    next_scan_cycle = s_1033;
                    state_next = IDLE;
            end
            s_1033:begin
                state_next = s_1035;
            end
            s_1035:begin
                state_next = s_1037;
            end
            s_1037:begin
                state_next = s_1039; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1039:begin
                state_next = s_1041; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1041:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1043;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1043;
                    aux_inner_valve_out = 0;
                end
            end
            s_1043:begin
                
                    next_scan_cycle = s_1045;
                    state_next = IDLE;
            end
            s_1045:begin
                state_next = s_1047;
            end
            s_1047:begin
                state_next = s_1049;
            end
            s_1049:begin
                state_next = s_1051; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1051:begin
                state_next = s_1053; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1053:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1055;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1055;
                    aux_inner_valve_out = 0;
                end
            end
            s_1055:begin
                
                    next_scan_cycle = s_1057;
                    state_next = IDLE;
            end
            s_1057:begin
                state_next = s_1059;
            end
            s_1059:begin
                state_next = s_1061;
            end
            s_1061:begin
                state_next = s_1063; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1063:begin
                state_next = s_1065; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1065:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1067;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1067;
                    aux_inner_valve_out = 0;
                end
            end
            s_1067:begin
                
                    next_scan_cycle = s_1069;
                    state_next = IDLE;
            end
            s_1069:begin
                state_next = s_1071;
            end
            s_1071:begin
                state_next = s_1073;
            end
            s_1073:begin
                state_next = s_1075; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1075:begin
                state_next = s_1077; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1077:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1079;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1079;
                    aux_inner_valve_out = 0;
                end
            end
            s_1079:begin
                
                    next_scan_cycle = s_1081;
                    state_next = IDLE;
            end
            s_1081:begin
                state_next = s_1083;
            end
            s_1083:begin
                state_next = s_1085;
            end
            s_1085:begin
                state_next = s_1087; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1087:begin
                state_next = s_1089; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1089:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1091;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1091;
                    aux_inner_valve_out = 0;
                end
            end
            s_1091:begin
                
                    next_scan_cycle = s_1093;
                    state_next = IDLE;
            end
            s_1093:begin
                state_next = s_1095;
            end
            s_1095:begin
                state_next = s_1097;
            end
            s_1097:begin
                state_next = s_1099; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1099:begin
                state_next = s_1101; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1101:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1103;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1103;
                    aux_inner_valve_out = 0;
                end
            end
            s_1103:begin
                
                    next_scan_cycle = s_1105;
                    state_next = IDLE;
            end
            s_1105:begin
                state_next = s_1107;
            end
            s_1107:begin
                state_next = s_1109;
            end
            s_1109:begin
                state_next = s_1111; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1111:begin
                state_next = s_1113; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1113:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1115;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1115;
                    aux_inner_valve_out = 0;
                end
            end
            s_1115:begin
                
                    next_scan_cycle = s_1117;
                    state_next = IDLE;
            end
            s_1117:begin
                state_next = s_1119;
            end
            s_1119:begin
                state_next = s_1121;
            end
            s_1121:begin
                state_next = s_1123; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1123:begin
                state_next = s_1125; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1125:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1127;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1127;
                    aux_inner_valve_out = 0;
                end
            end
            s_1127:begin
                
                    next_scan_cycle = s_1129;
                    state_next = IDLE;
            end
            s_1129:begin
                state_next = s_1131;
            end
            s_1131:begin
                state_next = s_1133;
            end
            s_1133:begin
                state_next = s_1135; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1135:begin
                state_next = s_1137; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1137:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1139;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1139;
                    aux_inner_valve_out = 0;
                end
            end
            s_1139:begin
                
                    next_scan_cycle = s_1141;
                    state_next = IDLE;
            end
            s_1141:begin
                state_next = s_1143;
            end
            s_1143:begin
                state_next = s_1145;
            end
            s_1145:begin
                state_next = s_1147; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1147:begin
                state_next = s_1149; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1149:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1151;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1151;
                    aux_inner_valve_out = 0;
                end
            end
            s_1151:begin
                
                    next_scan_cycle = s_1153;
                    state_next = IDLE;
            end
            s_1153:begin
                state_next = s_1155;
            end
            s_1155:begin
                state_next = s_1157;
            end
            s_1157:begin
                state_next = s_1159; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1159:begin
                state_next = s_1161; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1161:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1163;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1163;
                    aux_inner_valve_out = 0;
                end
            end
            s_1163:begin
                
                    next_scan_cycle = s_1165;
                    state_next = IDLE;
            end
            s_1165:begin
                state_next = s_1167;
            end
            s_1167:begin
                state_next = s_1169;
            end
            s_1169:begin
                state_next = s_1171; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1171:begin
                state_next = s_1173; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1173:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1175;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1175;
                    aux_inner_valve_out = 0;
                end
            end
            s_1175:begin
                
                    next_scan_cycle = s_1177;
                    state_next = IDLE;
            end
            s_1177:begin
                state_next = s_1179;
            end
            s_1179:begin
                state_next = s_1181;
            end
            s_1181:begin
                state_next = s_1183; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1183:begin
                state_next = s_1185; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1185:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1187;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1187;
                    aux_inner_valve_out = 0;
                end
            end
            s_1187:begin
                
                    next_scan_cycle = s_1189;
                    state_next = IDLE;
            end
            s_1189:begin
                state_next = s_1191;
            end
            s_1191:begin
                state_next = s_1193;
            end
            s_1193:begin
                state_next = s_1195; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1195:begin
                state_next = s_1197; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1197:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1199;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1199;
                    aux_inner_valve_out = 0;
                end
            end
            s_1199:begin
                
                    next_scan_cycle = s_1201;
                    state_next = IDLE;
            end
            s_1201:begin
                state_next = s_1203;
            end
            s_1203:begin
                state_next = s_1205;
            end
            s_1205:begin
                state_next = s_1207; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1207:begin
                state_next = s_1209; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1209:begin
                
                if (inner_valve_in == 0) begin
                    state_next = s_1211;
                    aux_inner_valve_out = 0;
                end
                else begin
                    state_next = s_1211;
                    aux_inner_valve_out = 0;
                end
            end
            s_1211:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
            s_1213:begin
                
                    next_scan_cycle = s_1215;
                    state_next = IDLE;
            end
            s_1215:begin
                state_next = s_1217;
            end
            s_1217:begin
                state_next = s_1219;
            end
            s_1219:begin
                state_next = s_1221; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1221:begin
                state_next = s_1223; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1223:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1225;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1225;
                    aux_inner_valve_out = 1;
                end
            end
            s_1225:begin
                
                    next_scan_cycle = s_1227;
                    state_next = IDLE;
            end
            s_1227:begin
                state_next = s_1229;
            end
            s_1229:begin
                state_next = s_1231;
            end
            s_1231:begin
                state_next = s_1233; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1233:begin
                state_next = s_1235; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1235:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1237;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1237;
                    aux_inner_valve_out = 1;
                end
            end
            s_1237:begin
                
                    next_scan_cycle = s_1239;
                    state_next = IDLE;
            end
            s_1239:begin
                state_next = s_1241;
            end
            s_1241:begin
                state_next = s_1243;
            end
            s_1243:begin
                state_next = s_1245; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1245:begin
                state_next = s_1247; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1247:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1249;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1249;
                    aux_inner_valve_out = 1;
                end
            end
            s_1249:begin
                
                    next_scan_cycle = s_1251;
                    state_next = IDLE;
            end
            s_1251:begin
                state_next = s_1253;
            end
            s_1253:begin
                state_next = s_1255;
            end
            s_1255:begin
                state_next = s_1257; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1257:begin
                state_next = s_1259; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1259:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1261;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1261;
                    aux_inner_valve_out = 1;
                end
            end
            s_1261:begin
                
                    next_scan_cycle = s_1263;
                    state_next = IDLE;
            end
            s_1263:begin
                state_next = s_1265;
            end
            s_1265:begin
                state_next = s_1267;
            end
            s_1267:begin
                state_next = s_1269; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1269:begin
                state_next = s_1271; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1271:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1273;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1273;
                    aux_inner_valve_out = 1;
                end
            end
            s_1273:begin
                
                    next_scan_cycle = s_1275;
                    state_next = IDLE;
            end
            s_1275:begin
                state_next = s_1277;
            end
            s_1277:begin
                state_next = s_1279;
            end
            s_1279:begin
                state_next = s_1281; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1281:begin
                state_next = s_1283; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1283:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1285;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1285;
                    aux_inner_valve_out = 1;
                end
            end
            s_1285:begin
                
                    next_scan_cycle = s_1287;
                    state_next = IDLE;
            end
            s_1287:begin
                state_next = s_1289;
            end
            s_1289:begin
                state_next = s_1291;
            end
            s_1291:begin
                state_next = s_1293; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1293:begin
                state_next = s_1295; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1295:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1297;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1297;
                    aux_inner_valve_out = 1;
                end
            end
            s_1297:begin
                
                    next_scan_cycle = s_1299;
                    state_next = IDLE;
            end
            s_1299:begin
                state_next = s_1301;
            end
            s_1301:begin
                state_next = s_1303;
            end
            s_1303:begin
                state_next = s_1305; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1305:begin
                state_next = s_1307; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1307:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1309;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1309;
                    aux_inner_valve_out = 1;
                end
            end
            s_1309:begin
                
                    next_scan_cycle = s_1311;
                    state_next = IDLE;
            end
            s_1311:begin
                state_next = s_1313;
            end
            s_1313:begin
                state_next = s_1315;
            end
            s_1315:begin
                state_next = s_1317; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1317:begin
                state_next = s_1319; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1319:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1321;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1321;
                    aux_inner_valve_out = 1;
                end
            end
            s_1321:begin
                
                    next_scan_cycle = s_1323;
                    state_next = IDLE;
            end
            s_1323:begin
                state_next = s_1325;
            end
            s_1325:begin
                state_next = s_1327;
            end
            s_1327:begin
                state_next = s_1329; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1329:begin
                state_next = s_1331; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1331:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1333;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1333;
                    aux_inner_valve_out = 1;
                end
            end
            s_1333:begin
                
                    next_scan_cycle = s_1335;
                    state_next = IDLE;
            end
            s_1335:begin
                state_next = s_1337;
            end
            s_1337:begin
                state_next = s_1339;
            end
            s_1339:begin
                state_next = s_1341; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1341:begin
                state_next = s_1343; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1343:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1345;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1345;
                    aux_inner_valve_out = 1;
                end
            end
            s_1345:begin
                
                    next_scan_cycle = s_1347;
                    state_next = IDLE;
            end
            s_1347:begin
                state_next = s_1349;
            end
            s_1349:begin
                state_next = s_1351;
            end
            s_1351:begin
                state_next = s_1353; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1353:begin
                state_next = s_1355; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1355:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1357;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1357;
                    aux_inner_valve_out = 1;
                end
            end
            s_1357:begin
                
                    next_scan_cycle = s_1359;
                    state_next = IDLE;
            end
            s_1359:begin
                state_next = s_1361;
            end
            s_1361:begin
                state_next = s_1363;
            end
            s_1363:begin
                state_next = s_1365; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1365:begin
                state_next = s_1367; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1367:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1369;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1369;
                    aux_inner_valve_out = 1;
                end
            end
            s_1369:begin
                
                    next_scan_cycle = s_1371;
                    state_next = IDLE;
            end
            s_1371:begin
                state_next = s_1373;
            end
            s_1373:begin
                state_next = s_1375;
            end
            s_1375:begin
                state_next = s_1377; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1377:begin
                state_next = s_1379; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1379:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1381;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1381;
                    aux_inner_valve_out = 1;
                end
            end
            s_1381:begin
                
                    next_scan_cycle = s_1383;
                    state_next = IDLE;
            end
            s_1383:begin
                state_next = s_1385;
            end
            s_1385:begin
                state_next = s_1387;
            end
            s_1387:begin
                state_next = s_1389; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1389:begin
                state_next = s_1391; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1391:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1393;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1393;
                    aux_inner_valve_out = 1;
                end
            end
            s_1393:begin
                
                    next_scan_cycle = s_1395;
                    state_next = IDLE;
            end
            s_1395:begin
                state_next = s_1397;
            end
            s_1397:begin
                state_next = s_1399;
            end
            s_1399:begin
                state_next = s_1401; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1401:begin
                state_next = s_1403; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1403:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1405;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1405;
                    aux_inner_valve_out = 1;
                end
            end
            s_1405:begin
                
                    next_scan_cycle = s_1407;
                    state_next = IDLE;
            end
            s_1407:begin
                state_next = s_1409;
            end
            s_1409:begin
                state_next = s_1411;
            end
            s_1411:begin
                state_next = s_1413; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1413:begin
                state_next = s_1415; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1415:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1417;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1417;
                    aux_inner_valve_out = 1;
                end
            end
            s_1417:begin
                
                    next_scan_cycle = s_1419;
                    state_next = IDLE;
            end
            s_1419:begin
                state_next = s_1421;
            end
            s_1421:begin
                state_next = s_1423;
            end
            s_1423:begin
                state_next = s_1425; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1425:begin
                state_next = s_1427; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1427:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1429;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1429;
                    aux_inner_valve_out = 1;
                end
            end
            s_1429:begin
                
                    next_scan_cycle = s_1431;
                    state_next = IDLE;
            end
            s_1431:begin
                state_next = s_1433;
            end
            s_1433:begin
                state_next = s_1435;
            end
            s_1435:begin
                state_next = s_1437; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1437:begin
                state_next = s_1439; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1439:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1441;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1441;
                    aux_inner_valve_out = 1;
                end
            end
            s_1441:begin
                
                    next_scan_cycle = s_1443;
                    state_next = IDLE;
            end
            s_1443:begin
                state_next = s_1445;
            end
            s_1445:begin
                state_next = s_1447;
            end
            s_1447:begin
                state_next = s_1449; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1449:begin
                state_next = s_1451; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1451:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1453;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1453;
                    aux_inner_valve_out = 1;
                end
            end
            s_1453:begin
                
                    next_scan_cycle = s_1455;
                    state_next = IDLE;
            end
            s_1455:begin
                state_next = s_1457;
            end
            s_1457:begin
                state_next = s_1459;
            end
            s_1459:begin
                state_next = s_1461; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1461:begin
                state_next = s_1463; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1463:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1465;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1465;
                    aux_inner_valve_out = 1;
                end
            end
            s_1465:begin
                
                    next_scan_cycle = s_1467;
                    state_next = IDLE;
            end
            s_1467:begin
                state_next = s_1469;
            end
            s_1469:begin
                state_next = s_1471;
            end
            s_1471:begin
                state_next = s_1473; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1473:begin
                state_next = s_1475; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1475:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1477;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1477;
                    aux_inner_valve_out = 1;
                end
            end
            s_1477:begin
                
                    next_scan_cycle = s_1479;
                    state_next = IDLE;
            end
            s_1479:begin
                state_next = s_1481;
            end
            s_1481:begin
                state_next = s_1483;
            end
            s_1483:begin
                state_next = s_1485; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1485:begin
                state_next = s_1487; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1487:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1489;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1489;
                    aux_inner_valve_out = 1;
                end
            end
            s_1489:begin
                
                    next_scan_cycle = s_1491;
                    state_next = IDLE;
            end
            s_1491:begin
                state_next = s_1493;
            end
            s_1493:begin
                state_next = s_1495;
            end
            s_1495:begin
                state_next = s_1497; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1497:begin
                state_next = s_1499; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1499:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1501;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1501;
                    aux_inner_valve_out = 1;
                end
            end
            s_1501:begin
                
                    next_scan_cycle = s_1503;
                    state_next = IDLE;
            end
            s_1503:begin
                state_next = s_1505;
            end
            s_1505:begin
                state_next = s_1507;
            end
            s_1507:begin
                state_next = s_1509; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1509:begin
                state_next = s_1511; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1511:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1513;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1513;
                    aux_inner_valve_out = 1;
                end
            end
            s_1513:begin
                
                    next_scan_cycle = s_1515;
                    state_next = IDLE;
            end
            s_1515:begin
                state_next = s_1517;
            end
            s_1517:begin
                state_next = s_1519;
            end
            s_1519:begin
                state_next = s_1521; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1521:begin
                state_next = s_1523; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1523:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1525;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1525;
                    aux_inner_valve_out = 1;
                end
            end
            s_1525:begin
                
                    next_scan_cycle = s_1527;
                    state_next = IDLE;
            end
            s_1527:begin
                state_next = s_1529;
            end
            s_1529:begin
                state_next = s_1531;
            end
            s_1531:begin
                state_next = s_1533; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1533:begin
                state_next = s_1535; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1535:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1537;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1537;
                    aux_inner_valve_out = 1;
                end
            end
            s_1537:begin
                
                    next_scan_cycle = s_1539;
                    state_next = IDLE;
            end
            s_1539:begin
                state_next = s_1541;
            end
            s_1541:begin
                state_next = s_1543;
            end
            s_1543:begin
                state_next = s_1545; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1545:begin
                state_next = s_1547; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1547:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1549;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1549;
                    aux_inner_valve_out = 1;
                end
            end
            s_1549:begin
                
                    next_scan_cycle = s_1551;
                    state_next = IDLE;
            end
            s_1551:begin
                state_next = s_1553;
            end
            s_1553:begin
                state_next = s_1555;
            end
            s_1555:begin
                state_next = s_1557; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1557:begin
                state_next = s_1559; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1559:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1561;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1561;
                    aux_inner_valve_out = 1;
                end
            end
            s_1561:begin
                
                    next_scan_cycle = s_1563;
                    state_next = IDLE;
            end
            s_1563:begin
                state_next = s_1565;
            end
            s_1565:begin
                state_next = s_1567;
            end
            s_1567:begin
                state_next = s_1569; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1569:begin
                state_next = s_1571; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1571:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1573;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1573;
                    aux_inner_valve_out = 1;
                end
            end
            s_1573:begin
                
                    next_scan_cycle = s_1575;
                    state_next = IDLE;
            end
            s_1575:begin
                state_next = s_1577;
            end
            s_1577:begin
                state_next = s_1579;
            end
            s_1579:begin
                state_next = s_1581; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1581:begin
                state_next = s_1583; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1583:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1585;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1585;
                    aux_inner_valve_out = 1;
                end
            end
            s_1585:begin
                
                    next_scan_cycle = s_1587;
                    state_next = IDLE;
            end
            s_1587:begin
                state_next = s_1589;
            end
            s_1589:begin
                state_next = s_1591;
            end
            s_1591:begin
                state_next = s_1593; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1593:begin
                state_next = s_1595; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1595:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1597;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1597;
                    aux_inner_valve_out = 1;
                end
            end
            s_1597:begin
                
                    next_scan_cycle = s_1599;
                    state_next = IDLE;
            end
            s_1599:begin
                state_next = s_1601;
            end
            s_1601:begin
                state_next = s_1603;
            end
            s_1603:begin
                state_next = s_1605; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1605:begin
                state_next = s_1607; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1607:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1609;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1609;
                    aux_inner_valve_out = 1;
                end
            end
            s_1609:begin
                
                    next_scan_cycle = s_1611;
                    state_next = IDLE;
            end
            s_1611:begin
                state_next = s_1613;
            end
            s_1613:begin
                state_next = s_1615;
            end
            s_1615:begin
                state_next = s_1617; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1617:begin
                state_next = s_1619; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1619:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1621;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1621;
                    aux_inner_valve_out = 1;
                end
            end
            s_1621:begin
                
                    next_scan_cycle = s_1623;
                    state_next = IDLE;
            end
            s_1623:begin
                state_next = s_1625;
            end
            s_1625:begin
                state_next = s_1627;
            end
            s_1627:begin
                state_next = s_1629; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1629:begin
                state_next = s_1631; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1631:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1633;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1633;
                    aux_inner_valve_out = 1;
                end
            end
            s_1633:begin
                
                    next_scan_cycle = s_1635;
                    state_next = IDLE;
            end
            s_1635:begin
                state_next = s_1637;
            end
            s_1637:begin
                state_next = s_1639;
            end
            s_1639:begin
                state_next = s_1641; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1641:begin
                state_next = s_1643; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1643:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1645;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1645;
                    aux_inner_valve_out = 1;
                end
            end
            s_1645:begin
                
                    next_scan_cycle = s_1647;
                    state_next = IDLE;
            end
            s_1647:begin
                state_next = s_1649;
            end
            s_1649:begin
                state_next = s_1651;
            end
            s_1651:begin
                state_next = s_1653; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1653:begin
                state_next = s_1655; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1655:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1657;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1657;
                    aux_inner_valve_out = 1;
                end
            end
            s_1657:begin
                
                    next_scan_cycle = s_1659;
                    state_next = IDLE;
            end
            s_1659:begin
                state_next = s_1661;
            end
            s_1661:begin
                state_next = s_1663;
            end
            s_1663:begin
                state_next = s_1665; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1665:begin
                state_next = s_1667; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1667:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1669;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1669;
                    aux_inner_valve_out = 1;
                end
            end
            s_1669:begin
                
                    next_scan_cycle = s_1671;
                    state_next = IDLE;
            end
            s_1671:begin
                state_next = s_1673;
            end
            s_1673:begin
                state_next = s_1675;
            end
            s_1675:begin
                state_next = s_1677; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1677:begin
                state_next = s_1679; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1679:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1681;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1681;
                    aux_inner_valve_out = 1;
                end
            end
            s_1681:begin
                
                    next_scan_cycle = s_1683;
                    state_next = IDLE;
            end
            s_1683:begin
                state_next = s_1685;
            end
            s_1685:begin
                state_next = s_1687;
            end
            s_1687:begin
                state_next = s_1689; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1689:begin
                state_next = s_1691; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1691:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1693;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1693;
                    aux_inner_valve_out = 1;
                end
            end
            s_1693:begin
                
                    next_scan_cycle = s_1695;
                    state_next = IDLE;
            end
            s_1695:begin
                state_next = s_1697;
            end
            s_1697:begin
                state_next = s_1699;
            end
            s_1699:begin
                state_next = s_1701; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1701:begin
                state_next = s_1703; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1703:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1705;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1705;
                    aux_inner_valve_out = 1;
                end
            end
            s_1705:begin
                
                    next_scan_cycle = s_1707;
                    state_next = IDLE;
            end
            s_1707:begin
                state_next = s_1709;
            end
            s_1709:begin
                state_next = s_1711;
            end
            s_1711:begin
                state_next = s_1713; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1713:begin
                state_next = s_1715; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1715:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1717;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1717;
                    aux_inner_valve_out = 1;
                end
            end
            s_1717:begin
                
                    next_scan_cycle = s_1719;
                    state_next = IDLE;
            end
            s_1719:begin
                state_next = s_1721;
            end
            s_1721:begin
                state_next = s_1723;
            end
            s_1723:begin
                state_next = s_1725; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1725:begin
                state_next = s_1727; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1727:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1729;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1729;
                    aux_inner_valve_out = 1;
                end
            end
            s_1729:begin
                
                    next_scan_cycle = s_1731;
                    state_next = IDLE;
            end
            s_1731:begin
                state_next = s_1733;
            end
            s_1733:begin
                state_next = s_1735;
            end
            s_1735:begin
                state_next = s_1737; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1737:begin
                state_next = s_1739; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1739:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1741;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1741;
                    aux_inner_valve_out = 1;
                end
            end
            s_1741:begin
                
                    next_scan_cycle = s_1743;
                    state_next = IDLE;
            end
            s_1743:begin
                state_next = s_1745;
            end
            s_1745:begin
                state_next = s_1747;
            end
            s_1747:begin
                state_next = s_1749; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1749:begin
                state_next = s_1751; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1751:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1753;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1753;
                    aux_inner_valve_out = 1;
                end
            end
            s_1753:begin
                
                    next_scan_cycle = s_1755;
                    state_next = IDLE;
            end
            s_1755:begin
                state_next = s_1757;
            end
            s_1757:begin
                state_next = s_1759;
            end
            s_1759:begin
                state_next = s_1761; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1761:begin
                state_next = s_1763; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1763:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1765;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1765;
                    aux_inner_valve_out = 1;
                end
            end
            s_1765:begin
                
                    next_scan_cycle = s_1767;
                    state_next = IDLE;
            end
            s_1767:begin
                state_next = s_1769;
            end
            s_1769:begin
                state_next = s_1771;
            end
            s_1771:begin
                state_next = s_1773; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1773:begin
                state_next = s_1775; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1775:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1777;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1777;
                    aux_inner_valve_out = 1;
                end
            end
            s_1777:begin
                
                    next_scan_cycle = s_1779;
                    state_next = IDLE;
            end
            s_1779:begin
                state_next = s_1781;
            end
            s_1781:begin
                state_next = s_1783;
            end
            s_1783:begin
                state_next = s_1785; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1785:begin
                state_next = s_1787; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1787:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1789;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1789;
                    aux_inner_valve_out = 1;
                end
            end
            s_1789:begin
                
                    next_scan_cycle = s_1791;
                    state_next = IDLE;
            end
            s_1791:begin
                state_next = s_1793;
            end
            s_1793:begin
                state_next = s_1795;
            end
            s_1795:begin
                state_next = s_1797; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1797:begin
                state_next = s_1799; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1799:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1801;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1801;
                    aux_inner_valve_out = 1;
                end
            end
            s_1801:begin
                
                    next_scan_cycle = s_1803;
                    state_next = IDLE;
            end
            s_1803:begin
                state_next = s_1805;
            end
            s_1805:begin
                state_next = s_1807;
            end
            s_1807:begin
                state_next = s_1809; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1809:begin
                state_next = s_1811; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1811:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1813;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1813;
                    aux_inner_valve_out = 1;
                end
            end
            s_1813:begin
                
                    next_scan_cycle = s_1815;
                    state_next = IDLE;
            end
            s_1815:begin
                state_next = s_1817;
            end
            s_1817:begin
                state_next = s_1819;
            end
            s_1819:begin
                state_next = s_1821; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1821:begin
                state_next = s_1823; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1823:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1825;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1825;
                    aux_inner_valve_out = 1;
                end
            end
            s_1825:begin
                
                    next_scan_cycle = s_1827;
                    state_next = IDLE;
            end
            s_1827:begin
                state_next = s_1829;
            end
            s_1829:begin
                state_next = s_1831;
            end
            s_1831:begin
                state_next = s_1833; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1833:begin
                state_next = s_1835; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1835:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1837;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1837;
                    aux_inner_valve_out = 1;
                end
            end
            s_1837:begin
                
                    next_scan_cycle = s_1839;
                    state_next = IDLE;
            end
            s_1839:begin
                state_next = s_1841;
            end
            s_1841:begin
                state_next = s_1843;
            end
            s_1843:begin
                state_next = s_1845; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1845:begin
                state_next = s_1847; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1847:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1849;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1849;
                    aux_inner_valve_out = 1;
                end
            end
            s_1849:begin
                
                    next_scan_cycle = s_1851;
                    state_next = IDLE;
            end
            s_1851:begin
                state_next = s_1853;
            end
            s_1853:begin
                state_next = s_1855;
            end
            s_1855:begin
                state_next = s_1857; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1857:begin
                state_next = s_1859; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1859:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1861;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1861;
                    aux_inner_valve_out = 1;
                end
            end
            s_1861:begin
                
                    next_scan_cycle = s_1863;
                    state_next = IDLE;
            end
            s_1863:begin
                state_next = s_1865;
            end
            s_1865:begin
                state_next = s_1867;
            end
            s_1867:begin
                state_next = s_1869; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1869:begin
                state_next = s_1871; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1871:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1873;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1873;
                    aux_inner_valve_out = 1;
                end
            end
            s_1873:begin
                
                    next_scan_cycle = s_1875;
                    state_next = IDLE;
            end
            s_1875:begin
                state_next = s_1877;
            end
            s_1877:begin
                state_next = s_1879;
            end
            s_1879:begin
                state_next = s_1881; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1881:begin
                state_next = s_1883; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1883:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1885;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1885;
                    aux_inner_valve_out = 1;
                end
            end
            s_1885:begin
                
                    next_scan_cycle = s_1887;
                    state_next = IDLE;
            end
            s_1887:begin
                state_next = s_1889;
            end
            s_1889:begin
                state_next = s_1891;
            end
            s_1891:begin
                state_next = s_1893; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1893:begin
                state_next = s_1895; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1895:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1897;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1897;
                    aux_inner_valve_out = 1;
                end
            end
            s_1897:begin
                
                    next_scan_cycle = s_1899;
                    state_next = IDLE;
            end
            s_1899:begin
                state_next = s_1901;
            end
            s_1901:begin
                state_next = s_1903;
            end
            s_1903:begin
                state_next = s_1905; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1905:begin
                state_next = s_1907; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1907:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1909;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1909;
                    aux_inner_valve_out = 1;
                end
            end
            s_1909:begin
                
                    next_scan_cycle = s_1911;
                    state_next = IDLE;
            end
            s_1911:begin
                state_next = s_1913;
            end
            s_1913:begin
                state_next = s_1915;
            end
            s_1915:begin
                state_next = s_1917; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1917:begin
                state_next = s_1919; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1919:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1921;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1921;
                    aux_inner_valve_out = 1;
                end
            end
            s_1921:begin
                
                    next_scan_cycle = s_1923;
                    state_next = IDLE;
            end
            s_1923:begin
                state_next = s_1925;
            end
            s_1925:begin
                state_next = s_1927;
            end
            s_1927:begin
                state_next = s_1929; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1929:begin
                state_next = s_1931; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1931:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1933;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1933;
                    aux_inner_valve_out = 1;
                end
            end
            s_1933:begin
                
                    next_scan_cycle = s_1935;
                    state_next = IDLE;
            end
            s_1935:begin
                state_next = s_1937;
            end
            s_1937:begin
                state_next = s_1939;
            end
            s_1939:begin
                state_next = s_1941; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1941:begin
                state_next = s_1943; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1943:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1945;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1945;
                    aux_inner_valve_out = 1;
                end
            end
            s_1945:begin
                
                    next_scan_cycle = s_1947;
                    state_next = IDLE;
            end
            s_1947:begin
                state_next = s_1949;
            end
            s_1949:begin
                state_next = s_1951;
            end
            s_1951:begin
                state_next = s_1953; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1953:begin
                state_next = s_1955; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1955:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1957;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1957;
                    aux_inner_valve_out = 1;
                end
            end
            s_1957:begin
                
                    next_scan_cycle = s_1959;
                    state_next = IDLE;
            end
            s_1959:begin
                state_next = s_1961;
            end
            s_1961:begin
                state_next = s_1963;
            end
            s_1963:begin
                state_next = s_1965; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1965:begin
                state_next = s_1967; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1967:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1969;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1969;
                    aux_inner_valve_out = 1;
                end
            end
            s_1969:begin
                
                    next_scan_cycle = s_1971;
                    state_next = IDLE;
            end
            s_1971:begin
                state_next = s_1973;
            end
            s_1973:begin
                state_next = s_1975;
            end
            s_1975:begin
                state_next = s_1977; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1977:begin
                state_next = s_1979; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1979:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1981;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1981;
                    aux_inner_valve_out = 1;
                end
            end
            s_1981:begin
                
                    next_scan_cycle = s_1983;
                    state_next = IDLE;
            end
            s_1983:begin
                state_next = s_1985;
            end
            s_1985:begin
                state_next = s_1987;
            end
            s_1987:begin
                state_next = s_1989; aux_inner_pump1_out = inner_pump1_in;
            end
            s_1989:begin
                state_next = s_1991; aux_inner_pump2_out = inner_pump2_in;
            end
            s_1991:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_1993;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_1993;
                    aux_inner_valve_out = 1;
                end
            end
            s_1993:begin
                
                    next_scan_cycle = s_1995;
                    state_next = IDLE;
            end
            s_1995:begin
                state_next = s_1997;
            end
            s_1997:begin
                state_next = s_1999;
            end
            s_1999:begin
                state_next = s_2001; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2001:begin
                state_next = s_2003; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2003:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2005;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2005;
                    aux_inner_valve_out = 1;
                end
            end
            s_2005:begin
                
                    next_scan_cycle = s_2007;
                    state_next = IDLE;
            end
            s_2007:begin
                state_next = s_2009;
            end
            s_2009:begin
                state_next = s_2011;
            end
            s_2011:begin
                state_next = s_2013; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2013:begin
                state_next = s_2015; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2015:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2017;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2017;
                    aux_inner_valve_out = 1;
                end
            end
            s_2017:begin
                
                    next_scan_cycle = s_2019;
                    state_next = IDLE;
            end
            s_2019:begin
                state_next = s_2021;
            end
            s_2021:begin
                state_next = s_2023;
            end
            s_2023:begin
                state_next = s_2025; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2025:begin
                state_next = s_2027; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2027:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2029;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2029;
                    aux_inner_valve_out = 1;
                end
            end
            s_2029:begin
                
                    next_scan_cycle = s_2031;
                    state_next = IDLE;
            end
            s_2031:begin
                state_next = s_2033;
            end
            s_2033:begin
                state_next = s_2035;
            end
            s_2035:begin
                state_next = s_2037; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2037:begin
                state_next = s_2039; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2039:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2041;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2041;
                    aux_inner_valve_out = 1;
                end
            end
            s_2041:begin
                
                    next_scan_cycle = s_2043;
                    state_next = IDLE;
            end
            s_2043:begin
                state_next = s_2045;
            end
            s_2045:begin
                state_next = s_2047;
            end
            s_2047:begin
                state_next = s_2049; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2049:begin
                state_next = s_2051; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2051:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2053;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2053;
                    aux_inner_valve_out = 1;
                end
            end
            s_2053:begin
                
                    next_scan_cycle = s_2055;
                    state_next = IDLE;
            end
            s_2055:begin
                state_next = s_2057;
            end
            s_2057:begin
                state_next = s_2059;
            end
            s_2059:begin
                state_next = s_2061; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2061:begin
                state_next = s_2063; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2063:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2065;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2065;
                    aux_inner_valve_out = 1;
                end
            end
            s_2065:begin
                
                    next_scan_cycle = s_2067;
                    state_next = IDLE;
            end
            s_2067:begin
                state_next = s_2069;
            end
            s_2069:begin
                state_next = s_2071;
            end
            s_2071:begin
                state_next = s_2073; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2073:begin
                state_next = s_2075; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2075:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2077;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2077;
                    aux_inner_valve_out = 1;
                end
            end
            s_2077:begin
                
                    next_scan_cycle = s_2079;
                    state_next = IDLE;
            end
            s_2079:begin
                state_next = s_2081;
            end
            s_2081:begin
                state_next = s_2083;
            end
            s_2083:begin
                state_next = s_2085; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2085:begin
                state_next = s_2087; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2087:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2089;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2089;
                    aux_inner_valve_out = 1;
                end
            end
            s_2089:begin
                
                    next_scan_cycle = s_2091;
                    state_next = IDLE;
            end
            s_2091:begin
                state_next = s_2093;
            end
            s_2093:begin
                state_next = s_2095;
            end
            s_2095:begin
                state_next = s_2097; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2097:begin
                state_next = s_2099; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2099:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2101;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2101;
                    aux_inner_valve_out = 1;
                end
            end
            s_2101:begin
                
                    next_scan_cycle = s_2103;
                    state_next = IDLE;
            end
            s_2103:begin
                state_next = s_2105;
            end
            s_2105:begin
                state_next = s_2107;
            end
            s_2107:begin
                state_next = s_2109; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2109:begin
                state_next = s_2111; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2111:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2113;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2113;
                    aux_inner_valve_out = 1;
                end
            end
            s_2113:begin
                
                    next_scan_cycle = s_2115;
                    state_next = IDLE;
            end
            s_2115:begin
                state_next = s_2117;
            end
            s_2117:begin
                state_next = s_2119;
            end
            s_2119:begin
                state_next = s_2121; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2121:begin
                state_next = s_2123; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2123:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2125;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2125;
                    aux_inner_valve_out = 1;
                end
            end
            s_2125:begin
                
                    next_scan_cycle = s_2127;
                    state_next = IDLE;
            end
            s_2127:begin
                state_next = s_2129;
            end
            s_2129:begin
                state_next = s_2131;
            end
            s_2131:begin
                state_next = s_2133; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2133:begin
                state_next = s_2135; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2135:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2137;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2137;
                    aux_inner_valve_out = 1;
                end
            end
            s_2137:begin
                
                    next_scan_cycle = s_2139;
                    state_next = IDLE;
            end
            s_2139:begin
                state_next = s_2141;
            end
            s_2141:begin
                state_next = s_2143;
            end
            s_2143:begin
                state_next = s_2145; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2145:begin
                state_next = s_2147; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2147:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2149;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2149;
                    aux_inner_valve_out = 1;
                end
            end
            s_2149:begin
                
                    next_scan_cycle = s_2151;
                    state_next = IDLE;
            end
            s_2151:begin
                state_next = s_2153;
            end
            s_2153:begin
                state_next = s_2155;
            end
            s_2155:begin
                state_next = s_2157; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2157:begin
                state_next = s_2159; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2159:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2161;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2161;
                    aux_inner_valve_out = 1;
                end
            end
            s_2161:begin
                
                    next_scan_cycle = s_2163;
                    state_next = IDLE;
            end
            s_2163:begin
                state_next = s_2165;
            end
            s_2165:begin
                state_next = s_2167;
            end
            s_2167:begin
                state_next = s_2169; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2169:begin
                state_next = s_2171; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2171:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2173;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2173;
                    aux_inner_valve_out = 1;
                end
            end
            s_2173:begin
                
                    next_scan_cycle = s_2175;
                    state_next = IDLE;
            end
            s_2175:begin
                state_next = s_2177;
            end
            s_2177:begin
                state_next = s_2179;
            end
            s_2179:begin
                state_next = s_2181; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2181:begin
                state_next = s_2183; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2183:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2185;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2185;
                    aux_inner_valve_out = 1;
                end
            end
            s_2185:begin
                
                    next_scan_cycle = s_2187;
                    state_next = IDLE;
            end
            s_2187:begin
                state_next = s_2189;
            end
            s_2189:begin
                state_next = s_2191;
            end
            s_2191:begin
                state_next = s_2193; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2193:begin
                state_next = s_2195; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2195:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2197;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2197;
                    aux_inner_valve_out = 1;
                end
            end
            s_2197:begin
                
                    next_scan_cycle = s_2199;
                    state_next = IDLE;
            end
            s_2199:begin
                state_next = s_2201;
            end
            s_2201:begin
                state_next = s_2203;
            end
            s_2203:begin
                state_next = s_2205; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2205:begin
                state_next = s_2207; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2207:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2209;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2209;
                    aux_inner_valve_out = 1;
                end
            end
            s_2209:begin
                
                    next_scan_cycle = s_2211;
                    state_next = IDLE;
            end
            s_2211:begin
                state_next = s_2213;
            end
            s_2213:begin
                state_next = s_2215;
            end
            s_2215:begin
                state_next = s_2217; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2217:begin
                state_next = s_2219; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2219:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2221;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2221;
                    aux_inner_valve_out = 1;
                end
            end
            s_2221:begin
                
                    next_scan_cycle = s_2223;
                    state_next = IDLE;
            end
            s_2223:begin
                state_next = s_2225;
            end
            s_2225:begin
                state_next = s_2227;
            end
            s_2227:begin
                state_next = s_2229; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2229:begin
                state_next = s_2231; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2231:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2233;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2233;
                    aux_inner_valve_out = 1;
                end
            end
            s_2233:begin
                
                    next_scan_cycle = s_2235;
                    state_next = IDLE;
            end
            s_2235:begin
                state_next = s_2237;
            end
            s_2237:begin
                state_next = s_2239;
            end
            s_2239:begin
                state_next = s_2241; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2241:begin
                state_next = s_2243; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2243:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2245;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2245;
                    aux_inner_valve_out = 1;
                end
            end
            s_2245:begin
                
                    next_scan_cycle = s_2247;
                    state_next = IDLE;
            end
            s_2247:begin
                state_next = s_2249;
            end
            s_2249:begin
                state_next = s_2251;
            end
            s_2251:begin
                state_next = s_2253; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2253:begin
                state_next = s_2255; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2255:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2257;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2257;
                    aux_inner_valve_out = 1;
                end
            end
            s_2257:begin
                
                    next_scan_cycle = s_2259;
                    state_next = IDLE;
            end
            s_2259:begin
                state_next = s_2261;
            end
            s_2261:begin
                state_next = s_2263;
            end
            s_2263:begin
                state_next = s_2265; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2265:begin
                state_next = s_2267; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2267:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2269;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2269;
                    aux_inner_valve_out = 1;
                end
            end
            s_2269:begin
                
                    next_scan_cycle = s_2271;
                    state_next = IDLE;
            end
            s_2271:begin
                state_next = s_2273;
            end
            s_2273:begin
                state_next = s_2275;
            end
            s_2275:begin
                state_next = s_2277; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2277:begin
                state_next = s_2279; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2279:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2281;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2281;
                    aux_inner_valve_out = 1;
                end
            end
            s_2281:begin
                
                    next_scan_cycle = s_2283;
                    state_next = IDLE;
            end
            s_2283:begin
                state_next = s_2285;
            end
            s_2285:begin
                state_next = s_2287;
            end
            s_2287:begin
                state_next = s_2289; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2289:begin
                state_next = s_2291; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2291:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2293;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2293;
                    aux_inner_valve_out = 1;
                end
            end
            s_2293:begin
                
                    next_scan_cycle = s_2295;
                    state_next = IDLE;
            end
            s_2295:begin
                state_next = s_2297;
            end
            s_2297:begin
                state_next = s_2299;
            end
            s_2299:begin
                state_next = s_2301; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2301:begin
                state_next = s_2303; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2303:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2305;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2305;
                    aux_inner_valve_out = 1;
                end
            end
            s_2305:begin
                
                    next_scan_cycle = s_2307;
                    state_next = IDLE;
            end
            s_2307:begin
                state_next = s_2309;
            end
            s_2309:begin
                state_next = s_2311;
            end
            s_2311:begin
                state_next = s_2313; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2313:begin
                state_next = s_2315; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2315:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2317;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2317;
                    aux_inner_valve_out = 1;
                end
            end
            s_2317:begin
                
                    next_scan_cycle = s_2319;
                    state_next = IDLE;
            end
            s_2319:begin
                state_next = s_2321;
            end
            s_2321:begin
                state_next = s_2323;
            end
            s_2323:begin
                state_next = s_2325; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2325:begin
                state_next = s_2327; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2327:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2329;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2329;
                    aux_inner_valve_out = 1;
                end
            end
            s_2329:begin
                
                    next_scan_cycle = s_2331;
                    state_next = IDLE;
            end
            s_2331:begin
                state_next = s_2333;
            end
            s_2333:begin
                state_next = s_2335;
            end
            s_2335:begin
                state_next = s_2337; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2337:begin
                state_next = s_2339; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2339:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2341;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2341;
                    aux_inner_valve_out = 1;
                end
            end
            s_2341:begin
                
                    next_scan_cycle = s_2343;
                    state_next = IDLE;
            end
            s_2343:begin
                state_next = s_2345;
            end
            s_2345:begin
                state_next = s_2347;
            end
            s_2347:begin
                state_next = s_2349; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2349:begin
                state_next = s_2351; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2351:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2353;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2353;
                    aux_inner_valve_out = 1;
                end
            end
            s_2353:begin
                
                    next_scan_cycle = s_2355;
                    state_next = IDLE;
            end
            s_2355:begin
                state_next = s_2357;
            end
            s_2357:begin
                state_next = s_2359;
            end
            s_2359:begin
                state_next = s_2361; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2361:begin
                state_next = s_2363; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2363:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2365;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2365;
                    aux_inner_valve_out = 1;
                end
            end
            s_2365:begin
                
                    next_scan_cycle = s_2367;
                    state_next = IDLE;
            end
            s_2367:begin
                state_next = s_2369;
            end
            s_2369:begin
                state_next = s_2371;
            end
            s_2371:begin
                state_next = s_2373; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2373:begin
                state_next = s_2375; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2375:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2377;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2377;
                    aux_inner_valve_out = 1;
                end
            end
            s_2377:begin
                
                    next_scan_cycle = s_2379;
                    state_next = IDLE;
            end
            s_2379:begin
                state_next = s_2381;
            end
            s_2381:begin
                state_next = s_2383;
            end
            s_2383:begin
                state_next = s_2385; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2385:begin
                state_next = s_2387; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2387:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2389;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2389;
                    aux_inner_valve_out = 1;
                end
            end
            s_2389:begin
                
                    next_scan_cycle = s_2391;
                    state_next = IDLE;
            end
            s_2391:begin
                state_next = s_2393;
            end
            s_2393:begin
                state_next = s_2395;
            end
            s_2395:begin
                state_next = s_2397; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2397:begin
                state_next = s_2399; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2399:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2401;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2401;
                    aux_inner_valve_out = 1;
                end
            end
            s_2401:begin
                
                    next_scan_cycle = s_2403;
                    state_next = IDLE;
            end
            s_2403:begin
                state_next = s_2405;
            end
            s_2405:begin
                state_next = s_2407;
            end
            s_2407:begin
                state_next = s_2409; aux_inner_pump1_out = inner_pump1_in;
            end
            s_2409:begin
                state_next = s_2411; aux_inner_pump2_out = inner_pump2_in;
            end
            s_2411:begin
                
                if (inner_valve_in == 1) begin
                    state_next = s_2413;
                    aux_inner_valve_out = 1;
                end
                else begin
                    state_next = s_2413;
                    aux_inner_valve_out = 1;
                end
            end
            s_2413:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
    endcase

end
/*
Output assignments
*/
//assign output variable

assign  inner_pump1_out = to_out_inner_pump1_out;
assign  inner_pump2_out = to_out_inner_pump2_out;
assign  inner_valve_out = to_out_inner_valve_out;
endmodule


