/*
States declaration of combinatorial module
*/
`timescale 10ms/1ms
//timescale 1 ns/10 ps




module prop_PLC3_combinatorial(
/*
Input/Output ports
*/
//for each variable

input  wire [31:0] inner_level_in,

input  wire [31:0] inner_pump_in,
output wire [31:0] inner_pump_out,
input signal_start,
input wire clk
);
localparam IDLE = 0;
localparam initial_state = 1; 
localparam s_3 = 2; 
localparam s_5 = 3; 
localparam s_7 = 4; 
localparam s_9 = 5; 
localparam s_11 = 6; 
localparam s_13 = 7; 
localparam s_15 = 8; 
localparam s_17 = 9; 
localparam s_19 = 10; 
localparam s_21 = 11; 
localparam s_23 = 12; 
localparam s_25 = 13; 
localparam s_27 = 14; 
localparam s_29 = 15; 
localparam s_31 = 16; 
localparam s_33 = 17; 
localparam s_35 = 18; 
localparam s_37 = 19; 
localparam s_39 = 20; 
localparam s_41 = 21; 
localparam s_43 = 22; 
localparam s_45 = 23; 
localparam s_47 = 24; 
localparam s_49 = 25; 
localparam s_51 = 26; 
localparam s_53 = 27; 
localparam s_55 = 28; 
localparam s_57 = 29; 
localparam s_59 = 30; 
localparam s_61 = 31; 
localparam s_63 = 32; 
localparam s_65 = 33; 
localparam s_67 = 34; 
localparam s_69 = 35; 
localparam s_71 = 36; 
localparam s_73 = 37; 
localparam s_75 = 38; 
localparam s_77 = 39; 
localparam s_79 = 40; 
localparam s_81 = 41; 
localparam s_83 = 42; 
localparam s_85 = 43; 
localparam s_87 = 44; 
localparam s_89 = 45; 
localparam s_91 = 46; 
localparam s_93 = 47; 
localparam s_95 = 48; 
localparam s_97 = 49; 
localparam s_99 = 50; 
localparam s_101 = 51; 
localparam s_103 = 52; 
localparam s_105 = 53; 
localparam s_107 = 54; 
localparam s_109 = 55; 
localparam s_111 = 56; 
localparam s_113 = 57; 
localparam s_115 = 58; 
localparam s_117 = 59; 
localparam s_119 = 60; 
localparam s_121 = 61; 
localparam s_123 = 62; 
localparam s_125 = 63; 
localparam s_127 = 64; 
localparam s_129 = 65; 
localparam s_131 = 66; 
localparam s_133 = 67; 
localparam s_135 = 68; 
localparam s_137 = 69; 
localparam s_139 = 70; 
localparam s_141 = 71; 
localparam s_143 = 72; 
localparam s_145 = 73; 
localparam s_147 = 74; 
localparam s_149 = 75; 
localparam s_151 = 76; 
localparam s_153 = 77; 
localparam s_155 = 78; 
localparam s_157 = 79; 
localparam s_159 = 80; 
localparam s_161 = 81; 
localparam s_163 = 82; 
localparam s_165 = 83; 
localparam s_167 = 84; 
localparam s_169 = 85; 
localparam s_171 = 86; 
localparam s_173 = 87; 
localparam s_175 = 88; 
localparam s_177 = 89; 
localparam s_179 = 90; 
localparam s_181 = 91; 
localparam s_183 = 92; 
localparam s_185 = 93; 
localparam s_187 = 94; 
localparam s_189 = 95; 
localparam s_191 = 96; 
localparam s_193 = 97; 
localparam s_195 = 98; 
localparam s_197 = 99; 
localparam s_199 = 100; 
localparam s_201 = 101; 
localparam s_203 = 102; 
localparam s_205 = 103; 
localparam s_207 = 104; 
localparam s_209 = 105; 
localparam s_211 = 106; 
localparam s_213 = 107; 
localparam s_215 = 108; 
localparam s_217 = 109; 
localparam s_219 = 110; 
localparam s_221 = 111; 
localparam s_223 = 112; 
localparam s_225 = 113; 
localparam s_227 = 114; 
localparam s_229 = 115; 
localparam s_231 = 116; 
localparam s_233 = 117; 
localparam s_235 = 118; 
localparam s_237 = 119; 
localparam s_239 = 120; 
localparam s_241 = 121; 
localparam s_243 = 122; 

reg [7:0] state, state_next, next_scan_cycle;
reg [31:0] aux_inner_pump_out;
reg [31:0] to_out_inner_pump_out = 'd1;


initial begin
state = IDLE;
state_next = IDLE;
next_scan_cycle = initial_state;
end
always @(posedge clk) begin
    state <= state_next;
end

always @* begin

    case(state)
        IDLE : begin
            
        to_out_inner_pump_out = aux_inner_pump_out;
            if (signal_start == 'b1) begin
                state_next = next_scan_cycle;
            end
        end
        
            initial_state:begin
                
                if (inner_level_in >= 10) begin
                    state_next = s_3;
                    
                end
                else if (inner_level_in < 10) begin
                    state_next = s_241;
                    
                end
            end
            s_3:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_5;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_5;
                    aux_inner_pump_out = 1;
                end
            end
            s_5:begin
                
                    next_scan_cycle = s_7;
                    state_next = IDLE;
            end
            s_7:begin
                state_next = s_9;
            end
            s_9:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_11;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_11;
                    aux_inner_pump_out = 1;
                end
            end
            s_11:begin
                
                    next_scan_cycle = s_13;
                    state_next = IDLE;
            end
            s_13:begin
                state_next = s_15;
            end
            s_15:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_17;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_17;
                    aux_inner_pump_out = 1;
                end
            end
            s_17:begin
                
                    next_scan_cycle = s_19;
                    state_next = IDLE;
            end
            s_19:begin
                state_next = s_21;
            end
            s_21:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_23;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_23;
                    aux_inner_pump_out = 1;
                end
            end
            s_23:begin
                
                    next_scan_cycle = s_25;
                    state_next = IDLE;
            end
            s_25:begin
                state_next = s_27;
            end
            s_27:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_29;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_29;
                    aux_inner_pump_out = 1;
                end
            end
            s_29:begin
                
                    next_scan_cycle = s_31;
                    state_next = IDLE;
            end
            s_31:begin
                state_next = s_33;
            end
            s_33:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_35;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_35;
                    aux_inner_pump_out = 1;
                end
            end
            s_35:begin
                
                    next_scan_cycle = s_37;
                    state_next = IDLE;
            end
            s_37:begin
                state_next = s_39;
            end
            s_39:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_41;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_41;
                    aux_inner_pump_out = 1;
                end
            end
            s_41:begin
                
                    next_scan_cycle = s_43;
                    state_next = IDLE;
            end
            s_43:begin
                state_next = s_45;
            end
            s_45:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_47;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_47;
                    aux_inner_pump_out = 1;
                end
            end
            s_47:begin
                
                    next_scan_cycle = s_49;
                    state_next = IDLE;
            end
            s_49:begin
                state_next = s_51;
            end
            s_51:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_53;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_53;
                    aux_inner_pump_out = 1;
                end
            end
            s_53:begin
                
                    next_scan_cycle = s_55;
                    state_next = IDLE;
            end
            s_55:begin
                state_next = s_57;
            end
            s_57:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_59;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_59;
                    aux_inner_pump_out = 1;
                end
            end
            s_59:begin
                
                    next_scan_cycle = s_61;
                    state_next = IDLE;
            end
            s_61:begin
                state_next = s_63;
            end
            s_63:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_65;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_65;
                    aux_inner_pump_out = 1;
                end
            end
            s_65:begin
                
                    next_scan_cycle = s_67;
                    state_next = IDLE;
            end
            s_67:begin
                state_next = s_69;
            end
            s_69:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_71;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_71;
                    aux_inner_pump_out = 1;
                end
            end
            s_71:begin
                
                    next_scan_cycle = s_73;
                    state_next = IDLE;
            end
            s_73:begin
                state_next = s_75;
            end
            s_75:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_77;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_77;
                    aux_inner_pump_out = 1;
                end
            end
            s_77:begin
                
                    next_scan_cycle = s_79;
                    state_next = IDLE;
            end
            s_79:begin
                state_next = s_81;
            end
            s_81:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_83;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_83;
                    aux_inner_pump_out = 1;
                end
            end
            s_83:begin
                
                    next_scan_cycle = s_85;
                    state_next = IDLE;
            end
            s_85:begin
                state_next = s_87;
            end
            s_87:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_89;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_89;
                    aux_inner_pump_out = 1;
                end
            end
            s_89:begin
                
                    next_scan_cycle = s_91;
                    state_next = IDLE;
            end
            s_91:begin
                state_next = s_93;
            end
            s_93:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_95;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_95;
                    aux_inner_pump_out = 1;
                end
            end
            s_95:begin
                
                    next_scan_cycle = s_97;
                    state_next = IDLE;
            end
            s_97:begin
                state_next = s_99;
            end
            s_99:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_101;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_101;
                    aux_inner_pump_out = 1;
                end
            end
            s_101:begin
                
                    next_scan_cycle = s_103;
                    state_next = IDLE;
            end
            s_103:begin
                state_next = s_105;
            end
            s_105:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_107;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_107;
                    aux_inner_pump_out = 1;
                end
            end
            s_107:begin
                
                    next_scan_cycle = s_109;
                    state_next = IDLE;
            end
            s_109:begin
                state_next = s_111;
            end
            s_111:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_113;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_113;
                    aux_inner_pump_out = 1;
                end
            end
            s_113:begin
                
                    next_scan_cycle = s_115;
                    state_next = IDLE;
            end
            s_115:begin
                state_next = s_117;
            end
            s_117:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_119;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_119;
                    aux_inner_pump_out = 1;
                end
            end
            s_119:begin
                
                    next_scan_cycle = s_121;
                    state_next = IDLE;
            end
            s_121:begin
                state_next = s_123;
            end
            s_123:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_125;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_125;
                    aux_inner_pump_out = 1;
                end
            end
            s_125:begin
                
                    next_scan_cycle = s_127;
                    state_next = IDLE;
            end
            s_127:begin
                state_next = s_129;
            end
            s_129:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_131;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_131;
                    aux_inner_pump_out = 1;
                end
            end
            s_131:begin
                
                    next_scan_cycle = s_133;
                    state_next = IDLE;
            end
            s_133:begin
                state_next = s_135;
            end
            s_135:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_137;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_137;
                    aux_inner_pump_out = 1;
                end
            end
            s_137:begin
                
                    next_scan_cycle = s_139;
                    state_next = IDLE;
            end
            s_139:begin
                state_next = s_141;
            end
            s_141:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_143;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_143;
                    aux_inner_pump_out = 1;
                end
            end
            s_143:begin
                
                    next_scan_cycle = s_145;
                    state_next = IDLE;
            end
            s_145:begin
                state_next = s_147;
            end
            s_147:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_149;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_149;
                    aux_inner_pump_out = 1;
                end
            end
            s_149:begin
                
                    next_scan_cycle = s_151;
                    state_next = IDLE;
            end
            s_151:begin
                state_next = s_153;
            end
            s_153:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_155;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_155;
                    aux_inner_pump_out = 1;
                end
            end
            s_155:begin
                
                    next_scan_cycle = s_157;
                    state_next = IDLE;
            end
            s_157:begin
                state_next = s_159;
            end
            s_159:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_161;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_161;
                    aux_inner_pump_out = 1;
                end
            end
            s_161:begin
                
                    next_scan_cycle = s_163;
                    state_next = IDLE;
            end
            s_163:begin
                state_next = s_165;
            end
            s_165:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_167;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_167;
                    aux_inner_pump_out = 1;
                end
            end
            s_167:begin
                
                    next_scan_cycle = s_169;
                    state_next = IDLE;
            end
            s_169:begin
                state_next = s_171;
            end
            s_171:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_173;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_173;
                    aux_inner_pump_out = 1;
                end
            end
            s_173:begin
                
                    next_scan_cycle = s_175;
                    state_next = IDLE;
            end
            s_175:begin
                state_next = s_177;
            end
            s_177:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_179;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_179;
                    aux_inner_pump_out = 1;
                end
            end
            s_179:begin
                
                    next_scan_cycle = s_181;
                    state_next = IDLE;
            end
            s_181:begin
                state_next = s_183;
            end
            s_183:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_185;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_185;
                    aux_inner_pump_out = 1;
                end
            end
            s_185:begin
                
                    next_scan_cycle = s_187;
                    state_next = IDLE;
            end
            s_187:begin
                state_next = s_189;
            end
            s_189:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_191;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_191;
                    aux_inner_pump_out = 1;
                end
            end
            s_191:begin
                
                    next_scan_cycle = s_193;
                    state_next = IDLE;
            end
            s_193:begin
                state_next = s_195;
            end
            s_195:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_197;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_197;
                    aux_inner_pump_out = 1;
                end
            end
            s_197:begin
                
                    next_scan_cycle = s_199;
                    state_next = IDLE;
            end
            s_199:begin
                state_next = s_201;
            end
            s_201:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_203;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_203;
                    aux_inner_pump_out = 1;
                end
            end
            s_203:begin
                
                    next_scan_cycle = s_205;
                    state_next = IDLE;
            end
            s_205:begin
                state_next = s_207;
            end
            s_207:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_209;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_209;
                    aux_inner_pump_out = 1;
                end
            end
            s_209:begin
                
                    next_scan_cycle = s_211;
                    state_next = IDLE;
            end
            s_211:begin
                state_next = s_213;
            end
            s_213:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_215;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_215;
                    aux_inner_pump_out = 1;
                end
            end
            s_215:begin
                
                    next_scan_cycle = s_217;
                    state_next = IDLE;
            end
            s_217:begin
                state_next = s_219;
            end
            s_219:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_221;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_221;
                    aux_inner_pump_out = 1;
                end
            end
            s_221:begin
                
                    next_scan_cycle = s_223;
                    state_next = IDLE;
            end
            s_223:begin
                state_next = s_225;
            end
            s_225:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_227;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_227;
                    aux_inner_pump_out = 1;
                end
            end
            s_227:begin
                
                    next_scan_cycle = s_229;
                    state_next = IDLE;
            end
            s_229:begin
                state_next = s_231;
            end
            s_231:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_233;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_233;
                    aux_inner_pump_out = 1;
                end
            end
            s_233:begin
                
                    next_scan_cycle = s_235;
                    state_next = IDLE;
            end
            s_235:begin
                state_next = s_237;
            end
            s_237:begin
                
                if (inner_pump_in == 1) begin
                    state_next = s_239;
                    aux_inner_pump_out = 1;
                end
                else begin
                    state_next = s_239;
                    aux_inner_pump_out = 1;
                end
            end
            s_239:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
            s_241:begin
                state_next = s_243; aux_inner_pump_out = inner_pump_in;
            end
            s_243:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
    endcase

end
/*
Output assignments
*/
//assign output variable

assign  inner_pump_out = to_out_inner_pump_out;
endmodule


