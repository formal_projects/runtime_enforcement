/*
States declaration of combinatorial module
*/
`timescale 10ms/1ms
//timescale 1 ns/10 ps




module prop_PLC3_combinatorial(
/*
Input/Output ports
*/
//for each variable

input  wire [31:0] inner_level_in,

input  wire [31:0] inner_pump_in,
output wire [31:0] inner_pump_out,
input signal_start,
input wire clk
);
localparam IDLE = 0;
localparam initial_state = 1; 
localparam s_3 = 2; 
localparam s_5 = 3; 
localparam s_7 = 4; 
localparam s_9 = 5; 
localparam s_11 = 6; 
localparam s_13 = 7; 
localparam s_15 = 8; 
localparam s_17 = 9; 
localparam s_19 = 10; 
localparam s_21 = 11; 
localparam s_23 = 12; 
localparam s_25 = 13; 
localparam s_27 = 14; 
localparam s_29 = 15; 
localparam s_31 = 16; 
localparam s_33 = 17; 
localparam s_35 = 18; 
localparam s_37 = 19; 
localparam s_39 = 20; 
localparam s_41 = 21; 
localparam s_43 = 22; 
localparam s_45 = 23; 
localparam s_47 = 24; 
localparam s_49 = 25; 
localparam s_51 = 26; 
localparam s_53 = 27; 
localparam s_55 = 28; 
localparam s_57 = 29; 
localparam s_59 = 30; 
localparam s_61 = 31; 
localparam s_63 = 32; 
localparam s_65 = 33; 
localparam s_67 = 34; 
localparam s_69 = 35; 
localparam s_71 = 36; 
localparam s_73 = 37; 
localparam s_75 = 38; 
localparam s_77 = 39; 
localparam s_79 = 40; 
localparam s_81 = 41; 
localparam s_83 = 42; 
localparam s_85 = 43; 
localparam s_87 = 44; 
localparam s_89 = 45; 
localparam s_91 = 46; 
localparam s_93 = 47; 
localparam s_95 = 48; 
localparam s_97 = 49; 
localparam s_99 = 50; 
localparam s_101 = 51; 
localparam s_103 = 52; 
localparam s_105 = 53; 
localparam s_107 = 54; 
localparam s_109 = 55; 
localparam s_111 = 56; 
localparam s_113 = 57; 
localparam s_115 = 58; 
localparam s_117 = 59; 
localparam s_119 = 60; 
localparam s_121 = 61; 
localparam s_123 = 62; 
localparam s_125 = 63; 
localparam s_127 = 64; 
localparam s_129 = 65; 
localparam s_131 = 66; 
localparam s_133 = 67; 
localparam s_135 = 68; 
localparam s_137 = 69; 
localparam s_139 = 70; 
localparam s_141 = 71; 
localparam s_143 = 72; 
localparam s_145 = 73; 
localparam s_147 = 74; 
localparam s_149 = 75; 
localparam s_151 = 76; 
localparam s_153 = 77; 
localparam s_155 = 78; 
localparam s_157 = 79; 
localparam s_159 = 80; 
localparam s_161 = 81; 
localparam s_163 = 82; 
localparam s_165 = 83; 
localparam s_167 = 84; 
localparam s_169 = 85; 
localparam s_171 = 86; 
localparam s_173 = 87; 
localparam s_175 = 88; 
localparam s_177 = 89; 
localparam s_179 = 90; 
localparam s_181 = 91; 
localparam s_183 = 92; 
localparam s_185 = 93; 
localparam s_187 = 94; 
localparam s_189 = 95; 
localparam s_191 = 96; 
localparam s_193 = 97; 
localparam s_195 = 98; 
localparam s_197 = 99; 
localparam s_199 = 100; 
localparam s_201 = 101; 
localparam s_203 = 102; 
localparam s_205 = 103; 
localparam s_207 = 104; 
localparam s_209 = 105; 
localparam s_211 = 106; 
localparam s_213 = 107; 
localparam s_215 = 108; 
localparam s_217 = 109; 
localparam s_219 = 110; 
localparam s_221 = 111; 
localparam s_223 = 112; 
localparam s_225 = 113; 
localparam s_227 = 114; 
localparam s_229 = 115; 
localparam s_231 = 116; 
localparam s_233 = 117; 
localparam s_235 = 118; 
localparam s_237 = 119; 
localparam s_239 = 120; 
localparam s_241 = 121; 
localparam s_243 = 122; 
localparam s_245 = 123; 
localparam s_247 = 124; 
localparam s_249 = 125; 
localparam s_251 = 126; 
localparam s_253 = 127; 
localparam s_255 = 128; 
localparam s_257 = 129; 
localparam s_259 = 130; 
localparam s_261 = 131; 
localparam s_263 = 132; 
localparam s_265 = 133; 
localparam s_267 = 134; 
localparam s_269 = 135; 
localparam s_271 = 136; 
localparam s_273 = 137; 
localparam s_275 = 138; 
localparam s_277 = 139; 
localparam s_279 = 140; 
localparam s_281 = 141; 
localparam s_283 = 142; 
localparam s_285 = 143; 
localparam s_287 = 144; 
localparam s_289 = 145; 
localparam s_291 = 146; 
localparam s_293 = 147; 
localparam s_295 = 148; 
localparam s_297 = 149; 
localparam s_299 = 150; 
localparam s_301 = 151; 
localparam s_303 = 152; 
localparam s_305 = 153; 
localparam s_307 = 154; 
localparam s_309 = 155; 
localparam s_311 = 156; 
localparam s_313 = 157; 
localparam s_315 = 158; 
localparam s_317 = 159; 
localparam s_319 = 160; 
localparam s_321 = 161; 
localparam s_323 = 162; 
localparam s_325 = 163; 
localparam s_327 = 164; 
localparam s_329 = 165; 
localparam s_331 = 166; 
localparam s_333 = 167; 
localparam s_335 = 168; 
localparam s_337 = 169; 
localparam s_339 = 170; 
localparam s_341 = 171; 
localparam s_343 = 172; 
localparam s_345 = 173; 
localparam s_347 = 174; 
localparam s_349 = 175; 
localparam s_351 = 176; 
localparam s_353 = 177; 
localparam s_355 = 178; 
localparam s_357 = 179; 
localparam s_359 = 180; 
localparam s_361 = 181; 
localparam s_363 = 182; 
localparam s_365 = 183; 
localparam s_367 = 184; 
localparam s_369 = 185; 
localparam s_371 = 186; 
localparam s_373 = 187; 
localparam s_375 = 188; 
localparam s_377 = 189; 
localparam s_379 = 190; 
localparam s_381 = 191; 
localparam s_383 = 192; 
localparam s_385 = 193; 
localparam s_387 = 194; 
localparam s_389 = 195; 
localparam s_391 = 196; 
localparam s_393 = 197; 
localparam s_395 = 198; 
localparam s_397 = 199; 
localparam s_399 = 200; 
localparam s_401 = 201; 
localparam s_403 = 202; 
localparam s_405 = 203; 
localparam s_407 = 204; 
localparam s_409 = 205; 
localparam s_411 = 206; 
localparam s_413 = 207; 
localparam s_415 = 208; 
localparam s_417 = 209; 
localparam s_419 = 210; 
localparam s_421 = 211; 
localparam s_423 = 212; 
localparam s_425 = 213; 
localparam s_427 = 214; 
localparam s_429 = 215; 
localparam s_431 = 216; 
localparam s_433 = 217; 
localparam s_435 = 218; 
localparam s_437 = 219; 
localparam s_439 = 220; 
localparam s_441 = 221; 
localparam s_443 = 222; 
localparam s_445 = 223; 
localparam s_447 = 224; 
localparam s_449 = 225; 
localparam s_451 = 226; 
localparam s_453 = 227; 
localparam s_455 = 228; 
localparam s_457 = 229; 
localparam s_459 = 230; 
localparam s_461 = 231; 
localparam s_463 = 232; 
localparam s_465 = 233; 
localparam s_467 = 234; 
localparam s_469 = 235; 
localparam s_471 = 236; 
localparam s_473 = 237; 
localparam s_475 = 238; 
localparam s_477 = 239; 
localparam s_479 = 240; 
localparam s_481 = 241; 
localparam s_483 = 242; 
localparam s_485 = 243; 
localparam s_487 = 244; 
localparam s_489 = 245; 
localparam s_491 = 246; 
localparam s_493 = 247; 
localparam s_495 = 248; 
localparam s_497 = 249; 
localparam s_499 = 250; 
localparam s_501 = 251; 
localparam s_503 = 252; 
localparam s_505 = 253; 
localparam s_507 = 254; 
localparam s_509 = 255; 
localparam s_511 = 256; 
localparam s_513 = 257; 
localparam s_515 = 258; 
localparam s_517 = 259; 
localparam s_519 = 260; 
localparam s_521 = 261; 
localparam s_523 = 262; 
localparam s_525 = 263; 
localparam s_527 = 264; 
localparam s_529 = 265; 
localparam s_531 = 266; 
localparam s_533 = 267; 
localparam s_535 = 268; 
localparam s_537 = 269; 
localparam s_539 = 270; 
localparam s_541 = 271; 
localparam s_543 = 272; 
localparam s_545 = 273; 
localparam s_547 = 274; 
localparam s_549 = 275; 
localparam s_551 = 276; 
localparam s_553 = 277; 
localparam s_555 = 278; 
localparam s_557 = 279; 
localparam s_559 = 280; 
localparam s_561 = 281; 
localparam s_563 = 282; 
localparam s_565 = 283; 
localparam s_567 = 284; 
localparam s_569 = 285; 
localparam s_571 = 286; 
localparam s_573 = 287; 
localparam s_575 = 288; 
localparam s_577 = 289; 
localparam s_579 = 290; 
localparam s_581 = 291; 
localparam s_583 = 292; 
localparam s_585 = 293; 
localparam s_587 = 294; 
localparam s_589 = 295; 
localparam s_591 = 296; 
localparam s_593 = 297; 
localparam s_595 = 298; 
localparam s_597 = 299; 
localparam s_599 = 300; 
localparam s_601 = 301; 
localparam s_603 = 302; 
localparam s_605 = 303; 
localparam s_607 = 304; 
localparam s_609 = 305; 
localparam s_611 = 306; 
localparam s_613 = 307; 
localparam s_615 = 308; 
localparam s_617 = 309; 
localparam s_619 = 310; 
localparam s_621 = 311; 
localparam s_623 = 312; 
localparam s_625 = 313; 
localparam s_627 = 314; 
localparam s_629 = 315; 
localparam s_631 = 316; 
localparam s_633 = 317; 
localparam s_635 = 318; 
localparam s_637 = 319; 
localparam s_639 = 320; 
localparam s_641 = 321; 
localparam s_643 = 322; 
localparam s_645 = 323; 
localparam s_647 = 324; 
localparam s_649 = 325; 
localparam s_651 = 326; 
localparam s_653 = 327; 
localparam s_655 = 328; 
localparam s_657 = 329; 
localparam s_659 = 330; 
localparam s_661 = 331; 
localparam s_663 = 332; 
localparam s_665 = 333; 
localparam s_667 = 334; 
localparam s_669 = 335; 
localparam s_671 = 336; 
localparam s_673 = 337; 
localparam s_675 = 338; 
localparam s_677 = 339; 
localparam s_679 = 340; 
localparam s_681 = 341; 
localparam s_683 = 342; 
localparam s_685 = 343; 
localparam s_687 = 344; 
localparam s_689 = 345; 
localparam s_691 = 346; 
localparam s_693 = 347; 
localparam s_695 = 348; 
localparam s_697 = 349; 
localparam s_699 = 350; 
localparam s_701 = 351; 
localparam s_703 = 352; 
localparam s_705 = 353; 
localparam s_707 = 354; 
localparam s_709 = 355; 
localparam s_711 = 356; 
localparam s_713 = 357; 
localparam s_715 = 358; 
localparam s_717 = 359; 
localparam s_719 = 360; 
localparam s_721 = 361; 
localparam s_723 = 362; 
localparam s_725 = 363; 
localparam s_727 = 364; 
localparam s_729 = 365; 
localparam s_731 = 366; 
localparam s_733 = 367; 
localparam s_735 = 368; 
localparam s_737 = 369; 
localparam s_739 = 370; 
localparam s_741 = 371; 
localparam s_743 = 372; 
localparam s_745 = 373; 
localparam s_747 = 374; 
localparam s_749 = 375; 
localparam s_751 = 376; 
localparam s_753 = 377; 
localparam s_755 = 378; 
localparam s_757 = 379; 
localparam s_759 = 380; 
localparam s_761 = 381; 
localparam s_763 = 382; 
localparam s_765 = 383; 
localparam s_767 = 384; 
localparam s_769 = 385; 
localparam s_771 = 386; 
localparam s_773 = 387; 
localparam s_775 = 388; 
localparam s_777 = 389; 
localparam s_779 = 390; 
localparam s_781 = 391; 
localparam s_783 = 392; 
localparam s_785 = 393; 
localparam s_787 = 394; 
localparam s_789 = 395; 
localparam s_791 = 396; 
localparam s_793 = 397; 
localparam s_795 = 398; 
localparam s_797 = 399; 
localparam s_799 = 400; 
localparam s_801 = 401; 
localparam s_803 = 402; 
localparam s_805 = 403; 
localparam s_807 = 404; 
localparam s_809 = 405; 
localparam s_811 = 406; 
localparam s_813 = 407; 
localparam s_815 = 408; 
localparam s_817 = 409; 
localparam s_819 = 410; 
localparam s_821 = 411; 
localparam s_823 = 412; 
localparam s_825 = 413; 
localparam s_827 = 414; 
localparam s_829 = 415; 
localparam s_831 = 416; 
localparam s_833 = 417; 
localparam s_835 = 418; 
localparam s_837 = 419; 
localparam s_839 = 420; 
localparam s_841 = 421; 
localparam s_843 = 422; 
localparam s_845 = 423; 
localparam s_847 = 424; 
localparam s_849 = 425; 
localparam s_851 = 426; 
localparam s_853 = 427; 
localparam s_855 = 428; 
localparam s_857 = 429; 
localparam s_859 = 430; 
localparam s_861 = 431; 
localparam s_863 = 432; 
localparam s_865 = 433; 
localparam s_867 = 434; 
localparam s_869 = 435; 
localparam s_871 = 436; 
localparam s_873 = 437; 
localparam s_875 = 438; 
localparam s_877 = 439; 
localparam s_879 = 440; 
localparam s_881 = 441; 
localparam s_883 = 442; 
localparam s_885 = 443; 
localparam s_887 = 444; 
localparam s_889 = 445; 
localparam s_891 = 446; 
localparam s_893 = 447; 
localparam s_895 = 448; 
localparam s_897 = 449; 
localparam s_899 = 450; 
localparam s_901 = 451; 
localparam s_903 = 452; 
localparam s_905 = 453; 
localparam s_907 = 454; 
localparam s_909 = 455; 
localparam s_911 = 456; 
localparam s_913 = 457; 
localparam s_915 = 458; 
localparam s_917 = 459; 
localparam s_919 = 460; 
localparam s_921 = 461; 
localparam s_923 = 462; 
localparam s_925 = 463; 
localparam s_927 = 464; 
localparam s_929 = 465; 
localparam s_931 = 466; 
localparam s_933 = 467; 
localparam s_935 = 468; 
localparam s_937 = 469; 
localparam s_939 = 470; 
localparam s_941 = 471; 
localparam s_943 = 472; 
localparam s_945 = 473; 
localparam s_947 = 474; 
localparam s_949 = 475; 
localparam s_951 = 476; 
localparam s_953 = 477; 
localparam s_955 = 478; 
localparam s_957 = 479; 
localparam s_959 = 480; 
localparam s_961 = 481; 
localparam s_963 = 482; 
localparam s_965 = 483; 
localparam s_967 = 484; 
localparam s_969 = 485; 
localparam s_971 = 486; 
localparam s_973 = 487; 
localparam s_975 = 488; 
localparam s_977 = 489; 
localparam s_979 = 490; 
localparam s_981 = 491; 
localparam s_983 = 492; 
localparam s_985 = 493; 
localparam s_987 = 494; 
localparam s_989 = 495; 
localparam s_991 = 496; 
localparam s_993 = 497; 
localparam s_995 = 498; 
localparam s_997 = 499; 
localparam s_999 = 500; 
localparam s_1001 = 501; 
localparam s_1003 = 502; 
localparam s_1005 = 503; 
localparam s_1007 = 504; 
localparam s_1009 = 505; 
localparam s_1011 = 506; 
localparam s_1013 = 507; 
localparam s_1015 = 508; 
localparam s_1017 = 509; 
localparam s_1019 = 510; 
localparam s_1021 = 511; 
localparam s_1023 = 512; 
localparam s_1025 = 513; 
localparam s_1027 = 514; 
localparam s_1029 = 515; 
localparam s_1031 = 516; 
localparam s_1033 = 517; 
localparam s_1035 = 518; 
localparam s_1037 = 519; 
localparam s_1039 = 520; 
localparam s_1041 = 521; 
localparam s_1043 = 522; 
localparam s_1045 = 523; 
localparam s_1047 = 524; 
localparam s_1049 = 525; 
localparam s_1051 = 526; 
localparam s_1053 = 527; 
localparam s_1055 = 528; 
localparam s_1057 = 529; 
localparam s_1059 = 530; 
localparam s_1061 = 531; 
localparam s_1063 = 532; 
localparam s_1065 = 533; 
localparam s_1067 = 534; 
localparam s_1069 = 535; 
localparam s_1071 = 536; 
localparam s_1073 = 537; 
localparam s_1075 = 538; 
localparam s_1077 = 539; 
localparam s_1079 = 540; 
localparam s_1081 = 541; 
localparam s_1083 = 542; 
localparam s_1085 = 543; 
localparam s_1087 = 544; 
localparam s_1089 = 545; 
localparam s_1091 = 546; 
localparam s_1093 = 547; 
localparam s_1095 = 548; 
localparam s_1097 = 549; 
localparam s_1099 = 550; 
localparam s_1101 = 551; 
localparam s_1103 = 552; 
localparam s_1105 = 553; 
localparam s_1107 = 554; 
localparam s_1109 = 555; 
localparam s_1111 = 556; 
localparam s_1113 = 557; 
localparam s_1115 = 558; 
localparam s_1117 = 559; 
localparam s_1119 = 560; 
localparam s_1121 = 561; 
localparam s_1123 = 562; 
localparam s_1125 = 563; 
localparam s_1127 = 564; 
localparam s_1129 = 565; 
localparam s_1131 = 566; 
localparam s_1133 = 567; 
localparam s_1135 = 568; 
localparam s_1137 = 569; 
localparam s_1139 = 570; 
localparam s_1141 = 571; 
localparam s_1143 = 572; 
localparam s_1145 = 573; 
localparam s_1147 = 574; 
localparam s_1149 = 575; 
localparam s_1151 = 576; 
localparam s_1153 = 577; 
localparam s_1155 = 578; 
localparam s_1157 = 579; 
localparam s_1159 = 580; 
localparam s_1161 = 581; 
localparam s_1163 = 582; 
localparam s_1165 = 583; 
localparam s_1167 = 584; 
localparam s_1169 = 585; 
localparam s_1171 = 586; 
localparam s_1173 = 587; 
localparam s_1175 = 588; 
localparam s_1177 = 589; 
localparam s_1179 = 590; 
localparam s_1181 = 591; 
localparam s_1183 = 592; 
localparam s_1185 = 593; 
localparam s_1187 = 594; 
localparam s_1189 = 595; 
localparam s_1191 = 596; 
localparam s_1193 = 597; 
localparam s_1195 = 598; 
localparam s_1197 = 599; 
localparam s_1199 = 600; 
localparam s_1201 = 601; 
localparam s_1203 = 602; 
localparam s_1205 = 603; 
localparam s_1207 = 604; 
localparam s_1209 = 605; 
localparam s_1211 = 606; 
localparam s_1213 = 607; 
localparam s_1215 = 608; 
localparam s_1217 = 609; 
localparam s_1219 = 610; 
localparam s_1221 = 611; 
localparam s_1223 = 612; 
localparam s_1225 = 613; 
localparam s_1227 = 614; 
localparam s_1229 = 615; 
localparam s_1231 = 616; 
localparam s_1233 = 617; 
localparam s_1235 = 618; 
localparam s_1237 = 619; 
localparam s_1239 = 620; 
localparam s_1241 = 621; 
localparam s_1243 = 622; 
localparam s_1245 = 623; 
localparam s_1247 = 624; 
localparam s_1249 = 625; 
localparam s_1251 = 626; 
localparam s_1253 = 627; 
localparam s_1255 = 628; 
localparam s_1257 = 629; 
localparam s_1259 = 630; 
localparam s_1261 = 631; 
localparam s_1263 = 632; 
localparam s_1265 = 633; 
localparam s_1267 = 634; 
localparam s_1269 = 635; 
localparam s_1271 = 636; 
localparam s_1273 = 637; 
localparam s_1275 = 638; 
localparam s_1277 = 639; 
localparam s_1279 = 640; 
localparam s_1281 = 641; 
localparam s_1283 = 642; 
localparam s_1285 = 643; 
localparam s_1287 = 644; 
localparam s_1289 = 645; 
localparam s_1291 = 646; 
localparam s_1293 = 647; 
localparam s_1295 = 648; 
localparam s_1297 = 649; 
localparam s_1299 = 650; 
localparam s_1301 = 651; 
localparam s_1303 = 652; 
localparam s_1305 = 653; 
localparam s_1307 = 654; 
localparam s_1309 = 655; 
localparam s_1311 = 656; 
localparam s_1313 = 657; 
localparam s_1315 = 658; 
localparam s_1317 = 659; 
localparam s_1319 = 660; 
localparam s_1321 = 661; 
localparam s_1323 = 662; 
localparam s_1325 = 663; 
localparam s_1327 = 664; 
localparam s_1329 = 665; 
localparam s_1331 = 666; 
localparam s_1333 = 667; 
localparam s_1335 = 668; 
localparam s_1337 = 669; 
localparam s_1339 = 670; 
localparam s_1341 = 671; 
localparam s_1343 = 672; 
localparam s_1345 = 673; 
localparam s_1347 = 674; 
localparam s_1349 = 675; 
localparam s_1351 = 676; 
localparam s_1353 = 677; 
localparam s_1355 = 678; 
localparam s_1357 = 679; 
localparam s_1359 = 680; 
localparam s_1361 = 681; 
localparam s_1363 = 682; 
localparam s_1365 = 683; 
localparam s_1367 = 684; 
localparam s_1369 = 685; 
localparam s_1371 = 686; 
localparam s_1373 = 687; 
localparam s_1375 = 688; 
localparam s_1377 = 689; 
localparam s_1379 = 690; 
localparam s_1381 = 691; 
localparam s_1383 = 692; 
localparam s_1385 = 693; 
localparam s_1387 = 694; 
localparam s_1389 = 695; 
localparam s_1391 = 696; 
localparam s_1393 = 697; 
localparam s_1395 = 698; 
localparam s_1397 = 699; 
localparam s_1399 = 700; 
localparam s_1401 = 701; 
localparam s_1403 = 702; 
localparam s_1405 = 703; 
localparam s_1407 = 704; 
localparam s_1409 = 705; 
localparam s_1411 = 706; 
localparam s_1413 = 707; 
localparam s_1415 = 708; 
localparam s_1417 = 709; 
localparam s_1419 = 710; 
localparam s_1421 = 711; 
localparam s_1423 = 712; 
localparam s_1425 = 713; 
localparam s_1427 = 714; 
localparam s_1429 = 715; 
localparam s_1431 = 716; 
localparam s_1433 = 717; 
localparam s_1435 = 718; 
localparam s_1437 = 719; 
localparam s_1439 = 720; 
localparam s_1441 = 721; 
localparam s_1443 = 722; 
localparam s_1445 = 723; 
localparam s_1447 = 724; 
localparam s_1449 = 725; 
localparam s_1451 = 726; 
localparam s_1453 = 727; 
localparam s_1455 = 728; 
localparam s_1457 = 729; 
localparam s_1459 = 730; 
localparam s_1461 = 731; 
localparam s_1463 = 732; 
localparam s_1465 = 733; 
localparam s_1467 = 734; 
localparam s_1469 = 735; 
localparam s_1471 = 736; 
localparam s_1473 = 737; 
localparam s_1475 = 738; 
localparam s_1477 = 739; 
localparam s_1479 = 740; 
localparam s_1481 = 741; 
localparam s_1483 = 742; 
localparam s_1485 = 743; 
localparam s_1487 = 744; 
localparam s_1489 = 745; 
localparam s_1491 = 746; 
localparam s_1493 = 747; 
localparam s_1495 = 748; 
localparam s_1497 = 749; 
localparam s_1499 = 750; 
localparam s_1501 = 751; 
localparam s_1503 = 752; 

reg [10:0] state, state_next, next_scan_cycle;
reg [31:0] aux_inner_pump_out;
reg [31:0] to_out_inner_pump_out = 'd1;


initial begin
state = IDLE;
state_next = IDLE;
next_scan_cycle = initial_state;
end
always @(posedge clk) begin
    state <= state_next;
end

always @* begin

    case(state)
        IDLE : begin
            
        to_out_inner_pump_out = aux_inner_pump_out;
            if (signal_start == 'b1) begin
                state_next = next_scan_cycle;
            end
        end
        
            initial_state:begin
                
                if (inner_level_in <= 0) begin
                    state_next = s_3;
                    
                end
                else if (inner_level_in > 0) begin
                    state_next = s_1501;
                    
                end
            end
            s_3:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_5;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_5;
                    aux_inner_pump_out = 0;
                end
            end
            s_5:begin
                
                    next_scan_cycle = s_7;
                    state_next = IDLE;
            end
            s_7:begin
                state_next = s_9;
            end
            s_9:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_11;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_11;
                    aux_inner_pump_out = 0;
                end
            end
            s_11:begin
                
                    next_scan_cycle = s_13;
                    state_next = IDLE;
            end
            s_13:begin
                state_next = s_15;
            end
            s_15:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_17;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_17;
                    aux_inner_pump_out = 0;
                end
            end
            s_17:begin
                
                    next_scan_cycle = s_19;
                    state_next = IDLE;
            end
            s_19:begin
                state_next = s_21;
            end
            s_21:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_23;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_23;
                    aux_inner_pump_out = 0;
                end
            end
            s_23:begin
                
                    next_scan_cycle = s_25;
                    state_next = IDLE;
            end
            s_25:begin
                state_next = s_27;
            end
            s_27:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_29;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_29;
                    aux_inner_pump_out = 0;
                end
            end
            s_29:begin
                
                    next_scan_cycle = s_31;
                    state_next = IDLE;
            end
            s_31:begin
                state_next = s_33;
            end
            s_33:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_35;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_35;
                    aux_inner_pump_out = 0;
                end
            end
            s_35:begin
                
                    next_scan_cycle = s_37;
                    state_next = IDLE;
            end
            s_37:begin
                state_next = s_39;
            end
            s_39:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_41;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_41;
                    aux_inner_pump_out = 0;
                end
            end
            s_41:begin
                
                    next_scan_cycle = s_43;
                    state_next = IDLE;
            end
            s_43:begin
                state_next = s_45;
            end
            s_45:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_47;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_47;
                    aux_inner_pump_out = 0;
                end
            end
            s_47:begin
                
                    next_scan_cycle = s_49;
                    state_next = IDLE;
            end
            s_49:begin
                state_next = s_51;
            end
            s_51:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_53;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_53;
                    aux_inner_pump_out = 0;
                end
            end
            s_53:begin
                
                    next_scan_cycle = s_55;
                    state_next = IDLE;
            end
            s_55:begin
                state_next = s_57;
            end
            s_57:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_59;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_59;
                    aux_inner_pump_out = 0;
                end
            end
            s_59:begin
                
                    next_scan_cycle = s_61;
                    state_next = IDLE;
            end
            s_61:begin
                state_next = s_63;
            end
            s_63:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_65;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_65;
                    aux_inner_pump_out = 0;
                end
            end
            s_65:begin
                
                    next_scan_cycle = s_67;
                    state_next = IDLE;
            end
            s_67:begin
                state_next = s_69;
            end
            s_69:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_71;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_71;
                    aux_inner_pump_out = 0;
                end
            end
            s_71:begin
                
                    next_scan_cycle = s_73;
                    state_next = IDLE;
            end
            s_73:begin
                state_next = s_75;
            end
            s_75:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_77;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_77;
                    aux_inner_pump_out = 0;
                end
            end
            s_77:begin
                
                    next_scan_cycle = s_79;
                    state_next = IDLE;
            end
            s_79:begin
                state_next = s_81;
            end
            s_81:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_83;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_83;
                    aux_inner_pump_out = 0;
                end
            end
            s_83:begin
                
                    next_scan_cycle = s_85;
                    state_next = IDLE;
            end
            s_85:begin
                state_next = s_87;
            end
            s_87:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_89;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_89;
                    aux_inner_pump_out = 0;
                end
            end
            s_89:begin
                
                    next_scan_cycle = s_91;
                    state_next = IDLE;
            end
            s_91:begin
                state_next = s_93;
            end
            s_93:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_95;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_95;
                    aux_inner_pump_out = 0;
                end
            end
            s_95:begin
                
                    next_scan_cycle = s_97;
                    state_next = IDLE;
            end
            s_97:begin
                state_next = s_99;
            end
            s_99:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_101;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_101;
                    aux_inner_pump_out = 0;
                end
            end
            s_101:begin
                
                    next_scan_cycle = s_103;
                    state_next = IDLE;
            end
            s_103:begin
                state_next = s_105;
            end
            s_105:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_107;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_107;
                    aux_inner_pump_out = 0;
                end
            end
            s_107:begin
                
                    next_scan_cycle = s_109;
                    state_next = IDLE;
            end
            s_109:begin
                state_next = s_111;
            end
            s_111:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_113;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_113;
                    aux_inner_pump_out = 0;
                end
            end
            s_113:begin
                
                    next_scan_cycle = s_115;
                    state_next = IDLE;
            end
            s_115:begin
                state_next = s_117;
            end
            s_117:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_119;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_119;
                    aux_inner_pump_out = 0;
                end
            end
            s_119:begin
                
                    next_scan_cycle = s_121;
                    state_next = IDLE;
            end
            s_121:begin
                state_next = s_123;
            end
            s_123:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_125;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_125;
                    aux_inner_pump_out = 0;
                end
            end
            s_125:begin
                
                    next_scan_cycle = s_127;
                    state_next = IDLE;
            end
            s_127:begin
                state_next = s_129;
            end
            s_129:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_131;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_131;
                    aux_inner_pump_out = 0;
                end
            end
            s_131:begin
                
                    next_scan_cycle = s_133;
                    state_next = IDLE;
            end
            s_133:begin
                state_next = s_135;
            end
            s_135:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_137;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_137;
                    aux_inner_pump_out = 0;
                end
            end
            s_137:begin
                
                    next_scan_cycle = s_139;
                    state_next = IDLE;
            end
            s_139:begin
                state_next = s_141;
            end
            s_141:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_143;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_143;
                    aux_inner_pump_out = 0;
                end
            end
            s_143:begin
                
                    next_scan_cycle = s_145;
                    state_next = IDLE;
            end
            s_145:begin
                state_next = s_147;
            end
            s_147:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_149;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_149;
                    aux_inner_pump_out = 0;
                end
            end
            s_149:begin
                
                    next_scan_cycle = s_151;
                    state_next = IDLE;
            end
            s_151:begin
                state_next = s_153;
            end
            s_153:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_155;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_155;
                    aux_inner_pump_out = 0;
                end
            end
            s_155:begin
                
                    next_scan_cycle = s_157;
                    state_next = IDLE;
            end
            s_157:begin
                state_next = s_159;
            end
            s_159:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_161;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_161;
                    aux_inner_pump_out = 0;
                end
            end
            s_161:begin
                
                    next_scan_cycle = s_163;
                    state_next = IDLE;
            end
            s_163:begin
                state_next = s_165;
            end
            s_165:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_167;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_167;
                    aux_inner_pump_out = 0;
                end
            end
            s_167:begin
                
                    next_scan_cycle = s_169;
                    state_next = IDLE;
            end
            s_169:begin
                state_next = s_171;
            end
            s_171:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_173;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_173;
                    aux_inner_pump_out = 0;
                end
            end
            s_173:begin
                
                    next_scan_cycle = s_175;
                    state_next = IDLE;
            end
            s_175:begin
                state_next = s_177;
            end
            s_177:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_179;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_179;
                    aux_inner_pump_out = 0;
                end
            end
            s_179:begin
                
                    next_scan_cycle = s_181;
                    state_next = IDLE;
            end
            s_181:begin
                state_next = s_183;
            end
            s_183:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_185;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_185;
                    aux_inner_pump_out = 0;
                end
            end
            s_185:begin
                
                    next_scan_cycle = s_187;
                    state_next = IDLE;
            end
            s_187:begin
                state_next = s_189;
            end
            s_189:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_191;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_191;
                    aux_inner_pump_out = 0;
                end
            end
            s_191:begin
                
                    next_scan_cycle = s_193;
                    state_next = IDLE;
            end
            s_193:begin
                state_next = s_195;
            end
            s_195:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_197;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_197;
                    aux_inner_pump_out = 0;
                end
            end
            s_197:begin
                
                    next_scan_cycle = s_199;
                    state_next = IDLE;
            end
            s_199:begin
                state_next = s_201;
            end
            s_201:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_203;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_203;
                    aux_inner_pump_out = 0;
                end
            end
            s_203:begin
                
                    next_scan_cycle = s_205;
                    state_next = IDLE;
            end
            s_205:begin
                state_next = s_207;
            end
            s_207:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_209;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_209;
                    aux_inner_pump_out = 0;
                end
            end
            s_209:begin
                
                    next_scan_cycle = s_211;
                    state_next = IDLE;
            end
            s_211:begin
                state_next = s_213;
            end
            s_213:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_215;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_215;
                    aux_inner_pump_out = 0;
                end
            end
            s_215:begin
                
                    next_scan_cycle = s_217;
                    state_next = IDLE;
            end
            s_217:begin
                state_next = s_219;
            end
            s_219:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_221;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_221;
                    aux_inner_pump_out = 0;
                end
            end
            s_221:begin
                
                    next_scan_cycle = s_223;
                    state_next = IDLE;
            end
            s_223:begin
                state_next = s_225;
            end
            s_225:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_227;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_227;
                    aux_inner_pump_out = 0;
                end
            end
            s_227:begin
                
                    next_scan_cycle = s_229;
                    state_next = IDLE;
            end
            s_229:begin
                state_next = s_231;
            end
            s_231:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_233;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_233;
                    aux_inner_pump_out = 0;
                end
            end
            s_233:begin
                
                    next_scan_cycle = s_235;
                    state_next = IDLE;
            end
            s_235:begin
                state_next = s_237;
            end
            s_237:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_239;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_239;
                    aux_inner_pump_out = 0;
                end
            end
            s_239:begin
                
                    next_scan_cycle = s_241;
                    state_next = IDLE;
            end
            s_241:begin
                state_next = s_243;
            end
            s_243:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_245;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_245;
                    aux_inner_pump_out = 0;
                end
            end
            s_245:begin
                
                    next_scan_cycle = s_247;
                    state_next = IDLE;
            end
            s_247:begin
                state_next = s_249;
            end
            s_249:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_251;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_251;
                    aux_inner_pump_out = 0;
                end
            end
            s_251:begin
                
                    next_scan_cycle = s_253;
                    state_next = IDLE;
            end
            s_253:begin
                state_next = s_255;
            end
            s_255:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_257;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_257;
                    aux_inner_pump_out = 0;
                end
            end
            s_257:begin
                
                    next_scan_cycle = s_259;
                    state_next = IDLE;
            end
            s_259:begin
                state_next = s_261;
            end
            s_261:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_263;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_263;
                    aux_inner_pump_out = 0;
                end
            end
            s_263:begin
                
                    next_scan_cycle = s_265;
                    state_next = IDLE;
            end
            s_265:begin
                state_next = s_267;
            end
            s_267:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_269;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_269;
                    aux_inner_pump_out = 0;
                end
            end
            s_269:begin
                
                    next_scan_cycle = s_271;
                    state_next = IDLE;
            end
            s_271:begin
                state_next = s_273;
            end
            s_273:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_275;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_275;
                    aux_inner_pump_out = 0;
                end
            end
            s_275:begin
                
                    next_scan_cycle = s_277;
                    state_next = IDLE;
            end
            s_277:begin
                state_next = s_279;
            end
            s_279:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_281;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_281;
                    aux_inner_pump_out = 0;
                end
            end
            s_281:begin
                
                    next_scan_cycle = s_283;
                    state_next = IDLE;
            end
            s_283:begin
                state_next = s_285;
            end
            s_285:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_287;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_287;
                    aux_inner_pump_out = 0;
                end
            end
            s_287:begin
                
                    next_scan_cycle = s_289;
                    state_next = IDLE;
            end
            s_289:begin
                state_next = s_291;
            end
            s_291:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_293;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_293;
                    aux_inner_pump_out = 0;
                end
            end
            s_293:begin
                
                    next_scan_cycle = s_295;
                    state_next = IDLE;
            end
            s_295:begin
                state_next = s_297;
            end
            s_297:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_299;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_299;
                    aux_inner_pump_out = 0;
                end
            end
            s_299:begin
                
                    next_scan_cycle = s_301;
                    state_next = IDLE;
            end
            s_301:begin
                state_next = s_303;
            end
            s_303:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_305;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_305;
                    aux_inner_pump_out = 0;
                end
            end
            s_305:begin
                
                    next_scan_cycle = s_307;
                    state_next = IDLE;
            end
            s_307:begin
                state_next = s_309;
            end
            s_309:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_311;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_311;
                    aux_inner_pump_out = 0;
                end
            end
            s_311:begin
                
                    next_scan_cycle = s_313;
                    state_next = IDLE;
            end
            s_313:begin
                state_next = s_315;
            end
            s_315:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_317;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_317;
                    aux_inner_pump_out = 0;
                end
            end
            s_317:begin
                
                    next_scan_cycle = s_319;
                    state_next = IDLE;
            end
            s_319:begin
                state_next = s_321;
            end
            s_321:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_323;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_323;
                    aux_inner_pump_out = 0;
                end
            end
            s_323:begin
                
                    next_scan_cycle = s_325;
                    state_next = IDLE;
            end
            s_325:begin
                state_next = s_327;
            end
            s_327:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_329;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_329;
                    aux_inner_pump_out = 0;
                end
            end
            s_329:begin
                
                    next_scan_cycle = s_331;
                    state_next = IDLE;
            end
            s_331:begin
                state_next = s_333;
            end
            s_333:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_335;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_335;
                    aux_inner_pump_out = 0;
                end
            end
            s_335:begin
                
                    next_scan_cycle = s_337;
                    state_next = IDLE;
            end
            s_337:begin
                state_next = s_339;
            end
            s_339:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_341;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_341;
                    aux_inner_pump_out = 0;
                end
            end
            s_341:begin
                
                    next_scan_cycle = s_343;
                    state_next = IDLE;
            end
            s_343:begin
                state_next = s_345;
            end
            s_345:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_347;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_347;
                    aux_inner_pump_out = 0;
                end
            end
            s_347:begin
                
                    next_scan_cycle = s_349;
                    state_next = IDLE;
            end
            s_349:begin
                state_next = s_351;
            end
            s_351:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_353;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_353;
                    aux_inner_pump_out = 0;
                end
            end
            s_353:begin
                
                    next_scan_cycle = s_355;
                    state_next = IDLE;
            end
            s_355:begin
                state_next = s_357;
            end
            s_357:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_359;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_359;
                    aux_inner_pump_out = 0;
                end
            end
            s_359:begin
                
                    next_scan_cycle = s_361;
                    state_next = IDLE;
            end
            s_361:begin
                state_next = s_363;
            end
            s_363:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_365;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_365;
                    aux_inner_pump_out = 0;
                end
            end
            s_365:begin
                
                    next_scan_cycle = s_367;
                    state_next = IDLE;
            end
            s_367:begin
                state_next = s_369;
            end
            s_369:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_371;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_371;
                    aux_inner_pump_out = 0;
                end
            end
            s_371:begin
                
                    next_scan_cycle = s_373;
                    state_next = IDLE;
            end
            s_373:begin
                state_next = s_375;
            end
            s_375:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_377;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_377;
                    aux_inner_pump_out = 0;
                end
            end
            s_377:begin
                
                    next_scan_cycle = s_379;
                    state_next = IDLE;
            end
            s_379:begin
                state_next = s_381;
            end
            s_381:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_383;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_383;
                    aux_inner_pump_out = 0;
                end
            end
            s_383:begin
                
                    next_scan_cycle = s_385;
                    state_next = IDLE;
            end
            s_385:begin
                state_next = s_387;
            end
            s_387:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_389;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_389;
                    aux_inner_pump_out = 0;
                end
            end
            s_389:begin
                
                    next_scan_cycle = s_391;
                    state_next = IDLE;
            end
            s_391:begin
                state_next = s_393;
            end
            s_393:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_395;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_395;
                    aux_inner_pump_out = 0;
                end
            end
            s_395:begin
                
                    next_scan_cycle = s_397;
                    state_next = IDLE;
            end
            s_397:begin
                state_next = s_399;
            end
            s_399:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_401;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_401;
                    aux_inner_pump_out = 0;
                end
            end
            s_401:begin
                
                    next_scan_cycle = s_403;
                    state_next = IDLE;
            end
            s_403:begin
                state_next = s_405;
            end
            s_405:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_407;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_407;
                    aux_inner_pump_out = 0;
                end
            end
            s_407:begin
                
                    next_scan_cycle = s_409;
                    state_next = IDLE;
            end
            s_409:begin
                state_next = s_411;
            end
            s_411:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_413;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_413;
                    aux_inner_pump_out = 0;
                end
            end
            s_413:begin
                
                    next_scan_cycle = s_415;
                    state_next = IDLE;
            end
            s_415:begin
                state_next = s_417;
            end
            s_417:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_419;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_419;
                    aux_inner_pump_out = 0;
                end
            end
            s_419:begin
                
                    next_scan_cycle = s_421;
                    state_next = IDLE;
            end
            s_421:begin
                state_next = s_423;
            end
            s_423:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_425;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_425;
                    aux_inner_pump_out = 0;
                end
            end
            s_425:begin
                
                    next_scan_cycle = s_427;
                    state_next = IDLE;
            end
            s_427:begin
                state_next = s_429;
            end
            s_429:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_431;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_431;
                    aux_inner_pump_out = 0;
                end
            end
            s_431:begin
                
                    next_scan_cycle = s_433;
                    state_next = IDLE;
            end
            s_433:begin
                state_next = s_435;
            end
            s_435:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_437;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_437;
                    aux_inner_pump_out = 0;
                end
            end
            s_437:begin
                
                    next_scan_cycle = s_439;
                    state_next = IDLE;
            end
            s_439:begin
                state_next = s_441;
            end
            s_441:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_443;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_443;
                    aux_inner_pump_out = 0;
                end
            end
            s_443:begin
                
                    next_scan_cycle = s_445;
                    state_next = IDLE;
            end
            s_445:begin
                state_next = s_447;
            end
            s_447:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_449;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_449;
                    aux_inner_pump_out = 0;
                end
            end
            s_449:begin
                
                    next_scan_cycle = s_451;
                    state_next = IDLE;
            end
            s_451:begin
                state_next = s_453;
            end
            s_453:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_455;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_455;
                    aux_inner_pump_out = 0;
                end
            end
            s_455:begin
                
                    next_scan_cycle = s_457;
                    state_next = IDLE;
            end
            s_457:begin
                state_next = s_459;
            end
            s_459:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_461;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_461;
                    aux_inner_pump_out = 0;
                end
            end
            s_461:begin
                
                    next_scan_cycle = s_463;
                    state_next = IDLE;
            end
            s_463:begin
                state_next = s_465;
            end
            s_465:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_467;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_467;
                    aux_inner_pump_out = 0;
                end
            end
            s_467:begin
                
                    next_scan_cycle = s_469;
                    state_next = IDLE;
            end
            s_469:begin
                state_next = s_471;
            end
            s_471:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_473;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_473;
                    aux_inner_pump_out = 0;
                end
            end
            s_473:begin
                
                    next_scan_cycle = s_475;
                    state_next = IDLE;
            end
            s_475:begin
                state_next = s_477;
            end
            s_477:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_479;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_479;
                    aux_inner_pump_out = 0;
                end
            end
            s_479:begin
                
                    next_scan_cycle = s_481;
                    state_next = IDLE;
            end
            s_481:begin
                state_next = s_483;
            end
            s_483:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_485;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_485;
                    aux_inner_pump_out = 0;
                end
            end
            s_485:begin
                
                    next_scan_cycle = s_487;
                    state_next = IDLE;
            end
            s_487:begin
                state_next = s_489;
            end
            s_489:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_491;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_491;
                    aux_inner_pump_out = 0;
                end
            end
            s_491:begin
                
                    next_scan_cycle = s_493;
                    state_next = IDLE;
            end
            s_493:begin
                state_next = s_495;
            end
            s_495:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_497;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_497;
                    aux_inner_pump_out = 0;
                end
            end
            s_497:begin
                
                    next_scan_cycle = s_499;
                    state_next = IDLE;
            end
            s_499:begin
                state_next = s_501;
            end
            s_501:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_503;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_503;
                    aux_inner_pump_out = 0;
                end
            end
            s_503:begin
                
                    next_scan_cycle = s_505;
                    state_next = IDLE;
            end
            s_505:begin
                state_next = s_507;
            end
            s_507:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_509;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_509;
                    aux_inner_pump_out = 0;
                end
            end
            s_509:begin
                
                    next_scan_cycle = s_511;
                    state_next = IDLE;
            end
            s_511:begin
                state_next = s_513;
            end
            s_513:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_515;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_515;
                    aux_inner_pump_out = 0;
                end
            end
            s_515:begin
                
                    next_scan_cycle = s_517;
                    state_next = IDLE;
            end
            s_517:begin
                state_next = s_519;
            end
            s_519:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_521;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_521;
                    aux_inner_pump_out = 0;
                end
            end
            s_521:begin
                
                    next_scan_cycle = s_523;
                    state_next = IDLE;
            end
            s_523:begin
                state_next = s_525;
            end
            s_525:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_527;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_527;
                    aux_inner_pump_out = 0;
                end
            end
            s_527:begin
                
                    next_scan_cycle = s_529;
                    state_next = IDLE;
            end
            s_529:begin
                state_next = s_531;
            end
            s_531:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_533;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_533;
                    aux_inner_pump_out = 0;
                end
            end
            s_533:begin
                
                    next_scan_cycle = s_535;
                    state_next = IDLE;
            end
            s_535:begin
                state_next = s_537;
            end
            s_537:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_539;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_539;
                    aux_inner_pump_out = 0;
                end
            end
            s_539:begin
                
                    next_scan_cycle = s_541;
                    state_next = IDLE;
            end
            s_541:begin
                state_next = s_543;
            end
            s_543:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_545;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_545;
                    aux_inner_pump_out = 0;
                end
            end
            s_545:begin
                
                    next_scan_cycle = s_547;
                    state_next = IDLE;
            end
            s_547:begin
                state_next = s_549;
            end
            s_549:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_551;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_551;
                    aux_inner_pump_out = 0;
                end
            end
            s_551:begin
                
                    next_scan_cycle = s_553;
                    state_next = IDLE;
            end
            s_553:begin
                state_next = s_555;
            end
            s_555:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_557;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_557;
                    aux_inner_pump_out = 0;
                end
            end
            s_557:begin
                
                    next_scan_cycle = s_559;
                    state_next = IDLE;
            end
            s_559:begin
                state_next = s_561;
            end
            s_561:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_563;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_563;
                    aux_inner_pump_out = 0;
                end
            end
            s_563:begin
                
                    next_scan_cycle = s_565;
                    state_next = IDLE;
            end
            s_565:begin
                state_next = s_567;
            end
            s_567:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_569;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_569;
                    aux_inner_pump_out = 0;
                end
            end
            s_569:begin
                
                    next_scan_cycle = s_571;
                    state_next = IDLE;
            end
            s_571:begin
                state_next = s_573;
            end
            s_573:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_575;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_575;
                    aux_inner_pump_out = 0;
                end
            end
            s_575:begin
                
                    next_scan_cycle = s_577;
                    state_next = IDLE;
            end
            s_577:begin
                state_next = s_579;
            end
            s_579:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_581;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_581;
                    aux_inner_pump_out = 0;
                end
            end
            s_581:begin
                
                    next_scan_cycle = s_583;
                    state_next = IDLE;
            end
            s_583:begin
                state_next = s_585;
            end
            s_585:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_587;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_587;
                    aux_inner_pump_out = 0;
                end
            end
            s_587:begin
                
                    next_scan_cycle = s_589;
                    state_next = IDLE;
            end
            s_589:begin
                state_next = s_591;
            end
            s_591:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_593;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_593;
                    aux_inner_pump_out = 0;
                end
            end
            s_593:begin
                
                    next_scan_cycle = s_595;
                    state_next = IDLE;
            end
            s_595:begin
                state_next = s_597;
            end
            s_597:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_599;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_599;
                    aux_inner_pump_out = 0;
                end
            end
            s_599:begin
                
                    next_scan_cycle = s_601;
                    state_next = IDLE;
            end
            s_601:begin
                state_next = s_603;
            end
            s_603:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_605;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_605;
                    aux_inner_pump_out = 0;
                end
            end
            s_605:begin
                
                    next_scan_cycle = s_607;
                    state_next = IDLE;
            end
            s_607:begin
                state_next = s_609;
            end
            s_609:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_611;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_611;
                    aux_inner_pump_out = 0;
                end
            end
            s_611:begin
                
                    next_scan_cycle = s_613;
                    state_next = IDLE;
            end
            s_613:begin
                state_next = s_615;
            end
            s_615:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_617;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_617;
                    aux_inner_pump_out = 0;
                end
            end
            s_617:begin
                
                    next_scan_cycle = s_619;
                    state_next = IDLE;
            end
            s_619:begin
                state_next = s_621;
            end
            s_621:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_623;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_623;
                    aux_inner_pump_out = 0;
                end
            end
            s_623:begin
                
                    next_scan_cycle = s_625;
                    state_next = IDLE;
            end
            s_625:begin
                state_next = s_627;
            end
            s_627:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_629;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_629;
                    aux_inner_pump_out = 0;
                end
            end
            s_629:begin
                
                    next_scan_cycle = s_631;
                    state_next = IDLE;
            end
            s_631:begin
                state_next = s_633;
            end
            s_633:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_635;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_635;
                    aux_inner_pump_out = 0;
                end
            end
            s_635:begin
                
                    next_scan_cycle = s_637;
                    state_next = IDLE;
            end
            s_637:begin
                state_next = s_639;
            end
            s_639:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_641;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_641;
                    aux_inner_pump_out = 0;
                end
            end
            s_641:begin
                
                    next_scan_cycle = s_643;
                    state_next = IDLE;
            end
            s_643:begin
                state_next = s_645;
            end
            s_645:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_647;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_647;
                    aux_inner_pump_out = 0;
                end
            end
            s_647:begin
                
                    next_scan_cycle = s_649;
                    state_next = IDLE;
            end
            s_649:begin
                state_next = s_651;
            end
            s_651:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_653;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_653;
                    aux_inner_pump_out = 0;
                end
            end
            s_653:begin
                
                    next_scan_cycle = s_655;
                    state_next = IDLE;
            end
            s_655:begin
                state_next = s_657;
            end
            s_657:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_659;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_659;
                    aux_inner_pump_out = 0;
                end
            end
            s_659:begin
                
                    next_scan_cycle = s_661;
                    state_next = IDLE;
            end
            s_661:begin
                state_next = s_663;
            end
            s_663:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_665;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_665;
                    aux_inner_pump_out = 0;
                end
            end
            s_665:begin
                
                    next_scan_cycle = s_667;
                    state_next = IDLE;
            end
            s_667:begin
                state_next = s_669;
            end
            s_669:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_671;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_671;
                    aux_inner_pump_out = 0;
                end
            end
            s_671:begin
                
                    next_scan_cycle = s_673;
                    state_next = IDLE;
            end
            s_673:begin
                state_next = s_675;
            end
            s_675:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_677;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_677;
                    aux_inner_pump_out = 0;
                end
            end
            s_677:begin
                
                    next_scan_cycle = s_679;
                    state_next = IDLE;
            end
            s_679:begin
                state_next = s_681;
            end
            s_681:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_683;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_683;
                    aux_inner_pump_out = 0;
                end
            end
            s_683:begin
                
                    next_scan_cycle = s_685;
                    state_next = IDLE;
            end
            s_685:begin
                state_next = s_687;
            end
            s_687:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_689;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_689;
                    aux_inner_pump_out = 0;
                end
            end
            s_689:begin
                
                    next_scan_cycle = s_691;
                    state_next = IDLE;
            end
            s_691:begin
                state_next = s_693;
            end
            s_693:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_695;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_695;
                    aux_inner_pump_out = 0;
                end
            end
            s_695:begin
                
                    next_scan_cycle = s_697;
                    state_next = IDLE;
            end
            s_697:begin
                state_next = s_699;
            end
            s_699:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_701;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_701;
                    aux_inner_pump_out = 0;
                end
            end
            s_701:begin
                
                    next_scan_cycle = s_703;
                    state_next = IDLE;
            end
            s_703:begin
                state_next = s_705;
            end
            s_705:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_707;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_707;
                    aux_inner_pump_out = 0;
                end
            end
            s_707:begin
                
                    next_scan_cycle = s_709;
                    state_next = IDLE;
            end
            s_709:begin
                state_next = s_711;
            end
            s_711:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_713;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_713;
                    aux_inner_pump_out = 0;
                end
            end
            s_713:begin
                
                    next_scan_cycle = s_715;
                    state_next = IDLE;
            end
            s_715:begin
                state_next = s_717;
            end
            s_717:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_719;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_719;
                    aux_inner_pump_out = 0;
                end
            end
            s_719:begin
                
                    next_scan_cycle = s_721;
                    state_next = IDLE;
            end
            s_721:begin
                state_next = s_723;
            end
            s_723:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_725;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_725;
                    aux_inner_pump_out = 0;
                end
            end
            s_725:begin
                
                    next_scan_cycle = s_727;
                    state_next = IDLE;
            end
            s_727:begin
                state_next = s_729;
            end
            s_729:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_731;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_731;
                    aux_inner_pump_out = 0;
                end
            end
            s_731:begin
                
                    next_scan_cycle = s_733;
                    state_next = IDLE;
            end
            s_733:begin
                state_next = s_735;
            end
            s_735:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_737;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_737;
                    aux_inner_pump_out = 0;
                end
            end
            s_737:begin
                
                    next_scan_cycle = s_739;
                    state_next = IDLE;
            end
            s_739:begin
                state_next = s_741;
            end
            s_741:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_743;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_743;
                    aux_inner_pump_out = 0;
                end
            end
            s_743:begin
                
                    next_scan_cycle = s_745;
                    state_next = IDLE;
            end
            s_745:begin
                state_next = s_747;
            end
            s_747:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_749;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_749;
                    aux_inner_pump_out = 0;
                end
            end
            s_749:begin
                
                    next_scan_cycle = s_751;
                    state_next = IDLE;
            end
            s_751:begin
                state_next = s_753;
            end
            s_753:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_755;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_755;
                    aux_inner_pump_out = 0;
                end
            end
            s_755:begin
                
                    next_scan_cycle = s_757;
                    state_next = IDLE;
            end
            s_757:begin
                state_next = s_759;
            end
            s_759:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_761;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_761;
                    aux_inner_pump_out = 0;
                end
            end
            s_761:begin
                
                    next_scan_cycle = s_763;
                    state_next = IDLE;
            end
            s_763:begin
                state_next = s_765;
            end
            s_765:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_767;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_767;
                    aux_inner_pump_out = 0;
                end
            end
            s_767:begin
                
                    next_scan_cycle = s_769;
                    state_next = IDLE;
            end
            s_769:begin
                state_next = s_771;
            end
            s_771:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_773;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_773;
                    aux_inner_pump_out = 0;
                end
            end
            s_773:begin
                
                    next_scan_cycle = s_775;
                    state_next = IDLE;
            end
            s_775:begin
                state_next = s_777;
            end
            s_777:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_779;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_779;
                    aux_inner_pump_out = 0;
                end
            end
            s_779:begin
                
                    next_scan_cycle = s_781;
                    state_next = IDLE;
            end
            s_781:begin
                state_next = s_783;
            end
            s_783:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_785;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_785;
                    aux_inner_pump_out = 0;
                end
            end
            s_785:begin
                
                    next_scan_cycle = s_787;
                    state_next = IDLE;
            end
            s_787:begin
                state_next = s_789;
            end
            s_789:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_791;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_791;
                    aux_inner_pump_out = 0;
                end
            end
            s_791:begin
                
                    next_scan_cycle = s_793;
                    state_next = IDLE;
            end
            s_793:begin
                state_next = s_795;
            end
            s_795:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_797;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_797;
                    aux_inner_pump_out = 0;
                end
            end
            s_797:begin
                
                    next_scan_cycle = s_799;
                    state_next = IDLE;
            end
            s_799:begin
                state_next = s_801;
            end
            s_801:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_803;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_803;
                    aux_inner_pump_out = 0;
                end
            end
            s_803:begin
                
                    next_scan_cycle = s_805;
                    state_next = IDLE;
            end
            s_805:begin
                state_next = s_807;
            end
            s_807:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_809;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_809;
                    aux_inner_pump_out = 0;
                end
            end
            s_809:begin
                
                    next_scan_cycle = s_811;
                    state_next = IDLE;
            end
            s_811:begin
                state_next = s_813;
            end
            s_813:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_815;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_815;
                    aux_inner_pump_out = 0;
                end
            end
            s_815:begin
                
                    next_scan_cycle = s_817;
                    state_next = IDLE;
            end
            s_817:begin
                state_next = s_819;
            end
            s_819:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_821;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_821;
                    aux_inner_pump_out = 0;
                end
            end
            s_821:begin
                
                    next_scan_cycle = s_823;
                    state_next = IDLE;
            end
            s_823:begin
                state_next = s_825;
            end
            s_825:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_827;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_827;
                    aux_inner_pump_out = 0;
                end
            end
            s_827:begin
                
                    next_scan_cycle = s_829;
                    state_next = IDLE;
            end
            s_829:begin
                state_next = s_831;
            end
            s_831:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_833;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_833;
                    aux_inner_pump_out = 0;
                end
            end
            s_833:begin
                
                    next_scan_cycle = s_835;
                    state_next = IDLE;
            end
            s_835:begin
                state_next = s_837;
            end
            s_837:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_839;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_839;
                    aux_inner_pump_out = 0;
                end
            end
            s_839:begin
                
                    next_scan_cycle = s_841;
                    state_next = IDLE;
            end
            s_841:begin
                state_next = s_843;
            end
            s_843:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_845;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_845;
                    aux_inner_pump_out = 0;
                end
            end
            s_845:begin
                
                    next_scan_cycle = s_847;
                    state_next = IDLE;
            end
            s_847:begin
                state_next = s_849;
            end
            s_849:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_851;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_851;
                    aux_inner_pump_out = 0;
                end
            end
            s_851:begin
                
                    next_scan_cycle = s_853;
                    state_next = IDLE;
            end
            s_853:begin
                state_next = s_855;
            end
            s_855:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_857;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_857;
                    aux_inner_pump_out = 0;
                end
            end
            s_857:begin
                
                    next_scan_cycle = s_859;
                    state_next = IDLE;
            end
            s_859:begin
                state_next = s_861;
            end
            s_861:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_863;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_863;
                    aux_inner_pump_out = 0;
                end
            end
            s_863:begin
                
                    next_scan_cycle = s_865;
                    state_next = IDLE;
            end
            s_865:begin
                state_next = s_867;
            end
            s_867:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_869;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_869;
                    aux_inner_pump_out = 0;
                end
            end
            s_869:begin
                
                    next_scan_cycle = s_871;
                    state_next = IDLE;
            end
            s_871:begin
                state_next = s_873;
            end
            s_873:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_875;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_875;
                    aux_inner_pump_out = 0;
                end
            end
            s_875:begin
                
                    next_scan_cycle = s_877;
                    state_next = IDLE;
            end
            s_877:begin
                state_next = s_879;
            end
            s_879:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_881;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_881;
                    aux_inner_pump_out = 0;
                end
            end
            s_881:begin
                
                    next_scan_cycle = s_883;
                    state_next = IDLE;
            end
            s_883:begin
                state_next = s_885;
            end
            s_885:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_887;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_887;
                    aux_inner_pump_out = 0;
                end
            end
            s_887:begin
                
                    next_scan_cycle = s_889;
                    state_next = IDLE;
            end
            s_889:begin
                state_next = s_891;
            end
            s_891:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_893;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_893;
                    aux_inner_pump_out = 0;
                end
            end
            s_893:begin
                
                    next_scan_cycle = s_895;
                    state_next = IDLE;
            end
            s_895:begin
                state_next = s_897;
            end
            s_897:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_899;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_899;
                    aux_inner_pump_out = 0;
                end
            end
            s_899:begin
                
                    next_scan_cycle = s_901;
                    state_next = IDLE;
            end
            s_901:begin
                state_next = s_903;
            end
            s_903:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_905;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_905;
                    aux_inner_pump_out = 0;
                end
            end
            s_905:begin
                
                    next_scan_cycle = s_907;
                    state_next = IDLE;
            end
            s_907:begin
                state_next = s_909;
            end
            s_909:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_911;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_911;
                    aux_inner_pump_out = 0;
                end
            end
            s_911:begin
                
                    next_scan_cycle = s_913;
                    state_next = IDLE;
            end
            s_913:begin
                state_next = s_915;
            end
            s_915:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_917;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_917;
                    aux_inner_pump_out = 0;
                end
            end
            s_917:begin
                
                    next_scan_cycle = s_919;
                    state_next = IDLE;
            end
            s_919:begin
                state_next = s_921;
            end
            s_921:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_923;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_923;
                    aux_inner_pump_out = 0;
                end
            end
            s_923:begin
                
                    next_scan_cycle = s_925;
                    state_next = IDLE;
            end
            s_925:begin
                state_next = s_927;
            end
            s_927:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_929;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_929;
                    aux_inner_pump_out = 0;
                end
            end
            s_929:begin
                
                    next_scan_cycle = s_931;
                    state_next = IDLE;
            end
            s_931:begin
                state_next = s_933;
            end
            s_933:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_935;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_935;
                    aux_inner_pump_out = 0;
                end
            end
            s_935:begin
                
                    next_scan_cycle = s_937;
                    state_next = IDLE;
            end
            s_937:begin
                state_next = s_939;
            end
            s_939:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_941;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_941;
                    aux_inner_pump_out = 0;
                end
            end
            s_941:begin
                
                    next_scan_cycle = s_943;
                    state_next = IDLE;
            end
            s_943:begin
                state_next = s_945;
            end
            s_945:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_947;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_947;
                    aux_inner_pump_out = 0;
                end
            end
            s_947:begin
                
                    next_scan_cycle = s_949;
                    state_next = IDLE;
            end
            s_949:begin
                state_next = s_951;
            end
            s_951:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_953;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_953;
                    aux_inner_pump_out = 0;
                end
            end
            s_953:begin
                
                    next_scan_cycle = s_955;
                    state_next = IDLE;
            end
            s_955:begin
                state_next = s_957;
            end
            s_957:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_959;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_959;
                    aux_inner_pump_out = 0;
                end
            end
            s_959:begin
                
                    next_scan_cycle = s_961;
                    state_next = IDLE;
            end
            s_961:begin
                state_next = s_963;
            end
            s_963:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_965;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_965;
                    aux_inner_pump_out = 0;
                end
            end
            s_965:begin
                
                    next_scan_cycle = s_967;
                    state_next = IDLE;
            end
            s_967:begin
                state_next = s_969;
            end
            s_969:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_971;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_971;
                    aux_inner_pump_out = 0;
                end
            end
            s_971:begin
                
                    next_scan_cycle = s_973;
                    state_next = IDLE;
            end
            s_973:begin
                state_next = s_975;
            end
            s_975:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_977;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_977;
                    aux_inner_pump_out = 0;
                end
            end
            s_977:begin
                
                    next_scan_cycle = s_979;
                    state_next = IDLE;
            end
            s_979:begin
                state_next = s_981;
            end
            s_981:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_983;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_983;
                    aux_inner_pump_out = 0;
                end
            end
            s_983:begin
                
                    next_scan_cycle = s_985;
                    state_next = IDLE;
            end
            s_985:begin
                state_next = s_987;
            end
            s_987:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_989;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_989;
                    aux_inner_pump_out = 0;
                end
            end
            s_989:begin
                
                    next_scan_cycle = s_991;
                    state_next = IDLE;
            end
            s_991:begin
                state_next = s_993;
            end
            s_993:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_995;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_995;
                    aux_inner_pump_out = 0;
                end
            end
            s_995:begin
                
                    next_scan_cycle = s_997;
                    state_next = IDLE;
            end
            s_997:begin
                state_next = s_999;
            end
            s_999:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1001;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1001;
                    aux_inner_pump_out = 0;
                end
            end
            s_1001:begin
                
                    next_scan_cycle = s_1003;
                    state_next = IDLE;
            end
            s_1003:begin
                state_next = s_1005;
            end
            s_1005:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1007;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1007;
                    aux_inner_pump_out = 0;
                end
            end
            s_1007:begin
                
                    next_scan_cycle = s_1009;
                    state_next = IDLE;
            end
            s_1009:begin
                state_next = s_1011;
            end
            s_1011:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1013;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1013;
                    aux_inner_pump_out = 0;
                end
            end
            s_1013:begin
                
                    next_scan_cycle = s_1015;
                    state_next = IDLE;
            end
            s_1015:begin
                state_next = s_1017;
            end
            s_1017:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1019;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1019;
                    aux_inner_pump_out = 0;
                end
            end
            s_1019:begin
                
                    next_scan_cycle = s_1021;
                    state_next = IDLE;
            end
            s_1021:begin
                state_next = s_1023;
            end
            s_1023:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1025;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1025;
                    aux_inner_pump_out = 0;
                end
            end
            s_1025:begin
                
                    next_scan_cycle = s_1027;
                    state_next = IDLE;
            end
            s_1027:begin
                state_next = s_1029;
            end
            s_1029:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1031;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1031;
                    aux_inner_pump_out = 0;
                end
            end
            s_1031:begin
                
                    next_scan_cycle = s_1033;
                    state_next = IDLE;
            end
            s_1033:begin
                state_next = s_1035;
            end
            s_1035:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1037;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1037;
                    aux_inner_pump_out = 0;
                end
            end
            s_1037:begin
                
                    next_scan_cycle = s_1039;
                    state_next = IDLE;
            end
            s_1039:begin
                state_next = s_1041;
            end
            s_1041:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1043;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1043;
                    aux_inner_pump_out = 0;
                end
            end
            s_1043:begin
                
                    next_scan_cycle = s_1045;
                    state_next = IDLE;
            end
            s_1045:begin
                state_next = s_1047;
            end
            s_1047:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1049;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1049;
                    aux_inner_pump_out = 0;
                end
            end
            s_1049:begin
                
                    next_scan_cycle = s_1051;
                    state_next = IDLE;
            end
            s_1051:begin
                state_next = s_1053;
            end
            s_1053:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1055;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1055;
                    aux_inner_pump_out = 0;
                end
            end
            s_1055:begin
                
                    next_scan_cycle = s_1057;
                    state_next = IDLE;
            end
            s_1057:begin
                state_next = s_1059;
            end
            s_1059:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1061;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1061;
                    aux_inner_pump_out = 0;
                end
            end
            s_1061:begin
                
                    next_scan_cycle = s_1063;
                    state_next = IDLE;
            end
            s_1063:begin
                state_next = s_1065;
            end
            s_1065:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1067;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1067;
                    aux_inner_pump_out = 0;
                end
            end
            s_1067:begin
                
                    next_scan_cycle = s_1069;
                    state_next = IDLE;
            end
            s_1069:begin
                state_next = s_1071;
            end
            s_1071:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1073;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1073;
                    aux_inner_pump_out = 0;
                end
            end
            s_1073:begin
                
                    next_scan_cycle = s_1075;
                    state_next = IDLE;
            end
            s_1075:begin
                state_next = s_1077;
            end
            s_1077:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1079;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1079;
                    aux_inner_pump_out = 0;
                end
            end
            s_1079:begin
                
                    next_scan_cycle = s_1081;
                    state_next = IDLE;
            end
            s_1081:begin
                state_next = s_1083;
            end
            s_1083:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1085;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1085;
                    aux_inner_pump_out = 0;
                end
            end
            s_1085:begin
                
                    next_scan_cycle = s_1087;
                    state_next = IDLE;
            end
            s_1087:begin
                state_next = s_1089;
            end
            s_1089:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1091;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1091;
                    aux_inner_pump_out = 0;
                end
            end
            s_1091:begin
                
                    next_scan_cycle = s_1093;
                    state_next = IDLE;
            end
            s_1093:begin
                state_next = s_1095;
            end
            s_1095:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1097;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1097;
                    aux_inner_pump_out = 0;
                end
            end
            s_1097:begin
                
                    next_scan_cycle = s_1099;
                    state_next = IDLE;
            end
            s_1099:begin
                state_next = s_1101;
            end
            s_1101:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1103;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1103;
                    aux_inner_pump_out = 0;
                end
            end
            s_1103:begin
                
                    next_scan_cycle = s_1105;
                    state_next = IDLE;
            end
            s_1105:begin
                state_next = s_1107;
            end
            s_1107:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1109;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1109;
                    aux_inner_pump_out = 0;
                end
            end
            s_1109:begin
                
                    next_scan_cycle = s_1111;
                    state_next = IDLE;
            end
            s_1111:begin
                state_next = s_1113;
            end
            s_1113:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1115;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1115;
                    aux_inner_pump_out = 0;
                end
            end
            s_1115:begin
                
                    next_scan_cycle = s_1117;
                    state_next = IDLE;
            end
            s_1117:begin
                state_next = s_1119;
            end
            s_1119:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1121;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1121;
                    aux_inner_pump_out = 0;
                end
            end
            s_1121:begin
                
                    next_scan_cycle = s_1123;
                    state_next = IDLE;
            end
            s_1123:begin
                state_next = s_1125;
            end
            s_1125:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1127;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1127;
                    aux_inner_pump_out = 0;
                end
            end
            s_1127:begin
                
                    next_scan_cycle = s_1129;
                    state_next = IDLE;
            end
            s_1129:begin
                state_next = s_1131;
            end
            s_1131:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1133;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1133;
                    aux_inner_pump_out = 0;
                end
            end
            s_1133:begin
                
                    next_scan_cycle = s_1135;
                    state_next = IDLE;
            end
            s_1135:begin
                state_next = s_1137;
            end
            s_1137:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1139;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1139;
                    aux_inner_pump_out = 0;
                end
            end
            s_1139:begin
                
                    next_scan_cycle = s_1141;
                    state_next = IDLE;
            end
            s_1141:begin
                state_next = s_1143;
            end
            s_1143:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1145;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1145;
                    aux_inner_pump_out = 0;
                end
            end
            s_1145:begin
                
                    next_scan_cycle = s_1147;
                    state_next = IDLE;
            end
            s_1147:begin
                state_next = s_1149;
            end
            s_1149:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1151;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1151;
                    aux_inner_pump_out = 0;
                end
            end
            s_1151:begin
                
                    next_scan_cycle = s_1153;
                    state_next = IDLE;
            end
            s_1153:begin
                state_next = s_1155;
            end
            s_1155:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1157;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1157;
                    aux_inner_pump_out = 0;
                end
            end
            s_1157:begin
                
                    next_scan_cycle = s_1159;
                    state_next = IDLE;
            end
            s_1159:begin
                state_next = s_1161;
            end
            s_1161:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1163;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1163;
                    aux_inner_pump_out = 0;
                end
            end
            s_1163:begin
                
                    next_scan_cycle = s_1165;
                    state_next = IDLE;
            end
            s_1165:begin
                state_next = s_1167;
            end
            s_1167:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1169;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1169;
                    aux_inner_pump_out = 0;
                end
            end
            s_1169:begin
                
                    next_scan_cycle = s_1171;
                    state_next = IDLE;
            end
            s_1171:begin
                state_next = s_1173;
            end
            s_1173:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1175;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1175;
                    aux_inner_pump_out = 0;
                end
            end
            s_1175:begin
                
                    next_scan_cycle = s_1177;
                    state_next = IDLE;
            end
            s_1177:begin
                state_next = s_1179;
            end
            s_1179:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1181;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1181;
                    aux_inner_pump_out = 0;
                end
            end
            s_1181:begin
                
                    next_scan_cycle = s_1183;
                    state_next = IDLE;
            end
            s_1183:begin
                state_next = s_1185;
            end
            s_1185:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1187;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1187;
                    aux_inner_pump_out = 0;
                end
            end
            s_1187:begin
                
                    next_scan_cycle = s_1189;
                    state_next = IDLE;
            end
            s_1189:begin
                state_next = s_1191;
            end
            s_1191:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1193;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1193;
                    aux_inner_pump_out = 0;
                end
            end
            s_1193:begin
                
                    next_scan_cycle = s_1195;
                    state_next = IDLE;
            end
            s_1195:begin
                state_next = s_1197;
            end
            s_1197:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1199;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1199;
                    aux_inner_pump_out = 0;
                end
            end
            s_1199:begin
                
                    next_scan_cycle = s_1201;
                    state_next = IDLE;
            end
            s_1201:begin
                state_next = s_1203;
            end
            s_1203:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1205;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1205;
                    aux_inner_pump_out = 0;
                end
            end
            s_1205:begin
                
                    next_scan_cycle = s_1207;
                    state_next = IDLE;
            end
            s_1207:begin
                state_next = s_1209;
            end
            s_1209:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1211;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1211;
                    aux_inner_pump_out = 0;
                end
            end
            s_1211:begin
                
                    next_scan_cycle = s_1213;
                    state_next = IDLE;
            end
            s_1213:begin
                state_next = s_1215;
            end
            s_1215:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1217;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1217;
                    aux_inner_pump_out = 0;
                end
            end
            s_1217:begin
                
                    next_scan_cycle = s_1219;
                    state_next = IDLE;
            end
            s_1219:begin
                state_next = s_1221;
            end
            s_1221:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1223;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1223;
                    aux_inner_pump_out = 0;
                end
            end
            s_1223:begin
                
                    next_scan_cycle = s_1225;
                    state_next = IDLE;
            end
            s_1225:begin
                state_next = s_1227;
            end
            s_1227:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1229;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1229;
                    aux_inner_pump_out = 0;
                end
            end
            s_1229:begin
                
                    next_scan_cycle = s_1231;
                    state_next = IDLE;
            end
            s_1231:begin
                state_next = s_1233;
            end
            s_1233:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1235;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1235;
                    aux_inner_pump_out = 0;
                end
            end
            s_1235:begin
                
                    next_scan_cycle = s_1237;
                    state_next = IDLE;
            end
            s_1237:begin
                state_next = s_1239;
            end
            s_1239:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1241;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1241;
                    aux_inner_pump_out = 0;
                end
            end
            s_1241:begin
                
                    next_scan_cycle = s_1243;
                    state_next = IDLE;
            end
            s_1243:begin
                state_next = s_1245;
            end
            s_1245:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1247;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1247;
                    aux_inner_pump_out = 0;
                end
            end
            s_1247:begin
                
                    next_scan_cycle = s_1249;
                    state_next = IDLE;
            end
            s_1249:begin
                state_next = s_1251;
            end
            s_1251:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1253;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1253;
                    aux_inner_pump_out = 0;
                end
            end
            s_1253:begin
                
                    next_scan_cycle = s_1255;
                    state_next = IDLE;
            end
            s_1255:begin
                state_next = s_1257;
            end
            s_1257:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1259;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1259;
                    aux_inner_pump_out = 0;
                end
            end
            s_1259:begin
                
                    next_scan_cycle = s_1261;
                    state_next = IDLE;
            end
            s_1261:begin
                state_next = s_1263;
            end
            s_1263:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1265;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1265;
                    aux_inner_pump_out = 0;
                end
            end
            s_1265:begin
                
                    next_scan_cycle = s_1267;
                    state_next = IDLE;
            end
            s_1267:begin
                state_next = s_1269;
            end
            s_1269:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1271;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1271;
                    aux_inner_pump_out = 0;
                end
            end
            s_1271:begin
                
                    next_scan_cycle = s_1273;
                    state_next = IDLE;
            end
            s_1273:begin
                state_next = s_1275;
            end
            s_1275:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1277;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1277;
                    aux_inner_pump_out = 0;
                end
            end
            s_1277:begin
                
                    next_scan_cycle = s_1279;
                    state_next = IDLE;
            end
            s_1279:begin
                state_next = s_1281;
            end
            s_1281:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1283;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1283;
                    aux_inner_pump_out = 0;
                end
            end
            s_1283:begin
                
                    next_scan_cycle = s_1285;
                    state_next = IDLE;
            end
            s_1285:begin
                state_next = s_1287;
            end
            s_1287:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1289;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1289;
                    aux_inner_pump_out = 0;
                end
            end
            s_1289:begin
                
                    next_scan_cycle = s_1291;
                    state_next = IDLE;
            end
            s_1291:begin
                state_next = s_1293;
            end
            s_1293:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1295;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1295;
                    aux_inner_pump_out = 0;
                end
            end
            s_1295:begin
                
                    next_scan_cycle = s_1297;
                    state_next = IDLE;
            end
            s_1297:begin
                state_next = s_1299;
            end
            s_1299:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1301;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1301;
                    aux_inner_pump_out = 0;
                end
            end
            s_1301:begin
                
                    next_scan_cycle = s_1303;
                    state_next = IDLE;
            end
            s_1303:begin
                state_next = s_1305;
            end
            s_1305:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1307;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1307;
                    aux_inner_pump_out = 0;
                end
            end
            s_1307:begin
                
                    next_scan_cycle = s_1309;
                    state_next = IDLE;
            end
            s_1309:begin
                state_next = s_1311;
            end
            s_1311:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1313;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1313;
                    aux_inner_pump_out = 0;
                end
            end
            s_1313:begin
                
                    next_scan_cycle = s_1315;
                    state_next = IDLE;
            end
            s_1315:begin
                state_next = s_1317;
            end
            s_1317:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1319;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1319;
                    aux_inner_pump_out = 0;
                end
            end
            s_1319:begin
                
                    next_scan_cycle = s_1321;
                    state_next = IDLE;
            end
            s_1321:begin
                state_next = s_1323;
            end
            s_1323:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1325;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1325;
                    aux_inner_pump_out = 0;
                end
            end
            s_1325:begin
                
                    next_scan_cycle = s_1327;
                    state_next = IDLE;
            end
            s_1327:begin
                state_next = s_1329;
            end
            s_1329:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1331;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1331;
                    aux_inner_pump_out = 0;
                end
            end
            s_1331:begin
                
                    next_scan_cycle = s_1333;
                    state_next = IDLE;
            end
            s_1333:begin
                state_next = s_1335;
            end
            s_1335:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1337;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1337;
                    aux_inner_pump_out = 0;
                end
            end
            s_1337:begin
                
                    next_scan_cycle = s_1339;
                    state_next = IDLE;
            end
            s_1339:begin
                state_next = s_1341;
            end
            s_1341:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1343;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1343;
                    aux_inner_pump_out = 0;
                end
            end
            s_1343:begin
                
                    next_scan_cycle = s_1345;
                    state_next = IDLE;
            end
            s_1345:begin
                state_next = s_1347;
            end
            s_1347:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1349;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1349;
                    aux_inner_pump_out = 0;
                end
            end
            s_1349:begin
                
                    next_scan_cycle = s_1351;
                    state_next = IDLE;
            end
            s_1351:begin
                state_next = s_1353;
            end
            s_1353:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1355;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1355;
                    aux_inner_pump_out = 0;
                end
            end
            s_1355:begin
                
                    next_scan_cycle = s_1357;
                    state_next = IDLE;
            end
            s_1357:begin
                state_next = s_1359;
            end
            s_1359:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1361;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1361;
                    aux_inner_pump_out = 0;
                end
            end
            s_1361:begin
                
                    next_scan_cycle = s_1363;
                    state_next = IDLE;
            end
            s_1363:begin
                state_next = s_1365;
            end
            s_1365:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1367;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1367;
                    aux_inner_pump_out = 0;
                end
            end
            s_1367:begin
                
                    next_scan_cycle = s_1369;
                    state_next = IDLE;
            end
            s_1369:begin
                state_next = s_1371;
            end
            s_1371:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1373;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1373;
                    aux_inner_pump_out = 0;
                end
            end
            s_1373:begin
                
                    next_scan_cycle = s_1375;
                    state_next = IDLE;
            end
            s_1375:begin
                state_next = s_1377;
            end
            s_1377:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1379;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1379;
                    aux_inner_pump_out = 0;
                end
            end
            s_1379:begin
                
                    next_scan_cycle = s_1381;
                    state_next = IDLE;
            end
            s_1381:begin
                state_next = s_1383;
            end
            s_1383:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1385;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1385;
                    aux_inner_pump_out = 0;
                end
            end
            s_1385:begin
                
                    next_scan_cycle = s_1387;
                    state_next = IDLE;
            end
            s_1387:begin
                state_next = s_1389;
            end
            s_1389:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1391;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1391;
                    aux_inner_pump_out = 0;
                end
            end
            s_1391:begin
                
                    next_scan_cycle = s_1393;
                    state_next = IDLE;
            end
            s_1393:begin
                state_next = s_1395;
            end
            s_1395:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1397;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1397;
                    aux_inner_pump_out = 0;
                end
            end
            s_1397:begin
                
                    next_scan_cycle = s_1399;
                    state_next = IDLE;
            end
            s_1399:begin
                state_next = s_1401;
            end
            s_1401:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1403;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1403;
                    aux_inner_pump_out = 0;
                end
            end
            s_1403:begin
                
                    next_scan_cycle = s_1405;
                    state_next = IDLE;
            end
            s_1405:begin
                state_next = s_1407;
            end
            s_1407:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1409;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1409;
                    aux_inner_pump_out = 0;
                end
            end
            s_1409:begin
                
                    next_scan_cycle = s_1411;
                    state_next = IDLE;
            end
            s_1411:begin
                state_next = s_1413;
            end
            s_1413:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1415;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1415;
                    aux_inner_pump_out = 0;
                end
            end
            s_1415:begin
                
                    next_scan_cycle = s_1417;
                    state_next = IDLE;
            end
            s_1417:begin
                state_next = s_1419;
            end
            s_1419:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1421;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1421;
                    aux_inner_pump_out = 0;
                end
            end
            s_1421:begin
                
                    next_scan_cycle = s_1423;
                    state_next = IDLE;
            end
            s_1423:begin
                state_next = s_1425;
            end
            s_1425:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1427;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1427;
                    aux_inner_pump_out = 0;
                end
            end
            s_1427:begin
                
                    next_scan_cycle = s_1429;
                    state_next = IDLE;
            end
            s_1429:begin
                state_next = s_1431;
            end
            s_1431:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1433;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1433;
                    aux_inner_pump_out = 0;
                end
            end
            s_1433:begin
                
                    next_scan_cycle = s_1435;
                    state_next = IDLE;
            end
            s_1435:begin
                state_next = s_1437;
            end
            s_1437:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1439;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1439;
                    aux_inner_pump_out = 0;
                end
            end
            s_1439:begin
                
                    next_scan_cycle = s_1441;
                    state_next = IDLE;
            end
            s_1441:begin
                state_next = s_1443;
            end
            s_1443:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1445;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1445;
                    aux_inner_pump_out = 0;
                end
            end
            s_1445:begin
                
                    next_scan_cycle = s_1447;
                    state_next = IDLE;
            end
            s_1447:begin
                state_next = s_1449;
            end
            s_1449:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1451;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1451;
                    aux_inner_pump_out = 0;
                end
            end
            s_1451:begin
                
                    next_scan_cycle = s_1453;
                    state_next = IDLE;
            end
            s_1453:begin
                state_next = s_1455;
            end
            s_1455:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1457;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1457;
                    aux_inner_pump_out = 0;
                end
            end
            s_1457:begin
                
                    next_scan_cycle = s_1459;
                    state_next = IDLE;
            end
            s_1459:begin
                state_next = s_1461;
            end
            s_1461:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1463;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1463;
                    aux_inner_pump_out = 0;
                end
            end
            s_1463:begin
                
                    next_scan_cycle = s_1465;
                    state_next = IDLE;
            end
            s_1465:begin
                state_next = s_1467;
            end
            s_1467:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1469;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1469;
                    aux_inner_pump_out = 0;
                end
            end
            s_1469:begin
                
                    next_scan_cycle = s_1471;
                    state_next = IDLE;
            end
            s_1471:begin
                state_next = s_1473;
            end
            s_1473:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1475;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1475;
                    aux_inner_pump_out = 0;
                end
            end
            s_1475:begin
                
                    next_scan_cycle = s_1477;
                    state_next = IDLE;
            end
            s_1477:begin
                state_next = s_1479;
            end
            s_1479:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1481;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1481;
                    aux_inner_pump_out = 0;
                end
            end
            s_1481:begin
                
                    next_scan_cycle = s_1483;
                    state_next = IDLE;
            end
            s_1483:begin
                state_next = s_1485;
            end
            s_1485:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1487;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1487;
                    aux_inner_pump_out = 0;
                end
            end
            s_1487:begin
                
                    next_scan_cycle = s_1489;
                    state_next = IDLE;
            end
            s_1489:begin
                state_next = s_1491;
            end
            s_1491:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1493;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1493;
                    aux_inner_pump_out = 0;
                end
            end
            s_1493:begin
                
                    next_scan_cycle = s_1495;
                    state_next = IDLE;
            end
            s_1495:begin
                state_next = s_1497;
            end
            s_1497:begin
                
                if (inner_pump_in == 0) begin
                    state_next = s_1499;
                    aux_inner_pump_out = 0;
                end
                else begin
                    state_next = s_1499;
                    aux_inner_pump_out = 0;
                end
            end
            s_1499:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
            s_1501:begin
                state_next = s_1503; aux_inner_pump_out = inner_pump_in;
            end
            s_1503:begin
                
                    next_scan_cycle = initial_state;
                    state_next = IDLE;
            end
    endcase

end
/*
Output assignments
*/
//assign output variable

assign  inner_pump_out = to_out_inner_pump_out;
endmodule


